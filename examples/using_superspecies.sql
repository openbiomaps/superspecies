-- ÖNPI - taxon update from superspecies

-- Turn off history /all update/ triggers!!!

--ALTER table onpi ADD COLUMN faj_auto character varying(64);
--ALTER table onpi ADD COLUMN magyar_auto character varying(64);
--ALTER table onpi ADD COLUMN ssp_auto character varying(64);
--UPDATE onpi SET faj_auto=NULL, magyar_auto=NULL;
--UPDATE onpi SET ssp_auto=NULL

UPDATE onpi SET ssp_auto=array_to_string(foo.a,' ') FROM (
	WITH foo AS (
     	SELECT faj,
		(regexp_split_to_array(faj, E'\\s+'))[3:] AS a, (regexp_split_to_array(faj, E'\\s+'))[2:] AS b FROM onpi
	)
	SELECT DISTINCT ON (faj,foo.a ) a,onpi.faj,array_length(b,1) as b
		FROM onpi
		LEFT JOIN foo ON foo.faj=onpi.faj
		GROUP BY a,onpi.faj,b
	) foo
WHERE foo.faj=onpi.faj AND b>1 AND a[1]!='.' AND ssp_auto IS NULL AND a[3]!='complex' AND a[3]!='boly';


-- teljes egyezés update
UPDATE onpi SET faj_auto=species_name, magyar_auto=magyarnev[1] FROM (SELECT species_name,magyarnev as magyarnev FROM superspecies) foo 
WHERE foo.species_name=faj AND faj_auto IS NULL;
-- ~311 ms

-- regexpek space-kre és kis-nagybetűre
WITH foo(species_name, magyarnev) AS (
  SELECT species_name, array_agg(regexp_replace(col, '\s+', '', 'g')) as magyarnev
  FROM superspecies, unnest(magyarnev) un(col)
  GROUP BY species_name
)
UPDATE onpi
SET faj_auto=species_name, magyar_auto=magyarnev[1]
FROM foo
WHERE lower(faj) = ANY(magyarnev) AND faj_auto IS NULL;
-- ~32s | ~16s

WITH foo(species_name, magyarnev) AS (
  SELECT species_name, array_agg(regexp_replace(col, '\s+', '', 'g')) as magyarnev
  FROM superspecies, unnest(magyarnev) un(col)
  GROUP BY species_name
)
UPDATE onpi
SET faj_auto=species_name, magyar_auto=magyarnev[1]
FROM foo
WHERE lower(REGEXP_REPLACE(faj,'\s+','','g')) = ANY(magyarnev) AND faj_auto IS NULL;
-- ~87s | ~54s

-- similarity based update / lassú!!!
SET pg_trgm.similarity_threshold = 0.7;
UPDATE onpi SET faj_auto=species_name, magyar_auto=magyarnev[1] FROM (
	SELECT faj,species_name,magyarnev,similarity(faj,species_name) as sim 
		FROM onpi JOIN superspecies ON species_name <> faj AND species_name % faj
	GROUP BY faj,species_name,superspecies.magyarnev,sim
	ORDER BY sim DESC) foo
WHERE faj_auto IS NULL AND onpi.faj=foo.faj;
-- ~221s

SET pg_trgm.similarity_threshold = 0.7;
UPDATE onpi SET faj_auto=species_name, magyar_auto=magyarnev[1] FROM (
	SELECT faj,species_name,magyarnev,similarity(faj,species_name) as sim 
		FROM onpi JOIN superspecies ON faj <> ANY(magyarnev) AND faj % ANY(magyarnev)
	GROUP BY faj,magyarnev,species_name,sim
	ORDER BY sim DESC) foo
WHERE faj_auto IS NULL AND onpi.faj=foo.faj;

SELECT faj FROM onpi WHERE faj_auto IS NULL;


-- similarity based update - gyorsabb
DROP FUNCTION similar_foo();
CREATE OR REPLACE FUNCTION similar_foo() RETURNS TABLE( a character varying, b character varying) AS 
$$
DECLARE
faj character varying(64);
rec record;
curs REFCURSOR;

BEGIN

EXECUTE 'SET pg_trgm.similarity_threshold = 0.7';

OPEN curs FOR EXECUTE 'SELECT faj FROM onpi WHERE faj_auto IS NULL';
LOOP
   FETCH curs INTO faj;
   EXIT WHEN NOT FOUND;

   return query
   SELECT species_name, faj FROM superspecies WHERE species_name % faj AND subspecies IS NULL;

END LOOP;
CLOSE curs;
END;
$$ language plpgsql;

SELECT distinct(b), a FROM similar_foo() ORDER BY b;
-- ~240s


-- OBM Taxon tábla kézi újraképzése

INSERT INTO onpi_taxon (meta,word,lang) 
	SELECT DISTINCT ON (replace(faj_auto,' ','')) replace(faj_auto,' ','') ,faj_auto, 'faj_auto' FROM onpi WHERE faj_auto IS NOT NULL;

INSERT INTO onpi_taxon (meta,word,lang,taxon_id) 
	SELECT DISTINCT ON (replace(magyar_auto,' ','')) replace(magyar_auto,' ','') ,magyar_auto, 'magyar_auto',taxon_id
        FROM onpi LEFT JOIN onpi_taxon ON faj_auto=word
        WHERE magyar_auto IS NOT NULL;
