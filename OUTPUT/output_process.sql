\c superspecies;

DROP TABLE IF EXISTS superspecies;
CREATE TABLE superspecies AS
SELECT species_id,s.species_name,subspecies,namestatus,magyarnev,vedettseg,f.values AS natura2000,g.values AS internationalagr,eszmei_ertek,shortname,j.species_name AS "odonata_chklssp_hun", taxon,auctor,mispell
FROM ssp_speciesnames s
	FULL JOIN ssp_magyarnevek b ON s.species_name=b.species_name
	FULL JOIN ssp_subspecies c ON s.species_name=c.species_name 
	FULL JOIN ssp_namestatus d ON s.species_name=d.species_name
	FULL JOIN ssp_vedettseg e ON s.species_name=e.species_name
	FULL JOIN ssp_natura2000 f ON s.species_name=f.species_name
	FULL JOIN ssp_international_agreements g ON s.species_name=g.species_name
	FULL JOIN ssp_eszmeiertek h ON s.species_name=h.species_name
	FULL JOIN ssp_shortnames i ON s.species_name=i.species_name
	FULL JOIN ssp_odonata_hun j ON s.species_name=j.species_name
	FULL JOIN ssp_taxon_hun k ON s.species_name=k.species_name
	FULL JOIN ssp_auctor l ON s.species_name=l.species_name
	FULL JOIN ssp_common_mispells m ON s.species_name=m.species_name;

ALTER TABLE ONLY public.superspecies
    ADD CONSTRAINT superspecies_pkey PRIMARY KEY (species_id);

CREATE INDEX trgm_idx_species_name ON superspecies USING gin (species_name gin_trgm_ops);
CREATE INDEX trgm_idx_magyarnev ON superspecies USING gin (magyarnev);
CREATE INDEX idx_taxon_hun ON superspecies USING btree (taxon);
CREATE INDEX idx_namestatus ON superspecies USING btree (namestatus);
