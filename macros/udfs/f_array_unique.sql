{% macro create_f_array_unique() %}
CREATE OR REPLACE FUNCTION {{target.schema}}.f_array_unique(a text[])
RETURNS TEXT[] AS $$
    select array (
        select distinct v from unnest(a) as b(v)
    )
$$ LANGUAGE sql
{% endmacro %}
