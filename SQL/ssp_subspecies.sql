\c superspecies;
--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Debian 11.7-0+deb10u1)
-- Dumped by pg_dump version 11.7 (Debian 11.7-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--ALTER TABLE ONLY public.ssp_subspecies DROP CONSTRAINT ssp_subspecies_species_name_fkey;
--ALTER TABLE ONLY public.ssp_subspecies DROP CONSTRAINT ssp_subspecies_pkey;
--DROP TABLE public.ssp_subspecies;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ssp_subspecies; Type: TABLE; Schema: public; Owner: ssp_admin
--

CREATE TABLE public.ssp_subspecies (
    species_name character varying(64) NOT NULL,
    subspecies character varying(32) NOT NULL
);


ALTER TABLE public.ssp_subspecies OWNER TO ssp_admin;

--
-- Data for Name: ssp_subspecies; Type: TABLE DATA; Schema: public; Owner: ssp_admin
--

COPY public.ssp_subspecies (species_name, subspecies) FROM stdin;
Aquilegia vulgaris subsp. nigricans	subsp. nigricans
Consolida regalis subsp. paniculata	subsp. paniculata
Paeonia officinalis subsp. banatica	subsp. banatica
Caltha palustris subsp. laeta	subsp. laeta
Daphne cneorum subsp. arbusculoides	subsp. arbusculoides
Hippophaë rhamnoides subsp. carpatica	subsp. carpatica
Pulsatilla pratensis subsp. zimmermannii	subsp. zimmermannii
Caltha palustris subsp. cornuta	subsp. cornuta
Aconitum variegatum subsp. gracilis	subsp. gracilis
Trollius europaeus subsp. demissorum	subsp. demissorum
Trollius europaeus subsp. tatrae	subsp. tatrae
Lathyrus pannonicus subsp. collinus	subsp. collinus
Lathyrus linifolius var. montanus	var. montanus
Pulsatilla pratensis subsp. nigricans	subsp. nigricans
Pulsatilla pratensis subsp. nigricans lus. rubra	subsp. nigricans lus. rubra
Pulsatilla pratensis subsp. hungarica	subsp. hungarica
Ficaria verna subsp. bulbifera	subsp. bulbifera
Ficaria verna subsp. calthifolia	subsp. calthifolia
Thalictrum minus subsp. pseudominus	subsp. pseudominus
Thalictrum simplex subsp. galioides	subsp. galioides
Pyrus nivalis subsp. salviifolia	subsp. salviifolia
Malus sylvestris subsp. dasyphylla	subsp. dasyphylla
Sorbus austriaca subsp. hazslinszkyana	subsp. hazslinszkyana
Crataegus monogyna subsp. nordica	subsp. nordica
Crataegus curvisepala subsp. calycina	subsp. calycina
Potentilla argentea subsp. grandiceps	subsp. grandiceps
Potentilla argentea subsp. tenuiloba	subsp. tenuiloba
Potentilla argentea subsp. demissa	subsp. demissa
Potentilla argentea subsp. decumbens	subsp. decumbens
Potentilla argentea subsp. macrotoma	subsp. macrotoma
Pisum sativum subsp. arvense	subsp. arvense
Potentilla wiemanniana var. javorkae	var. javorkae
Potentilla recta subsp. tuberosa	subsp. tuberosa
Potentilla recta subsp. laciniosa	subsp. laciniosa
Potentilla recta subsp. pilosa	subsp. pilosa
Potentilla recta subsp. crassa	subsp. crassa
Potentilla recta subsp. obscura	subsp. obscura
Potentilla recta subsp. leucotricha	subsp. leucotricha
Sanguisorba minor subsp. muricata	subsp. muricata
Prunus domestica subsp. insititia	subsp. insititia
Prunus spinosa subsp. fruticans	subsp. fruticans
Prunus spinosa subsp. dasyphylla	subsp. dasyphylla
Sedum reflexum subsp. glaucum	subsp. glaucum
Sedum neglectum subsp. sopianae	subsp. sopianae
Sedum sartorianum subsp. hillebrandtii	subsp. hillebrandtii
Ribes uva-crispa subsp. reclinatum	subsp. reclinatum
Ribes uva-crispa subsp. grossularia	subsp. grossularia
Ribes rubrum subsp. rubrum	subsp. rubrum
Ribes rubrum subsp. sylvestre	subsp. sylvestre
Genista tinctoria subsp. elatior	subsp. elatior
Genista ovata subsp. nervata	subsp. nervata
Chamaecytisus supinus subsp. aggregatus	subsp. aggregatus
Chamaecytisus supinus subsp. pseudorochelii	subsp. pseudorochelii
Chamaecytisus supinus subsp. pannonicus	subsp. pannonicus
Chamaecytisus hirsutus subsp. leucotrichus	subsp. leucotrichus
Ononis spinosa subsp. austriaca	subsp. austriaca
Ononis spinosiformis subsp. semihircina	subsp. semihircina
Melilotus altissimus subsp. macrorrhizus	subsp. macrorrhizus
Trifolium hybridum subsp. elegans	subsp. elegans
Trifolium fragiferum subsp. bonannii	subsp. bonannii
Trifolium medium subsp. sarosiense	subsp. sarosiense
Trifolium medium subsp. banaticum	subsp. banaticum
Trifolium subterraneum var. brachycladum	var. brachycladum
Trifolium pratense subsp. expansum	subsp. expansum
Trifolium pratense subsp. sativum	subsp. sativum
Anthyllis vulneraria subsp. polyphylla	subsp. polyphylla
Anthyllis vulneraria subsp. alpestris	subsp. alpestris
Anthyllis vulneraria subsp. carpatica	subsp. carpatica
Tetragonolobus maritimus subsp. siliquosus	subsp. siliquosus
Astragalus vesicarius subsp. albidus	subsp. albidus
Oxytropis pilosa subsp. hungarica	subsp. hungarica
Cornus sanguinea subsp. hungarica	subsp. hungarica
Anthriscus cerefolium subsp. trichosperma	subsp. trichosperma
Centaurium erythraea subsp. austriacum	subsp. austriacum
Caucalis platycarpos subsp. muricata	subsp. muricata
Bupleurum falcatum subsp. dilatatum	subsp. dilatatum
Pastinaca sativa subsp. pratensis	subsp. pratensis
Heracleum sphondylium subsp. trachycarpum	subsp. trachycarpum
Heracleum sphondylium subsp. flavescens	subsp. flavescens
Heracleum sphondylium subsp. chlorathum	subsp. chlorathum
Asperula taurina subsp. leucanthera	subsp. leucanthera
Asperula cynanchica subsp. montana	subsp. montana
Galium boreale subsp. pseudorubioides	subsp. pseudorubioides
Vicia villosa subsp. pseudovillosa	subsp. pseudovillosa
Vicia tenuifolia subsp. stenophylla	subsp. stenophylla
Vicia sativa subsp. cordata	subsp. cordata
Vicia angustifolia subsp. segetalis	subsp. segetalis
Vicia pannonica subsp. striata	subsp. striata
Vicia narbonensis subsp. serratifolia	subsp. serratifolia
Epilobium tetragonum subsp. lamyi	subsp. lamyi
Polygala vulgaris subsp. oxyptera	subsp. oxyptera
Polygala nicaeensis subsp. carniolica	subsp. carniolica
Polygala amara subsp. brachyptera	subsp. brachyptera
Polygala amarella subsp. austriaca	subsp. austriaca
Pimpinella saxifraga subsp. nigra	subsp. nigra
Libanotis pyrenaica subsp. intermedia	subsp. intermedia
Aethusa cynapium subsp. agrestis	subsp. agrestis
Aethusa cynapium subsp. cynapioides	subsp. cynapioides
Anethum graveolens var. hortorum	var. hortorum
Galium spurium subsp. infestum	subsp. infestum
Galium parisiense subsp. anglicum	subsp. anglicum
Galium glaucum subsp. hirsutum	subsp. hirsutum
Galium verum subsp. wirtgenii	subsp. wirtgenii
Galium album subsp. picnotrichum	subsp. picnotrichum
Campanula patula subsp. neglecta	subsp. neglecta
Scabiosa columbaria subsp. pseudobanatica	subsp. pseudobanatica
Scabiosa triandra subsp. agrestis	subsp. agrestis
Tilia platyphyllos subsp. cordifolia	subsp. cordifolia
Tilia platyphyllos subsp. pseudorubra	subsp. pseudorubra
Althaea officinalis subsp. pseudoarmeniaca	subsp. pseudoarmeniaca
Centaurium littorale subsp. uliginosum	subsp. uliginosum
Centaurium erythraea subsp. turcicum	subsp. turcicum
Valeriana tripteris subsp. austriaca	subsp. austriaca
Aster tripolium subsp. pannonicus	subsp. pannonicus
Limonium gmelini subsp. hungaricum	subsp. hungaricum
Rumex acetosella subsp. tenuifolius	subsp. tenuifolius
Rumex patientia subsp. recurvatus	subsp. recurvatus
Rumex patientia subsp. orientalis	subsp. orientalis
Rumex obtusifolius subsp. obtusifolius	subsp. obtusifolius
Rumex obtusifolius subsp. sylvestris	subsp. sylvestris
Rumex obtusifolius subsp. transiens	subsp. transiens
Rumex obtusifolius subsp. subalpinus	subsp. subalpinus
Rumex dentatus subsp. halacsyi	subsp. halacsyi
Polygonum lapathifolium subsp. danubiale	subsp. danubiale
Polygonum lapathifolium subsp. incanum	subsp. incanum
Knautia arvensis subsp. pannonica	subsp. pannonica
Knautia arvensis subsp. rosea	subsp. rosea
Knautia kitaibelii subsp. tomentella	subsp. tomentella
Linum hirsutum subsp. glabrescens	subsp. glabrescens
Geranium molle subsp. stripulare	subsp. stripulare
Tribulus terrestris subsp. orientalis	subsp. orientalis
Euphorbia dulcis subsp. incompta	subsp. incompta
Euphorbia platyphyllos subsp. literata	subsp. literata
Euphorbia esula subsp. pinifolia	subsp. pinifolia
Euphorbia falcata subsp. acuminata	subsp. acuminata
Fraxinus angustifolia subsp. pannonica	subsp. pannonica
Cuscuta epithymum subsp. kotschyi	subsp. kotschyi
Cuscuta australis subsp. tinei	subsp. tinei
Cuscuta australis subsp. cesatiana	subsp. cesatiana
Symphytum tuberosum subsp. angustifolium	subsp. angustifolium
Symphytum officinale subsp. uliginosum	subsp. uliginosum
Symphytum officinale subsp. bohemicum	subsp. bohemicum
Anchusa ochroleuca subsp. pustulata	subsp. pustulata
Veronica chamaedrys subsp. vindobonensis	subsp. vindobonensis
Veronica austriaca subsp. dentata	subsp. dentata
Veronica asutriaca subsp. bihariense	subsp. bihariense
Veronica spicata subsp. nitens	subsp. nitens
Xanthium albinum subsp. riparium	subsp. riparium
Veronica hederifolia subsp. triloba	subsp. triloba
Veronica hederifolia subsp. lucorum	subsp. lucorum
Lactuca quercina subsp. sagittata	subsp. sagittata
Melampyrum bihariense prol. romanicum	prol. romanicum
Lithospermum arvense subsp. coerulescens	subsp. coerulescens
Onosma arenarium subsp. tuberculatum	subsp. tuberculatum
Ajuga chamaepitys subsp. ciliata	subsp. ciliata
Teucrium montanum subsp. subvillosum	subsp. subvillosum
Galeopsis ladanum subsp. angustifolia	subsp. angustifolia
Galeobdolon luteum subsp. montanum	subsp. montanum
Leonurus cardiaca subsp. villosus	subsp. villosus
Calamintha menthifolia subsp. sylvatica	subsp. sylvatica
Origanum vulgare subsp. prismaticum	subsp. prismaticum
Thymus glabrescens subsp. decipiens	subsp. decipiens
Thymus pulegioides subsp. montanus	subsp. montanus
Lycopus europaeus subsp. mollis	subsp. mollis
Solanum nigrum subsp. schultesii	subsp. schultesii
Scrophularia umbrosa subsp. neesii	subsp. neesii
Orobanche coerulescens subsp. occidentalis	subsp. occidentalis
Orobanche cernua subsp. cumana	subsp. cumana
Cardamine pratensis subsp. dentata	subsp. dentata
Cardamine pratensis subsp. paludosus	subsp. paludosus
Cardamine pratensis subsp. matthioli	subsp. matthioli
Plantago lanceolata subsp. dubia	subsp. dubia
Plantago lanceolata subsp. sphaerostachya	subsp. sphaerostachya
Plantago major subsp. intermedia	subsp. intermedia
Plantago major subsp. winteri	subsp. winteri
Corydalis solida subsp. slivenensis	subsp. slivenensis
Brassica elongata subsp. armoracioides	subsp. armoracioides
Brassica rapa subsp. sylvestris	subsp. sylvestris
Sinapis alba subsp. dissecta	subsp. dissecta
Isatis tinctoria subsp. praecox	subsp. praecox
Biscutella laevigata subsp. kerneri	subsp. kerneri
Biscutella laevigata subsp. hungarica	subsp. hungarica
Biscutella laevigata subsp. austriaca	subsp. austriaca
Biscutella laevigata subsp. illyrica	subsp. illyrica
Thlaspi kovatsii subsp. schudichii	subsp. schudichii
Lunaria annua subsp. pachyrrhiza	subsp. pachyrrhiza
Alyssum montanum subsp. brymii	subsp. brymii
Alyssum montanum subsp. gmelinii	subsp. gmelinii
Alyssum tortuosum subsp. tortuosum	subsp. tortuosum
Alyssum tortuosum subsp. heterophyllum	subsp. heterophyllum
Filipendula ulmaria subsp. denudata	subsp. denudata
Tilia rubra ssp. caucasica	ssp. caucasica
Cardaminopsis arenosa subsp. petrogena	subsp. petrogena
Cardaminopsis arenosa subsp. borbasii	subsp. borbasii
Arabis hirsuta subsp. sagittata	subsp. sagittata
Arabis hirsuta subsp. gerardii	subsp. gerardii
Rorippa sylvestris subsp. sylvestris	subsp. sylvestris
Rorippa sylvestris subsp. kerneri	subsp. kerneri
Matthiola longipetala subsp. bicornis	subsp. bicornis
Hesperis matronalis subsp. spontanea	subsp. spontanea
Hesperis matronalis subsp. candida	subsp. candida
Hesperis matronalis subsp. vrabelyiana	subsp. vrabelyiana
Erysimum odoratum subsp. buekkense	subsp. buekkense
Camelina microcarpa subsp. pilosa	subsp. pilosa
Viola elatior subsp. jordanii	subsp. jordanii
Viola tricolor subsp. subalpina	subsp. subalpina
Viola tricolor subsp. polychroma	subsp. polychroma
Hypericum maculatum subsp. obtusiusculum	subsp. obtusiusculum
Asyneuma canescens subsp. salicifolium	subsp. salicifolium
Solidago gigantea subsp. serotina	subsp. serotina
Aster sedifolius subsp. canus	subsp. canus
Stenactis annua subsp. strigosa	subsp. strigosa
Carlina vulgaris subsp. intermedia	subsp. intermedia
Carlina vulgaris subsp. longifolia	subsp. longifolia
Arctium nemorosum subsp. pubens	subsp. pubens
Jurinea mollis subsp. macrocalathia	subsp. macrocalathia
Monotropa hypopitys subsp. hypophegea	subsp. hypophegea
Campanula glomerata subsp. farinosa	subsp. farinosa
Campanula glomerata subsp. elliptica	subsp. elliptica
Campanula sibirica subsp. divergentiformis	subsp. divergentiformis
Erigeron acris subsp. angulosus	subsp. angulosus
Erigeron acris subsp. macrophyllus	subsp. macrophyllus
Filago vulgaris subsp. lutescens	subsp. lutescens
Inula salicina subsp. aspera	subsp. aspera
Inula salicina subsp. denticulata	subsp. denticulata
Achillea nobilis subsp. neilreichii	subsp. neilreichii
Achillea distans subsp. stricta	subsp. stricta
Matricaria maritima subsp. inodora	subsp. inodora
Chrysanthemum leucanthemum subsp. sylvestre	subsp. sylvestre
Artemisia alba subsp. saxatilis	subsp. saxatilis
Artemisia alba subsp. canescens	subsp. canescens
Artemisia campestris subsp. lednicensis	subsp. lednicensis
Artemisia santonicum subsp. patens	subsp. patens
Senecio ovirensis subsp. gaudinii	subsp. gaudinii
Senecio erucifolius subsp. tenuifolius	subsp. tenuifolius
Senecio erraticus subsp. barbareifolius	subsp. barbareifolius
Senecio paludosus subsp. lanatus	subsp. lanatus
Senecio nemorensis subsp. fuchsii	subsp. fuchsii
Jurinea mollis subsp. dolomitica	subsp. dolomitica
Carduus nutans subsp. macrolepis	subsp. macrolepis
Centaurea triumfetti subsp. aligera	subsp. aligera
Centaurea triumfetti subsp. stricta	subsp. stricta
Centaurea salonitana var. taurica	var. taurica
Centaurea arenaria subsp. borysthenica	subsp. borysthenica
Centaurea arenaria subsp. tauscheri	subsp. tauscheri
Centaurea jacea subsp. subjacea	subsp. subjacea
Centaurea macroptilon subsp. oxylepis	subsp. oxylepis
Centaurea nigrescens subsp. vochinensis	subsp. vochinensis
Silene otites subsp. wolgensis	subsp. wolgensis
Silene otites subsp. pseudo-otites	subsp. pseudo-otites
Gypsophila fastigiata subsp. arenaria	subsp. arenaria
Petrorhagia glumacea var. obcordata	var. obcordata
Vaccaria hispanica subsp. grandiflora	subsp. grandiflora
Dianthus collinus subsp. glabriusculus	subsp. glabriusculus
Dianthus carthusianorum subsp. carthusianorum	subsp. carthusianorum
Dianthus plumarius subsp. praecox	subsp. praecox
Dianthus plumarius subsp. lumnitzeri	subsp. lumnitzeri
Dianthus plumarius subsp. regis-stephani	subsp. regis-stephani
Dianthus arenarius subsp. borussicus	subsp. borussicus
Cerastium fontanum subsp. macrocarpum	subsp. macrocarpum
Cerastium arvense subsp. calcicola	subsp. calcicola
Holosteum umbellatum subsp. glutinosum	subsp. glutinosum
Minuartia verna subsp. ramosissima	subsp. ramosissima
Arenaria procera subsp. glabra	subsp. glabra
Picris hieracioides subsp. crepoides	subsp. crepoides
Picris hieracioides subsp. spinulosa	subsp. spinulosa
Tragopogon dubius subsp. major	subsp. major
Sonchus arvensis subsp. uliginosus	subsp. uliginosus
Hieracium bupleuroides subsp. tatrae	subsp. tatrae
Viscum album subsp. abietis	subsp. abietis
Viscum album subsp. austriacum	subsp. austriacum
Thesium dollineri subsp. simplex	subsp. simplex
Montia fontana subsp. minor	subsp. minor
Dianthus armeria subsp. armeriastrum	subsp. armeriastrum
Dianthus carthusianorum subsp. latifolius	subsp. latifolius
Dianthus carthusianorum subsp. saxigenus	subsp. saxigenus
Stellaria media subsp. neglecta	subsp. neglecta
Stellaria media subsp. pallida	subsp. pallida
Cerastium brachypetalum subsp. tauricum	subsp. tauricum
Cerastium brachypetalum subsp. tenoreanum	subsp. tenoreanum
Cerastium pumilum subsp. pallens	subsp. pallens
Salicornia prostrata subsp. simonkaiana	subsp. simonkaiana
Suaeda maritima subsp. salinaria	subsp. salinaria
Suaeda prostrata subsp. prostrata	subsp. prostrata
Salsola kali subsp. ruthenica	subsp. ruthenica
Amaranthus graecizans subsp. sylvestris	subsp. sylvestris
Amaranthus lividus subsp. ascendens	subsp. ascendens
Primula veris subsp. inflata	subsp. inflata
Primula auricula subsp. hungarica	subsp. hungarica
Aquila nipalensis orientalis	orientalis
Primula farinosa subsp. alpigena	subsp. alpigena
Cannabis sativa subsp. spontanea	subsp. spontanea
Fagus sylvatica subsp. moesiaca	subsp. moesiaca
Quercus robur subsp. asterotricha	subsp. asterotricha
Quercus robur subsp. pilosa	subsp. pilosa
Quercus robur subsp. slavonica	subsp. slavonica
Salix triandra subsp. discolor	subsp. discolor
Salix alba subsp. vitellina	subsp. vitellina
Potamogeton pectinatus subsp. balatonicus	subsp. balatonicus
Zannichellia palustris subsp. polycarpa	subsp. polycarpa
Zannichellia palustris subsp. pedicellata	subsp. pedicellata
Veratrum album subsp. lobelianum	subsp. lobelianum
Allium paniculatum subsp. marginatum	subsp. marginatum
Allium sphaerocephalon subsp. amethystinum	subsp. amethystinum
Allium rotundum subsp. waldsteinii	subsp. waldsteinii
Muscari botryoides subsp. transsilvanicum	subsp. transsilvanicum
Muscari botryoides subsp. kerneri	subsp. kerneri
Iris aphylla subsp. hungarica	subsp. hungarica
Iris graminea subsp. pseudocyperus	subsp. pseudocyperus
Juncus bufonius subsp. nastanthus	subsp. nastanthus
Epipactis atrorubens subsp. borbasii	subsp. borbasii
Epipactis purpurata var. rosea	var. rosea
Epipactis leptochila subsp. neglecta	subsp. neglecta
Gymnadenia conopsea subsp. densiflora	subsp. densiflora
Dactylorhiza incarnata subsp. haematodes	subsp. haematodes
Ophrys scolopax subsp. cornuta	subsp. cornuta
Orchis ustulata subsp. aestivalis	subsp. aestivalis
Orchis mascula subsp. signifera	subsp. signifera
Orchis laxiflora subsp. palustris	subsp. palustris
Orchis laxiflora subsp. elegans	subsp. elegans
Dactylorhiza incarnata var. hyphaematodes	var. hyphaematodes
Dactylorhiza incarnata subsp. serotina	subsp. serotina
Dactylorhiza fuchsii subsp. sooiana	subsp. sooiana
Dactylorhiza maculata subsp. transsylvanica	subsp. transsylvanica
Holoschoenus romanus subsp. holoschoenus	subsp. holoschoenus
Schoenoplectus americanus subsp. triangularis	subsp. triangularis
Cladium mariscus subsp. martii	subsp. martii
Carex pairae subsp. leersiana	subsp. leersiana
Carex gracilis subsp. intermedia	subsp. intermedia
Brachypodium pinnatum subsp. rupestre	subsp. rupestre
Festuca pallens subsp. pannonica	subsp. pannonica
Festuca vaginata subsp. dominii	subsp. dominii
Festuca arundinacea subsp. uechtritziana	subsp. uechtritziana
Poa pannonica subsp. scabra	subsp. scabra
Molinia caerulea subsp. hungarica	subsp. hungarica
Molinia caerulea subsp. horanszkyi	subsp. horanszkyi
Molinia caerulea subsp. simonii	subsp. simonii
Molinia arundinacea subsp. ujhelyii	subsp. ujhelyii
Molinia arundinacea subsp. pocsii	subsp. pocsii
Agropyron intermedium subsp. trichophorum	subsp. trichophorum
Hordeum murinum subsp. leporinum	subsp. leporinum
Deschampsia caespitosa subsp. parviflora	subsp. parviflora
Avena sterilis subsp. ludoviciana	subsp. ludoviciana
Koeleria glauca subsp. rochelii	subsp. rochelii
Stipa joannis subsp. puberula	subsp. puberula
Panicum miliaceum subsp. ruderale	subsp. ruderale
Arum orientale subsp. bessarabicum	subsp. bessarabicum
Sparganium erectum subsp. microcarpum	subsp. microcarpum
Sparganium erectum subsp. neglectum	subsp. neglectum
Botrychium virginianum subsp. europaeum	subsp. europaeum
Asplenium trichomanes subsp. quadrivalens	subsp. quadrivalens
Pyrus pyraster subsp. anchras	subsp. anchras
Lotus corniculatus var. villosus	var. villosus
Vicia grandiflora subsp. sordida	subsp. sordida
Vicia grandiflora subsp. biebersteinii	subsp. biebersteinii
Asarum europaeum subsp. caucasicum	subsp. caucasicum
Scilla drunensis subsp. buekkensis	subsp. buekkensis
Lilium martagon subsp. alpinum	subsp. alpinum
Dactylorhiza incarnata var. ochrantha	var. ochrantha
Epipactis helleborine subsp. minor	subsp. minor
Epipactis helleborine subsp. orbicularis	subsp. orbicularis
Accipiter gentilis buteoides	buteoides
Abax schueppeli rendschmidtii	rendschmidtii
Ablepharus kitaibelii fitzingeri	fitzingeri
Acalles papei balcanicus	balcanicus
Acrocephalus agricola septima	septima
Alauda arvensis cantarella	cantarella
Alauda arvensis dulcivox	dulcivox
Alcedo atthis ispida	ispida
Alopecosa mariae orientalis	orientalis
Alosimus syriacus austriacus	austriacus
Amara chaudoiri incognita	incognita
Ameiurus nebulosus pannonicus	pannonicus
Ammophila terminata mocsaryi	mocsaryi
Amphipyra berbera svenssoni	svensoni
Andrena assimilis gallica	gallica
Andrena clypella hasitata	hasitata
Andrena combinata mehelyi	mehelyi
Andrena dorsata propinqua	propinqua
Andrena nitida limata	limata
Andrena sabulosa trimmerana	trimmerana
Andrena tibialis vindobonensis	vindobonensis
Anogcodes seladonius azureus	azureus
Anoplius viaticus paganus	paganus
Anser anser rubrirostris	rubrirostris
Anser caerulescens atlanticus	atlanticus
Anser fabalis johanseni	johanseni
Anser fabalis rossicus	rossicus
Anthaxia nitidula signaticollis	signaticollis
Apamea sicula tallosi	tallosi
Apomyelois bistriatella nephanes	nephanes
Aporinellus moestus sericeomaculatus	sericeomaculatus
Plebeius artaxerxes issekutzi	issekutzi
Arvicola terrestris scherman	scherman
Asteroscopus syriaca decipulae	decipulae
Atholus duodecimstriatus quatuordecimstriatus	quatuordecimstriatus
Barypeithes interpositus siliciensis	siliciensis
Ocydromus cruciatus bualei	bualei
Ocydromus tetragrammus illigeri	illigeri
Ocydromus subcostatus javurkovae	javurkovae
Bombus hypnorum ericetorum	ericetorum
Bombus laesus mocsaryi	mocsaryi
Bombus wurfleini mastrucatus	mastrucatus
Bonasa bonasia rupestris	rupestris
Brachydesmus attemsii tenkesensis	tenkesensis
Brachygluta helferi longispina	longispina
Bryaxis curtisii orientalis	orientalis
Buteo buteo vulpinus	vulpinus
Calandrella brachydactyla hungarica	hungarica
Calidris alpina schinzii	schinzii
Canis aureus moreoticus	moreoticus
Caprimulgus europaeus meridionalis	meridionalis
Carabus arvensis austriae	austriae
Carabus arvensis carpathus	carpathus
Carabus auronitens kraussi	kraussi
Carabus cancellatus adeptus	adeptus
Carabus cancellatus budensis	budensis
Carabus cancellatus durus	durus
Carabus cancellatus maximus	maximus
Carabus cancellatus muehlfeldi	muhlfeldi
Carabus cancellatus soproniensis	soproniensis
Carabus cancellatus tibiscinus	tibiscinus
Carabus cancellatus ungensis	ungensis
Carabus clatratus auraniensis	auraniensis
Carabus convexus kiskunensis	kiskunensis
Carabus convexus simplicipennis	simplicipennis
Carabus coriaceus praeillyricus	praeillyricus
Carabus coriaceus pseudorugifer	pseudorugifer
Carabus coriaceus rugifer	rugifer
Carabus germarii exasperatus	exasperatus
Carabus hampei ormayi	ormayi
Carabus irregularis cephalotes	cephalotes
Carabus linnaei transdanubialis	transdanubialis
Carabus montivagus blandus	blandus
Carabus problematicus holdhausi	holdhausi
Carabus scabriusculus lippii	lippii
Carabus scheidleri baderlei	baderlei
Carabus scheidleri distinguendus	distinguendus
Carabus scheidleri helleri	helleri
Carabus scheidleri jucundus	jucundus
Carabus scheidleri pannonicus	pannonicus
Carabus scheidleri praescheidleri	praescheidleri
Carabus scheidleri pseudopreyssleri	pseudopreyssleri
Carabus scheidleri vertesensis	vertesensis
Carabus ullrichii baranyensis	baranyensis
Carabus ullrichii planitiae	planitiae
Carabus ullrichii sokolari	sokolari
Carabus violaceus betuliae	betuliae
Carabus violaceus pseudoviolaceus	pseudoviolaceus
Carabus violaceus rakosiensis	rakosiensis
Carabus scheidleri zawadzkii	zawadzkii
Carabus scheidleri dissimilis	dissimilis
Carabus scheidleri ronayi	ronayi
Carduelis flammea cabaret	cabaret
Carduelis hornemanni exilipes	exilipes
Carinatodorcadion fulvum cervae	cervae
Carterus angustipennis lutshniki	lutshniki
Cerceris circularis dacica	dacica
Certhia familiaris macrodactyla	macrodactyla
Chaetopteryx schmidi mecsekensis	mecsekensis
Alburnus chalcoides mento	mento
Chamaesphecia doleriformis colpiformis	colpiformis
Charadrius hiaticula tundrae	tundrae
Charadrius leschenaultii columbinus	columbinus
Neognophina intermedia budensis	budensis
Chersotis fimbriola baloghi	baloghi
Chilopselaphus balneariellus podolicus	podolicus
Chlorophanus viridis balcanicus	balcanicus
Chondrina arcadica clienta	clienta
Chrysis viridula cylindrica	cylindrica
Chrysura dichroa socia	socia
Cylindera arenaria viennensis	viennensis
Lophyridia littoralis nemoralis	nemoralis
Cicindela soluta pannonica	pannonica
Cicindela hybrida transversalis	transversalis
Cinclus cinclus aquaticus	aquaticus
Cionus longicollis montanus	montanus
Clausilia dubia vindobonensis	vindobonensis
Clausilia rugosa parvula	parvula
Clubiona corticalis concolor	concolor
Coccinella undecimpunctata tripunctata	tripunctata
Coenagrion pulchellum interruptum	interruptum
Colpa quinquecincta abdominalis	abdominalis
Columba livia f. domestica	f. domestica
Corvus corone sardonius	sardonius
Corvus monedula soemmeringi	soemmeringi
Corvus monedula spermologus	spermologus
Craspedosoma transsilvanicum austriacum	austriacum
Craspedosoma transsilvanicum barcsicum	barcsicum
Craspedosoma transsilvanicum pakozdense	pakozdense
Cryptocheilus notatus affinis	affinis
Cryptophonus tenebrosus centralis	centralis
Cucullia mixta lorica	lorica
Culex pipiens molestus	molestus
Cyclodinus dentatus transdanubianus	transdanubianus
Cyphocleonus dealbatus tigrinus	tigrinus
Dendrocopos major pinetorum	pinetorum
Dendrocopos minor hortorum	hortorum
Dinodes decipiens ambiguus	ambiguus
Diplocephalus alpinus strandi	strandi
Dyschiriodes chalybaeus gibbifrons	gibbifrons
Dyschiriodes parallelus ruficornis	ruficornis
Dyschiriodes salinus striatopunctatus	striatopunctatus
Dyschiriodes substriatus priscus	priscus
Dysdera erythrina lantosquensis	lantosquensis
Eilema pygmaeola pallifrons	pallifrons
Elaphoidella pseudojeanneli aggtelekiensis	aggtelekiensis
Elaphoidella simplex szegedensis	szegedensis
Emberiza schoeniclus intermedia	intermedia
Emberiza schoeniclus stresemanni	stresemanni
Emberiza schoeniclus tschusii	tschusii
Emberiza schoeniclus ukrainae	ukrainae
Enantiulus tatranus evae	evae
Ephestia unicolorella woodiella	woodiella
Episyron gallicus tertius	tertius
Eremophila alpestris flava	flava
Erinaceus concolor roumanicus	roumanicus
Fagotia daudebartii thermalis	thermalis
Fagotia daudebartii acicularis	acicularis
Eucera caspica perezi	perezi
Eumenes sareptanus insolatus	insolatus
Euodynerus egregius unimaculatus	unimaculatus
Euodynerus notatus pubescens	pubescens
Euodynerus quadrifasciatus simplex	simplex
Euonthophagus amyntas alces	alces
Euplectus bonvouloiri rosae	rosae
Eutheia scydmaenoides orientalis	orientalis
Euxoa hastifera pomazensis	pomazensis
Falco cherrug cyanopus	cyanopus
Falco columbarius aesalon	aesalon
Falco peregrinus calidus	calidus
Fratercula arctica grabae	grabae
Galerida cristata tenuirostris	tenuirostris
Garrulus glandarius albipectus	albipectus
Gibbaranea bituberculata strandiana	strandiana
Gnathoncus disjunctus suturifer	suturifer
Gonocephalum granulatum pusillum	pusillum
Gortyna borelii lunata	lunata
Hadula dianthi hungarica	hungarica
Haematopus ostralegus longipes	longipes
Halictus confusus perkinsi	perkinsi
Halictus gavarnicus tataricus	tataricus
Halictus leucaheneus arenosus	arenosus
Halictus patellatus taorminicus	taorminicus
Halictus pollinosus cariniventris	cariniventris
Harpalus angulatus scytha	scytha
Harpalus caspius roubali	roubali
Harpalus cupreus fastuosus	fastuosus
Harpalus xanthopus winkleri	winkleri
Hedychrum aureicolle niemelai	niemelai
Heliothis maritima bulgarica	bulgarica
Hemipterochilus bembiciformis terricola	terricola
Herilla ziegleri dacida	dacica
Heterothops praevius niger	niger
Iduna pallida elaeica	elaeica
Holcopogon bubulcellus helveolellus	helveolellus
Holcostethus strictus vernalis	vernalis
Hydroporus discretus ponticus	ponticus
Hylaeus gibbus confusus	confusus
Hylebainosoma tatranum josvaense	josvaense
Hyperaspis reppensis quadrimaculata	quadrimaculata
Idaea elongaria pecharia	pecharia
Ischnura elegans pontica	pontica
Julus terrestris balatonensis	balatonensis
Lanius excubitor homeyeri	homeyeri
Larus canus heinei	heinei
Larus fuscus heuglini	heuglini
Lasioglossum brevicorne aciculatum	aciculatum
Lasioglossum nitidulum aeneidorsum	aeneidorsum
Lasioglossum obscuratum acerbum	acerbum
Leiosoma scrobifer baudii	baudii
Leistus piceus kaszabi	kaszabi
Hylesinus wachtli orni	orni
Leptidea morsei major	major
Leptoiulus proximus noaranus	noaranus
Leptoiulus simplex attenuatus	attenuatus
Leptothorax sordidulus saxonicus	saxonicus
Lestes virens vestalis	vestalis
Lindenius pygmaeus armatus	armatus
Linyphia triangularis juniperina	juniperina
Liocranoeca striata gracilior	gracilior
Lithobius aeruginosus batorligetensis	batorligetensis
Lithobius agilis pannonicus	pannonicus
Lithobius agilis tricalcaratus	tricalcaratus
Lithobius erythrocephalus schuleri	schuleri
Lithobius mutabilis carpathicus	carpathicus
Lithobius nodulipes scarabanciae	scarabanciae
Lithobius parietum mecsekensis	mecsekensis
Lithobius pusillus novemaculatus	novemaculatus
Lithobius stygius infernus	infernus
Lithobius tenebrosus sulcatipes	sulcatipes
Lithurgus cornutus fuscipennis	fuscipennis
Loxia leucoptera bifasciata	bifasciata
Luscinia svecica cyanecula	cyanecula
Lycaena dispar rutila	rutila
Maccevethus errans caucasicus	caucasicus
Macrogastra borealis bielzi	bielzi
Macrogastra plicatula rusiostoma	rusiostoma
Glaucopsyche arion ligurica	ligurica
Margarinotus striola succicola	succicola
Megaphyllum bosniense cotinophilum	cotinophilum
Megaphyllum projectum dioritanum	dioritanum
Megaphyllum projectum kochi	kochi
Megaphyllum transsylvanicum transdanubicum	transdanubicum
Megascolia maculata flavifrons	flavifrons
Melitaea punica telona	telona
Melogona broelemanni gebhardti	gebhardti
Melogona transsilvanica hungarica	hungarica
Microlestes corticalis escorialensis	escorialensis
Microtus oeconomus mehelyi	mehelyi
Miscophus helveticus rubriventris	rubriventris
Molops piceus austriacus	austriacus
Monochamus galloprovincialis pistor	pistor
Mordella velutina panonica	panonica
Morimus asper funereus	funereus
Morlina glabra striaria	striaria
Motacilla citreola werae	werae
Motacilla flava feldegg	feldegg
Motacilla flava thunbergi	thunbergi
Mustela eversmanii hungarica	hungarica
Mustela nivalis vulgaris	vulgaris
Mycetoporus solidicornis reichei	reichei
Myotis blythii oxygnathus	oxygnathus
Nemesia pannonica budensis	budensis
Neoplinthus tigratus porcatus	porcatus
Nephus bisignatus claudiae	claudiae
Protaetia cuprea obscura	obscura
Nineta guadarramensis principiae	principiae
Nomada baccata hrubanti	hrubanti
Nomada bifasciata fucata	fucata
Nomada bifasciata lepeletieri	lepeletieri
Nomada mauritanica manni	manni
Nomada panzeri glabella	glabella
Nomada sheppardana minuscula	minuscula
Notoxus cavifrons appendicinus	appendicinus
Nucifraga caryocatactes macrorhynchos	macrorhynchos
Numenius arquata orientalis	orientalis
Ochogona caroli hungaricum	hungaricum
Ochogona caroli somloense	somloense
Oenanthe hispanica melanoleuca	melanoleuca
Oenopia lyncea agnata	agnata
Ophonus sabulicola ponticus	ponticus
Ovis aries musimon	musimon
Oxybelus argentatus gerstaeckeri	gerstaeckeri
Oxybelus argentatus treforti	treforti
Oxybelus dissectus elegans	elegans
Pagodulina pagodula altilis	altilis
Panurus biarmicus russicus	russicus
Paranthrene insolita polonica	polonica
Parazuphium chevrolati praepannonicum	praepannonicum
Parus montanus borealis	borealis
Parus palustris stagnatilis	stagnatilis
Pedinus fallax gracilis	gracilis
Phalacrocorax carbo sinensis	sinensis
Phasianus colchicus torquatus	torquatus
Phoenicurus ochruros gibraltariensis	gibraltariensis
Photedes captiuncula delattini	delattini
Phylloscopus collybita abietinus	abietinus
Phylloscopus collybita tristis	tristis
Phylloscopus trochilus acredula	acredula
Pieris bryoniae marani	marani
Platyderus rufus transalpinus	transalpinus
Plectrophenax nivalis vlasowae	vlasowae
Polistes biglumis bimaculatus	bimaculatus
Polydesmus edentulus bidentatus	bidentatus
Polydesmus monticolus koszegensis	koszegensis
Polymixis rufocincta isolata	isolata
Porphyrio porphyrio seistanicus	seistanicus
Porzana pusilla intermedia	intermedia
Pseudovadonia livida pecta	pecta
Psithyrus barbutellus maxillosus	maxillosus
Pterocheilus phaleratus chevrieranus	chevrieranus
Pterostichus anthracinus biimpressus	biimpressus
Pterostichus piceolus latoricaensis	latoricaensis
Pyrrhocorax pyrrhocorax erythrorhamphus	erythrorhamphus
Pyrrhosoma nymphula interposita	interposita
Quedius dubius fimbriatus	fimbriatus
Rana arvalis wolterstorffi	wolterstorffi
Raphidia ophiopsis mediterranea	mediterranea
Rhadicoleptus alpestris sylvanocarpathicus	sylvanocarpathicus
Rhyacophila dorsalis persimilis	persimilis
Rophites algirus trispinosus	trispinosus
Sabanejewia aurata balcanica	balcanica
Sabanejewia aurata bulgarica	bulgarica
Saprinus tenuistrius sparsutus	sparsutus
Saragossa porosa kenderesiensis	kenderesiensis
Scolia sexmaculata quadripunctata	quadripunctata
Pseudophilotes vicrama schiffermuelleri	schiffermuelleri
Scrobipalpa samadensis plantaginella	plantaginella
Scymnus pallipediformis apetzoides	apetzoides
Sicista subtilis trizona	trizona
Sitta europaea caesia	caesia
Stenomax aeneus incurvus	incurvus
Stenus intricatus zoufali	zoufali
Stenus picipes brevipennis	brevipennis
Stilbum calens zimmermanni	zimmermanni
Streptopelia orientalis meena	meena
Strix uralensis macroura	macroura
Styrioiulus pelidnus orientalis	orientalis
Sylvia cantillans albistriata	albistriata
Tetragnatha extensa pulchra	pulchra
Tetrao urogallus major	major
Theodoxus danubialis stragulatus	stragulatus
Trachypteris picta decostigma	decastigma
Trachys puncticollis rectilineatus	rectilineatus
Trachysphaera noduligera hungarica	hungarica
Trichia striolata danubialis	danubialis
Trissemus antennatus serricornis	serricornis
Turdus torquatus alpestris	alpestris
Tyto alba guttata	guttata
Velia affinis filippii	filippii
Vipera ursinii rakosiensis	rakosiensis
Walckenaeria cuspidata obsoleta	obsoleta
Xestia ashworthii candelarum	candelarum
Xestoiulus laeticollis dudichi	dudichi
Xestoiulus laeticollis evae	evae
Zootoca vivipara pannonica	pannonica
Trachemys scripta elegans	elegans
Herilla ziegleri dacica	dacica
Aconitum lycoctonum subsp. moldavicum	subsp. moldavicum
Aconitum lycoctonum subsp. vulparia	subsp. vulparia
Ranunculus bulbosus subsp. aleae	subsp. aleae
Stipa eriocaulis subsp. austriaca	subsp. austriaca
Stipa eriocaulis subsp. eriocaulis	subsp. eriocaulis
Anemone pulsatilla subsp. grandis	subsp. grandis
Trox hispidus niger	niger
Vicia cracca subsp. cracca	subsp. cracca
Vicia cracca subsp. kitaibeliana	subsp. kitaibeliana
Vicia pannonica subsp. pannonica	subsp. pannonica
Vicia pannonica subsp. purpurascens	subsp. purpurascens
Vicia sativa subsp. angustifolia	subsp. angustifolia
Vicia sativa subsp. obovata	subsp. obovata
Vicia sativa subsp. sativa	subsp. sativa
Vicia sativa subsp. segetalis	subsp. segetalis
Vicia tenuifolia subsp. dalmatica	subsp. dalmatica
Vicia tenuifolia subsp. tenuifolia	subsp. tenuifolia
Vicia villosa subsp. varia	subsp. varia
Vicia villosa subsp. villosa	subsp. villosa
Viola alba subsp. alba	subsp. alba
Viola alba subsp. scotophylla	subsp. scotophylla
Viola canina subsp. canina	subsp. canina
Viola canina subsp. montana	subsp. montana
Viola elatior subsp. elatior	subsp. elatior
Viola odorata subsp. odorata	subsp. odorata
Rubus hirtus subsp. hercynicus	subsp. hercynicus
Rubus hirtus subsp. kaltenbachii	subsp. kaltenbachii
Rubus hirtus subsp. posoniensis	subsp. posoniensis
Rubus hirtus subsp. tenuidentatus	subsp. tenuidentatus
Lotus corniculatus subsp. hirsutus	subsp. hirsutus
Lotus corniculatus subsp. major	subsp. major
Trifolium arvense subsp. gracile	subsp. gracile
Vicia sativa subsp. nigra	subsp. nigra
Carex pairae subsp. leersii	subsp. leersii
Eleocharis palustris subsp. austriaca	subsp. austriaca
Abax schüppeli rendschmidti	rendschmidti
Acer campestre subsp. hebecarpum	subsp. hebecarpum
Achillea distans subsp. tanacetifolia	subsp. tanacetifolia
Achillea millefolium subsp. pannonica	subsp. pannonica
Acipenser gueldenstaedti colchicus	colchicus
Aconitum variegatum subsp. gracile	subsp. gracile
Aegithalos caudatus europaeus	europaeus
Aethusa cynapium subsp. segetalis	subsp. segetalis
Agapanthia cardui pannonica	pannonica
Agaricus arvensis subsp. macrolepis	subsp. macrolepis
Agropyron cristatum subsp. pectinatum	subsp. pectinatum
Agropyron repens subsp. caesium	subsp. caesium
Agrostis stolonifera subsp. gigantea	subsp. gigantea
Agrostis stolonifera subsp. stolonizans	subsp. stolonizans
Agrostis vinealis subsp. montana	subsp. montana
Agrostis vinealis subsp. syreistchikovii	subsp. syreistchikovii
Ajuga chamaepitys subsp. chia	subsp. chia
Allium scorodoprasum subsp. rotundum	subsp. rotundum
Allium scorodoprasum subsp. waldsteinii	subsp. waldsteinii
Allium senescens subsp. montanum	subsp. montanum
Allium sphaerocephalon subsp. descendens	subsp. descendens
Allium ursinum subsp. ucrainicum	subsp. ucrainicum
Allium ursinum subsp. ursinum	subsp. ursinum
Alopia livida bipalatalis	bipalatalis
Alopia livida monacha	monacha
Alopia straminicollis monacha	monacha
Alyssum alyssoides subsp. conglobatum	subsp. conglobatum
Alyssum minus subsp. strigosum	subsp. strigosum
Alyssum montanum subsp. montanum	subsp. montanum
Amaranthus chlorostachys subsp. powellii	subsp. powellii
Amaranthus graecizans subsp. silvestris	subsp. silvestris
Amaranthus lividus subsp. oleraceus	subsp. oleraceus
Amaranthus lividus subsp. polygonoides	subsp. polygonoides
Amaranthus retroflexus subsp. quitensis	subsp. quitensis
Amara tricuspidata pseudostrenua	pseudostrenua
Amphipyra berbera svensoni	svensoni
Anchusa ochroleuca subsp. legitima	subsp. legitima
Angelica sylvestris subsp. montana	subsp. montana
Anisoplia zwickii lata	lata
Anodonta anatina attenuata	attenuata
Anodonta anatina balatonica	balatonica
Anodonta complanata compacta	compacta
Anodonta cygnea solearis	solearis
Anodonta cygnea zellensis	zellensis
Anogcodes dispar azurea	azurea
Anthicus dentatus transdanubialis	transdanubialis
Anthriscus cerefolium subsp. trichospermus	subsp. trichospermus
Apamea syriaca tallosi	tallosi
Apium graveolens subsp. rapaceum	subsp. rapaceum
Arabis hirsuta subsp. gerardi	subsp. gerardi
Arenaria serpyllifolia subsp. leptoclados	subsp. leptoclados
Aricia artaxerxes issekutzi	issekutzi
Arion hortensis distinctus	distinctus
Arrhenatherum elatius subsp. bulbosum	subsp. bulbosum
Artemisia maritima subsp. monogyna	subsp. monogyna
Arum orientale subsp. besseranum	subsp. besseranum
Aster alpinus subsp. glabratus	subsp. glabratus
Aster punctatus subsp. canus	subsp. canus
Aster sedifolius subsp. sedifolius	subsp. sedifolius
Aster serpentimontanus subsp. glabratus	subsp. glabratus
Avena nuda subsp. brevis	subsp. brevis
Avena nuda subsp. strigosa	subsp. strigosa
Barbarea vulgaris subsp. arcuata	subsp. arcuata
Barbarea vulgaris subsp. rivularis	subsp. rivularis
Barbarea vulgaris subsp. vulgaris	subsp. vulgaris
Barbus peloponnesius petenyi	petenyi
Bembidion dalmatinum latinum	latinum
Bembidion subcostatum javurcovae	javurcovae
Biscutella austriaca subsp. hungarica	subsp. hungarica
Biscutella laevigata subsp. angustifolia	subsp. angustifolia
Biscutella laevigata subsp. laevigata	subsp. laevigata
Bithynia leachii troschelii	troschelii
Brachionycha syriaca decipulae	decipulae
Brachydesmus Attemsii tenkesensis	tenkesensis
Brachygluta haematica sinuata	sinuata
Brachyiulus pusillus kaszabi	kaszabi
Brachypodium pinnatum subsp. pinnatum	subsp. pinnatum
Brassica elongata subsp. elongata	subsp. elongata
Brassica elongata subsp. integrifolia	subsp. integrifolia
Brassica napus subsp. napus	subsp. napus
Brassica napus subsp. rapifera	subsp. rapifera
Brassica rapa subsp. campestris	subsp. campestris
Brassica rapa subsp. olifera	subsp. olifera
Brassica rapa subsp. rapa	subsp. rapa
Bromus japonicus subsp. subquarrosus	subsp. subquarrosus
Bromus pannonicus subsp. monocladus	subsp. monocladus
Bromus pannonicus subsp. pannonicus	subsp. pannonicus
Bromus ramosus subsp. benekenii	subsp. benekenii
Bromus secalinus subsp. velutinus	subsp. velutinus
Bromus squarrosus subsp. danubialis	subsp. danubialis
Bromus squarrosus subsp. squarrosus	subsp. squarrosus
Bryaxis curtisi orientalis	orientalis
Buglossoides arvensis subsp. arvensis	subsp. arvensis
Buglossoides arvensis subsp. sibthorpiana	subsp. sibthorpiana
Bupleurum commutatum subsp. glaucocarpum	subsp. glaucocarpum
Bupleurum falcatum subsp. falcatum	subsp. falcatum
Bythinella austriaca hungarica	hungarica
Calamintha sylvatica subsp. sylvatica	subsp. sylvatica
Calliptamus barbarus parvus	parvus
Calosoma maderae auropunctatum	auropunctatum
Caltha palustris subsp. palustris	subsp. palustris
Camelina microcarpa subsp. microcarpa	subsp. microcarpa
Campanula glomerata subsp. glomerata	subsp. glomerata
Campanula moravica subsp. xylorrhiza	subsp. xylorrhiza
Campanula rotundifolia subsp. pinifolia	subsp. pinifolia
Campanula sibirica subsp. divergens	subsp. divergens
Campanula sibirica subsp. sibirica	subsp. sibirica
Camponotus caryae var. fallax	var. fallax
Candidula unifasciata soosiana	soosiana
Cannabis sativa subsp. sativa	subsp. sativa
Carabus zawadskyi ronayi	ronayi
Carassius auratus gibelio	gibelio
Cardamine pratensis subsp. paludosa	subsp. paludosa
Cardamine pratensis subsp. pratensis	subsp. pratensis
Carduus collinus subsp. glabrescens	subsp. glabrescens
Carduus crassifolius subsp. glaucus	subsp. glaucus
Carduus defloratus subsp. glaucus	subsp. glaucus
Carduus nutans subsp. leiophyllus	subsp. leiophyllus
Carex acuta subsp. intermedia	subsp. intermedia
Carex divulsa subsp. leersii	subsp. leersii
Carex praecox subsp. intermedia	subsp. intermedia
Carlina acaulis subsp. simplex	subsp. simplex
Carlina biebersteinii subsp. brevibracteata	subsp. brevibracteata
Cataglyphis bicolor nodus	nodus
Centaurea apiculata subsp. spinulosa	subsp. spinulosa
Centaurea arenaria subsp. pseudorhenana	subsp. pseudorhenana
Centaurea jacea subsp. angustifolia	subsp. angustifolia
Centaurea jacea subsp. macroptilon	subsp. macroptilon
Centaurea jacea subsp. pratensis	subsp. pratensis
Centaurea maculosa subsp. rhenana	subsp. rhenana
Centaurea montana subsp. mollis	subsp. mollis
Centaurea phrygia subsp. pseudophrygia	subsp. pseudophrygia
Centaurea scabiosa subsp. fritschii	subsp. fritschii
Centaurea scabiosa subsp. sadleriana	subsp. sadleriana
Centaurea scabiosa subsp. spinulosa	subsp. spinulosa
Centaurea scabiosa subsp. tematinensis	subsp. tematinensis
Centaurea stoebe subsp. micranthos	subsp. micranthos
Centaurea triumfetti subsp. dominii	subsp. dominii
Centaurium litorale subsp. uliginosum	subsp. uliginosum
Cerastium arvense subsp. ciliatum	subsp. ciliatum
Cerastium arvense subsp. matrense	subsp. matrense
Cerastium arvense subsp. molle	subsp. molle
Cerastium brachypetalum subsp. luridum	subsp. luridum
Cerastium diffusum subsp. subtetrandum	subsp. subtetrandum
Cerastium fontanum subsp. triviale	subsp. triviale
Cerastium fontanum subsp. vulgare	subsp. vulgare
Cerastium pumilum subsp. glutinosum	subsp. glutinosum
Cerastium pumilum subsp. obscurum	subsp. obscurum
Cerasus avium subsp. sylvestris	subsp. sylvestris
Cerasus mahaleb subsp. simonkaii	subsp. simonkaii
Ceratophyllum demersum subsp. pentacanthum	subsp. pentacanthum
Ceratophyllum demersum subsp. platyacanthum	subsp. platyacanthum
Ceratosoma caroli hungaricum	hungaricum
Ceratosoma caroli somloense	somloense
Cercopis sanguinea nicolausi	nicolausi
Ceterach officinarum subsp. bivalens	subsp. bivalens
Chalcalburnus chalcoides mento	mento
Chamaecytisus albus subsp. pallidus	subsp. pallidus
Chamaecytisus austriacus subsp. virescens	subsp. virescens
Chamaecytisus hirsutus subsp. ciliatus	subsp. ciliatus
Chamaecytisus triflorus subsp. leucotrichus	subsp. leucotrichus
Chanithus pannonicus fieberi	fieberi
Chariaspilates formosaria pannonicus	pannonicus
Charissa intermedia budensis	budensis
Charissa pullata kovacsi	kovacsi
Charissa variegata cavus	cavus
Chenopodium album subsp. amaranthicolor	subsp. amaranthicolor
Chenopodium album subsp. borbasii	subsp. borbasii
Chenopodium album subsp. microphyllum	subsp. microphyllum
Chenopodium berlandieri subsp. zschackei	subsp. zschackei
Chenopodium strictum subsp. striatiforme	subsp. striatiforme
Chloriona unicolor flava	flava
Chromatoiulus bosniensis cotinophilus	cotinophilus
Chromatoiulus projectus dioritanus	dioritanus
Chromatoiulus projectus Kochi	Kochi
Chromatoiulus transsylvanicus transdanubicus	transdanubicus
Cichorium endivia subsp. pumilum	subsp. pumilum
Circulifer guttulatus laeta	laeta
Cirsium eriophorum subsp. degenii	subsp. degenii
Cirsium vulgare subsp. sylvaticum	subsp. sylvaticum
Cixius heydenii notativertex	notativertex
Cobitis taenia elongatoides	elongatoides
Colchicum autumnale subsp. pannonicum	subsp. pannonicum
Coluber jugularis caspius	caspius
Cornus sanguinea subsp. australis	subsp. australis
Cortinarius albidus subsp. europaeus	subsp. europaeus
Cortusa matthioli subsp. sibirica	subsp. sibirica
Corvus corone cornix	cornix
Corydalis solida subsp. vajdae	subsp. vajdae
Cotinus coggygria subsp. pubescens	subsp. pubescens
Crataegus monogyna subsp. curvisepala	subsp. curvisepala
Crepis foetida subsp. rhoeadifolia	subsp. rhoeadifolia
Crepis mollis subsp. succisifolia	subsp. succisifolia
Crepis vesicaria subsp. haenseleri	subsp. haenseleri
Crepis vesicaria subsp. taraxacifolia	subsp. taraxacifolia
Crocus vernus subsp. albiflorus	subsp. albiflorus
Cryptocephalus aureolus illyricus	illyricus
Cuscuta australis subsp. cesattiana	subsp. cesattiana
Cuscuta epithymum subsp. trifolii	subsp. trifolii
Cuscuta scandens subsp. cesatiana	subsp. cesatiana
Cyanus montanus subsp. mollis	subsp. mollis
Cygnus columbianus bewickii	bewickii
Cytisus albus subsp. leucanthus	subsp. leucanthus
Cytisus hirsutus subsp. ciliatus	subsp. ciliatus
Cytisus hirsutus subsp. leucotrichus	subsp. leucotrichus
Cytisus hirsutus subsp. ratisbonensis	subsp. ratisbonensis
Cytisus supinus subsp. albus	subsp. albus
Cytisus supinus subsp. pallidus	subsp. pallidus
Dactylis glomerata subsp. aschersoniana	subsp. aschersoniana
Dactylorhiza incarnata subsp. incarnata	subsp. incarnata
Dactylorhiza incarnata subsp. ochroleuca	subsp. ochroleuca
Dactylorhiza incarnata subsp. pulchella	subsp. pulchella
Daphne cneorum subsp. arbuscoloides	subsp. arbuscoloides
Daphne cneorum subsp. cneorum	subsp. cneorum
Daucus carota subsp. carota	subsp. carota
Daucus carota subsp. sativus	subsp. sativus
Deschampsia cespitosa subsp. cespitosa	subsp. cespitosa
Deschampsia cespitosa subsp. parviflora	subsp. parviflora
Dianthus armeria subsp. armeria	subsp. armeria
Dianthus collinus subsp. collinus	subsp. collinus
Dianthus giganteiformis subsp. giganteiformis	subsp. giganteiformis
Dianthus giganteiformis subsp. pontederae	subsp. pontederae
Dianthus hungaricus subsp. pseudopraecox	subsp. pseudopraecox
Dianthus lumnitzeri subsp. pseudopraecox	subsp. pseudopraecox
Dianthus praecox subsp. lumnitzeri	subsp. lumnitzeri
Dianthus praecox subsp. praecox	subsp. praecox
Dianthus praecox subsp. pseudopraecox	subsp. pseudopraecox
Dianthus praecox subsp. regis-stephani	subsp. regis-stephani
Digitaria sanguinalis subsp. pectiniformis	subsp. pectiniformis
Dina lineata var. punctata	var. punctata
Dorcadion fulvum cervae	cervae
Dorcadion pedestre kaszabi	kaszabi
Doronicum hungaricum subsp. praehungaricum	subsp. praehungaricum
Dorycnium pentaphyllum agg.	agg.
Dorycnium pentaphyllum subsp. germanicum	subsp. germanicum
Draba lasiocarpa subsp. klasterskyi	subsp. klasterskyi
Drassodes lapidosus macer	macer
Dryopteris affinis subsp. borreri	subsp. borreri
Dyschirius chalibaeus gibbifrons	gibbifrons
Dyschirius lucidus obenbergeri	obenbergeri
Dyschirius substriatus priscus	priscus
Echinochloa oryzoides subsp. phyllopogon	subsp. phyllopogon
Echinops ritro subsp. ruthenicus	subsp. ruthenicus
Ectobius erythronotus nigricans	nigricans
Eleocharis palustris subsp. mamillata	subsp. mamillata
Elymus hispidus subsp. trichophorum	subsp. trichophorum
Emmelina jezonica pseudojezonica	pseudojezonica
Empoasca tessellata hungarica	hungarica
Epacromius coerulipes pannonicus	pannonicus
Epacromius tergestinus ponticus	ponticus
Ephedra distachya subsp. monostachya	subsp. monostachya
Epipactis helleborine subsp. viridis	subsp. viridis
Eragrostis lugens subsp. flaccida	subsp. flaccida
Erannis ankeraria bervaensis	bervaensis
Erigeron acer subsp. angulosus	subsp. angulosus
Erigeron annuus subsp. strigosus	subsp. strigosus
Erinaceus europaeus roumanicus	roumanicus
Erodium hoefftianum subsp. neilreichii	subsp. neilreichii
Erophila praecox subsp. glabrescens	subsp. glabrescens
Erophila verna subsp. majuscula	subsp. majuscula
Erophila verna subsp. microcarpa	subsp. microcarpa
Erophila verna subsp. obconica	subsp. obconica
Erophila verna subsp. praecox	subsp. praecox
Erophila verna subsp. spathulata	subsp. spathulata
Erophila verna subsp. stenocarpa	subsp. stenocarpa
Erysimum wittmannii subsp. pallidiflorum	subsp. pallidiflorum
Erythroneura rorida deleta	deleta
Esperiana daudebartii acicularis	acicularis
Euchorthippus pulvinatus pulvina	pulvina
Euconulus trochiformis praticola	praticola
Euidella speciosa obscura	obscura
Euphorbia esula subsp. tommasiniana	subsp. tommasiniana
Euphorbia seguieriana subsp. minor	subsp. minor
Euphorbia villosa subsp. carpatica	subsp. carpatica
Euphrasia kerneri subsp. praestiva	subsp. praestiva
Euphrasia officinalis subsp. pratensis	subsp. pratensis
Euphrasia officinalis subsp. rostkoviana	subsp. rostkoviana
Euphrasia picta subsp. kerneri	subsp. kerneri
Euphrasia stricta subsp. pseudosuecica	subsp. pseudosuecica
Eupithecia analoga europaea	europaea
Euplectus kirbyi revelierei	revelierei
Euscelis plebejus vernalis	vernalis
Fagotia acicularis audebardi	audebardi
Festuca amethystina subsp. tatrae	subsp. tatrae
Festuca glauca subsp. pallens	subsp. pallens
Festuca ovina subsp. valesiaca	subsp. valesiaca
Festuca rubra subsp. commutata	subsp. commutata
Festuca vaginata subsp. Dominii	subsp. Dominii
Foeniculum vulgare subsp. sativum	subsp. sativum
Fraxinus angustifolia subsp. danubialis	subsp. danubialis
Galanthus nivalis subsp. imperati	subsp. imperati
Galeopsis speciosa subsp. sulfurea	subsp. sulfurea
Galinsoga quadriradiata subsp. hispida	subsp. hispida
Galium album subsp. pycnotrichum	subsp. pycnotrichum
Galium austriacum subsp. balatonense	subsp. balatonense
Galium mollugo subsp. elatum	subsp. elatum
Galium mollugo subsp. erectum	subsp. erectum
Galium mollugo subsp. vertesense	subsp. vertesense
Galium palustre subsp. elongatum	subsp. elongatum
Gambusia affinis holbrooki	holbrooki
Genista tinctoria subsp. campestris	subsp. campestris
Genista tinctoria subsp. ovata	subsp. ovata
Gentiana polymorpha subsp. carpathica	subsp. carpathica
Gentianella amarella subsp. livonica	subsp. livonica
Gentianella austriaca subsp. praecox	subsp. praecox
Geranium molle subsp. stipulare	subsp. stipulare
Gervaisia noduligera hungarica	hungarica
Gladiolus communis subsp. byzantinus	subsp. byzantinus
Glechoma hederacea subsp. hirsuta	subsp. hirsuta
Glossiphonia complanata concolor	concolor
Glossiphonia complanata nebulosa	nebulosa
Glyceria fluitans subsp. poiformis	subsp. poiformis
Glyceria maxima subsp. arundinacea	subsp. arundinacea
Glycyrrhiza glabra subsp. glandulifera	subsp. glandulifera
Gnophos intermedius budensis	budensis
Helianthemum chamaecistus Mill. subsp. barbatum	Mill. subsp. barbatum
Helianthemum chamaecistus Mill. subsp. nummularium	Mill. subsp. nummularium
Helianthemum chamaecistus Mill. subsp. ovatum	Mill. subsp. ovatum
Helianthemum grandiflorum subsp. obscurum	subsp. obscurum
Helianthemum nummularium subsp. obscurum	subsp. obscurum
Helicigona planospira illyrica	illyrica
Helicopsis striata hungarica	hungarica
Heliopsis helianthoides subsp. scabra	subsp. scabra
Heracleum sphondylium subsp. chloranthum	subsp. chloranthum
Heracleum sphondylium subsp. sibiricum	subsp. sibiricum
Heracleum sphondylium subsp. sphondylium	subsp. sphondylium
Heracleum sphondylium subsp. transsilvanicum	subsp. transsilvanicum
Hesperis matronalis subsp. matronalis	subsp. matronalis
Hesperis matronalis subsp. nivea	subsp. nivea
Heteroporatia simile eremita	eremita
Hieracium bifidum subsp. bifidum	subsp. bifidum
Hieracium bupleuroides subsp. tatrea	subsp. tatrea
Hieracium lachenali subsp. lachenali	subsp. lachenali
Hieracium leptophyton subsp. bauhiniflorum	subsp. bauhiniflorum
Hieracium maculatum subsp. maculatum	subsp. maculatum
Himantoglossum hircinum subsp. caprinum	subsp. caprinum
Hippolais pallida elaeica	elaeica
Hippophaë rhamnoides subsp. rhamnoides	subsp. rhamnoides
Hybomitra nitidifrons confiformis	confiformis
Hylotelephium maximum subsp. scherfelii	subsp. scherfelii
Hypericum maculatum subsp. desentangsiiforme	subsp. desentangsiiforme
Hypericum perforatum subsp. angustifolium	subsp. angustifolium
Hypericum perforatum subsp. latifolium	subsp. latifolium
Hyssopus officinalis subsp. borealis	subsp. borealis
Iris flavissima subsp. arenaria	subsp. arenaria
Iris humilis subsp. arenaria	subsp. arenaria
Iris spuria subsp. ochroleuca	subsp. ochroleuca
Iris variegata subsp. leucographa	subsp. leucographa
Isatis tinctoria subsp. campestris	subsp. campestris
Isophya modestior stysi	stysi
Jovibarba globifera subsp. glabrescens	subsp. glabrescens
Jovibarba globifera subsp. hirta	subsp. hirta
Jovibarba hirta subsp. glabrescens	subsp. glabrescens
Juncus articulatus subsp. nigritellus	subsp. nigritellus
Knautia arvensis subsp. pratensis	subsp. pratensis
Knautia dipsacifolia subsp. turocensis	subsp. turocensis
Labidostomis lucida axillaris	axillaris
Lacerta vivipara pannonica	pannonica
Lactuca quercina subsp. chaixii	subsp. chaixii
Laemostenus terricola punctatus	punctatus
Lamiastrum galeobdolon subsp. montanum	subsp. montanum
Lamium galeobdolon subsp. argentatum	subsp. argentatum
Lamium galeobdolon subsp. montanum	subsp. montanum
Lappula squarrosa subsp. heteracantha	subsp. heteracantha
Lapsana communis subsp. grandiflora	subsp. grandiflora
Lapsana communis subsp. intermedia	subsp. intermedia
Lapsana subsp. communis	communis
Lasius flavus myops	myops
Lathyrus laevigatus subsp. transsylvanicus	subsp. transsylvanicus
Lathyrus silvester subsp. latifolius	subsp. latifolius
Leontodon autumnalis subsp. pratensis	subsp. pratensis
Leontodon hastilis subsp. glabratus	subsp. glabratus
Leontodon hispidus subsp. danubialis	subsp. danubialis
Leontodon hispidus subsp. glabratus	subsp. glabratus
Leontodon hispidus subsp. hastilis	subsp. hastilis
Leontodon hispidus subsp. pseudoincanus	subsp. pseudoincanus
Leontodon saxatilis subsp. taraxacoides	subsp. taraxacoides
Leptophyllum tatranum evae	evae
Leptothorax tuberum var. interrupta	var. interrupta
Leptura livida pecta	pecta
Leuciscus souffia agassizi	agassizi
Linaria biebersteinii subsp. strictissima	subsp. strictissima
Linaria genistifolia subsp. dalmatica	subsp. dalmatica
Linum catharticum subsp. suecicum	subsp. suecicum
Linum flavum subsp. hungaricum	subsp. hungaricum
Lithobius nigrifrons sulcatipes	sulcatipes
Lolium multiflorum subsp. italicum	subsp. italicum
Lolium temulentum subsp. speciosum	subsp. speciosum
Lotus corniculatus subsp. tenuifolius	subsp. tenuifolius
Lotus maritimus subsp. siliquosus	subsp. siliquosus
Lunaria annua subsp. pachyrhiza	subsp. pachyrhiza
Luzula campestris subsp. multiflora	subsp. multiflora
Lycia hirtaria pusztae	pusztae
Lycia hirtaria subalpina	subalpina
Lymnaea peregra ampla	ampla
Lymnaea peregra ovata	ovata
Macrosteles fascifrons lindbergi	lindbergi
Maculinea alcon xerophila	xerophila
Maculinea arion ligurica	ligurica
Malcolmia orsiniana subsp. angulifolia	subsp. angulifolia
Malus sylvestris subsp. paradisiaca	subsp. paradisiaca
Malva sylvestris subsp. mauritiana	subsp. mauritiana
Megaphyllum projectum Kochi	Kochi
Melampyrum arvense subsp. pseudobarbatum	subsp. pseudobarbatum
Melampyrum arvense subsp. Schinzii	subsp. Schinzii
Melampyrum arvense subsp. semmleri	subsp. semmleri
Melampyrum barbatum subsp. filarszkyanum	subsp. filarszkyanum
Melampyrum barbatum subsp. kitaibelii	subsp. kitaibelii
Melampyrum bihariense subsp. roemeri	subsp. roemeri
Melampyrum bihariense subsp. romanicum	subsp. romanicum
Melampyrum nemorosum subsp. debreceniense	subsp. debreceniense
Melampyrum nemorosum subsp. moravicum	subsp. moravicum
Melampyrum nemorosum subsp. silesiacum	subsp. silesiacum
Melampyrum pratense subsp. angustifrons	subsp. angustifrons
Melampyrum pratense subsp. commutatum	subsp. commutatum
Melampyrum pratense subsp. oligocladum	subsp. oligocladum
Melanophila picta decastigma	decastigma
Melica ciliata subsp. transsilvanica	subsp. transsilvanica
Melitaea telona kovacsi	kovacsi
Melittis melissophyllum subsp. carpatica	subsp. carpatica
Mentha aquatica subsp. schleicheri	subsp. schleicheri
Mentha arvensis subsp. agrestis	subsp. agrestis
Mentha arvensis subsp. austriaca	subsp. austriaca
Mentha arvensis subsp. parietariifolia	subsp. parietariifolia
Mentha ×dalmatica subsp. borbasiana	subsp. borbasiana
Mentha ×dalmatica subsp. dalmatica	subsp. dalmatica
Mentha ×dumetorum subsp. dumetorum	subsp. dumetorum
Mentha ×dumetorum subsp. pubescens	subsp. pubescens
Mentha longifolia subsp. grisella	subsp. grisella
Mentha longifolia subsp. mollissima	subsp. mollissima
Mentha ×verticillata subsp. acutifolia	subsp. acutifolia
Mentha ×verticillata subsp. palustris	subsp. palustris
Microchordeuma Brölemanni gebhardti	gebhardti
Microchordeuma transsilvanicum hungaricum	hungaricum
Microiulus laeticollis dudichi	dudichi
Microiulus laeticollis evae	evae
Minuartia hirsuta subsp. frutescens	subsp. frutescens
Minuartia laricifolia subsp. kitaibelii	subsp. kitaibelii
Minuartia striata subsp. kitaibelii	subsp. kitaibelii
Minuartia verna subsp. collina	subsp. collina
Minuartia verna subsp. ramossisima	subsp. ramossisima
Molinia arundinacea subsp. altissima	subsp. altissima
Molinia arundinacea subsp. litoralis	subsp. litoralis
Molinia caerulea subsp. arundinacea	subsp. arundinacea
Monotarsobius aeruginosus batorligetensis	batorligetensis
Monotropa hypopithys subsp. hypophegea	subsp. hypophegea
Montia fontana subsp. chondrosperma	subsp. chondrosperma
Muscari botryoides subsp. hungaricum	subsp. hungaricum
Mus musculus spicilegus	spicilegus
Mustela eversmanni hungarica	hungarica
Myosotis arvensis subsp. umbrata	subsp. umbrata
Myosotis laxa subsp. caespitosa	subsp. caespitosa
Myosotis palustris subsp. laxiflora	subsp. laxiflora
Myosotis palustris subsp. nemorosa	subsp. nemorosa
Myosotis palustris subsp. radicans	subsp. radicans
Myrmeleon formicarius nigrolabris	nigrolabris
Nacerdes dispar austriaca	austriaca
Narcissus poeticus subsp. angustifolius	subsp. angustifolius
Narcissus poeticus subsp. radiiflorus	subsp. radiiflorus
Narraga tessularia pannonica	pannonica
Netocia cuprea obscura	obscura
Noccaea caerulescens subsp. tatrense	subsp. tatrense
Odontopodisma schmidtii rubripes	rubripes
Onosma arenaria subsp. tuberculata	subsp. tuberculata
Onosma pseudoarenaria subsp. tuberculatum	subsp. tuberculatum
Ophrys fuciflora subsp. holubyana	subsp. holubyana
Orgyia ericae intermedia	intermedia
Origanum vulgare subsp. barcense	subsp. barcense
Ornithogalum orthophyllum subsp. kochii	subsp. kochii
Orobanche alba subsp. major	subsp. major
Orobanche ramosa subsp. nana	subsp. nana
Orobanche reticulata subsp. pallidiflora	subsp. pallidiflora
Orthetrum coerulescens anceps	anceps
Papaver dubium subsp. austromoravicum	subsp. austromoravicum
Papaver dubium subsp. lecoqii	subsp. lecoqii
Papaver rhoeas subsp. strigosum	subsp. strigosum
Paramesus nervosus caucasicus	caucasicus
Pardosa proxima tenuipes	tenuipes
Pedestredorcadion pedestre kaszabi	kaszabi
Persicaria lapathifolia subsp. brittingeri	subsp. brittingeri
Persicaria lapathifolia subsp. pallida	subsp. pallida
Phleum pratense subsp. hubbardii	subsp. hubbardii
Phoenicopterus ruber roseus	roseus
Phragmites australis subsp. maritima	subsp. maritima
Phragmites communis subsp. humilis	subsp. humilis
Picris hieracioides subsp. auriculata	subsp. auriculata
Picris hieracioides subsp. villarsii	subsp. villarsii
Pisum sativum subsp. biflorum	subsp. biflorum
Pisum sativum subsp. elatius	subsp. elatius
Plantago lanceolata subsp. eriophora	subsp. eriophora
Plantago major subsp. pleiosperma	subsp. pleiosperma
Plantago media subsp. stepposa	subsp. stepposa
Plebeius pylaon foticus	foticus
Poa bulbosa subsp. pseudoconcinna	subsp. pseudoconcinna
Poa compressa subsp. langana	subsp. langana
Poa palustris subsp. xerotica	subsp. xerotica
Poa pratensis subsp. angustifolia	subsp. angustifolia
Poa sterilis subsp. eusterilis	subsp. eusterilis
Pogonus persicus peisonis	peisonis
Polycnemum arvense subsp. majus	subsp. majus
Polygala comosa subsp. podolica	subsp. podolica
Polygonum lapathifolium subsp. brittingeri	subsp. brittingeri
Polygonum lapathifolium subsp. nodosum	subsp. nodosum
Polygonum lapathifolium subsp. pallidum	subsp. pallidum
Polygonum lapathifolium subsp. tomentosum	subsp. tomentosum
Porrhomma rosenhaueri hungaricum	hungaricum
Portulaca oleracea subsp. sativa	subsp. sativa
Potamogeton gramineus subsp. heterophyllus	subsp. heterophyllus
Potentilla arenaria subsp. tommasiniana	subsp. tommasiniana
Potentilla collina subsp. leucopolitana	subsp. leucopolitana
Potentilla collina subsp. wiemanniana	subsp. wiemanniana
Potentilla impolita subsp. dissecta	subsp. dissecta
Potentilla impolita subsp. magyarica	subsp. magyarica
Potentilla impolita subsp. pseudocalabra	subsp. pseudocalabra
Potentilla impolita subsp. wallrothii	subsp. wallrothii
Potentilla recta subsp. auriflora	subsp. auriflora
Potentilla thyrsiflora subsp. leucopolitanoides	subsp. leucopolitanoides
Potentilla thyrsiflora subsp. loczyana	subsp. loczyana
Prenolepis imparis nitens	nitens
Primula veris subsp. canescens	subsp. canescens
Primula veris subsp. columnae	subsp. columnae
Prunus cerasifera subsp. divaricata	subsp. divaricata
Prunus cerasus subsp. acida	subsp. acida
Prunus domestica subsp. domestica	subsp. domestica
Prunus spinosa subsp. fruticosa	subsp. fruticosa
Pseudolysimachion spurium subsp. foliosum	subsp. foliosum
Puccinellia distans subsp. limosa	subsp. limosa
Puccinellia festuciformis subsp. intermedia	subsp. intermedia
Pulmonaria montana subsp. mollis	subsp. mollis
Pulmonaria officinalis subsp. obscura	subsp. obscura
Pulsatilla grandis subsp. slavica	subsp. slavica
Pulsatilla halleri subsp. slavica	subsp. slavica
Pulsatilla pratensis subsp. zimmermanii	subsp. zimmermanii
Pyrus communis subsp. pyraster	subsp. pyraster
Pyrus malus subsp. sylvestris	subsp. sylvestris
Pyrus pyraster subsp. achras	subsp. achras
Radix peregra ampla	ampla
Radix peregra ovata	ovata
Ranunculus aconitifolius subsp. platanifolius	subsp. platanifolius
Ranunculus acris subsp. strigulosus	subsp. strigulosus
Ranunculus auricomus agg.	agg.
Ranunculus ficaria subsp. bulbifer	subsp. bulbifer
Ranunculus ficaria subsp. bulbilifer	subsp. bulbilifer
Ranunculus ficaria subsp. calthifolius	subsp. calthifolius
Ranunculus ficaria subsp. nudicaulis	subsp. nudicaulis
Ranunculus peltatus subsp. baudotii	subsp. baudotii
Ranunculus polyanthemos subsp. nemorosus	subsp. nemorosus
Ranunculus serpens subsp. nemorosus	subsp. nemorosus
Ranunculus trichophyllus subsp. rionii	subsp. rionii
Rhinanthus angustifolius subsp. aestivalis	subsp. aestivalis
Rhinanthus borbasii subsp. interfoliatus	subsp. interfoliatus
Rhinanthus borbasii subsp. rapaicsianus	subsp. rapaicsianus
Rhinanthus serotinus subsp. aestivalis	subsp. aestivalis
Rhodeus sericeus amarus	amarus
Rhyparioides flavidus metelkanus	metelkanus
Rosa caesia subsp. subcollina	subsp. subcollina
Rosa canina agg.	agg.
Rosa ×dumalis subsp. subcanina	subsp. subcanina
Rosa gallica subsp. leiostyla	subsp. leiostyla
Rosa jundzillii subsp. trachyphylla	subsp. trachyphylla
Rosa rubiginosa agg.	agg.
Rosa rubiginosa subsp. umbellata	subsp. umbellata
Rosa spinosissima subsp. pimpinellifolia	subsp. pimpinellifolia
Rosa tomentosa agg.	agg.
Rosa villosa subsp. sancti-andreae	subsp. sancti-andreae
Rubus fruticosus agg.	agg.
Rubus hirtus subsp. glaucinellus	subsp. glaucinellus
Rubus hirtus subsp. guentheri	subsp. guentheri
Rubus hirtus subsp. nigricatus	subsp. nigricatus
Rubus ser. Canescentes	Canescentes
Rubus ser. Discolores	Discolores
Rubus ser. Glandulosi	Glandulosi
Rubus ser. Hystrices	Hystrices
Rubus ser. Micantes	Micantes
Rubus ser. Radulae	Radulae
Rubus ser. Sylvatici	Sylvatici
Rubus ser. Vestiti	Vestiti
Rubus subsect. Hiemales	Hiemales
Rubus subsect. Rubus	Rubus
Rumex acetosella subsp. angiocarpus	subsp. angiocarpus
Rumex acetosella subsp. multifidus	subsp. multifidus
Ruscus aculeatus subsp. angustifolius	subsp. angustifolius
Rutilus frisii meidingeri	meidingeri
Rutilus pigus virgo	virgo
Sagina apetala subsp. erecta	subsp. erecta
Sagina sabuletorum subsp. macrocarpa	subsp. macrocarpa
Salix purpurea subsp. lambertiana	subsp. lambertiana
Salix repens subsp. rosmarinifolia	subsp. rosmarinifolia
Salix triandra subsp. amygdalina	subsp. amygdalina
Salvia pratensis subsp. dumetorum	subsp. dumetorum
Sanguisorba minor subsp. polygama	subsp. polygama
Saxicola torquata rubicola	rubicola
Scabiosa lucida subsp. pseudobanatica	subsp. pseudobanatica
Scaphoideus formosus confluens	confluens
Scilla bifolia agg.	agg.
Scirpus lacustris subsp. tabernaemontani	subsp. tabernaemontani
Scleranthus annuus subsp. collinus	subsp. collinus
Scleranthus annuus subsp. polycarpos	subsp. polycarpos
Scleranthus annuus subsp. verticillatus	subsp. verticillatus
Scleranthus perennis subsp. dichotomus	subsp. dichotomus
Scorzonera purpurea subsp. rosea	subsp. rosea
Scymnus fraxini wichmanni	wichmanni
Sedum acre subsp. krajinae	subsp. krajinae
Sedum acre subsp. neglectum	subsp. neglectum
Sedum rupestre subsp. glaucum	subsp. glaucum
Sedum telephium subsp. maximum	subsp. maximum
Sedum urvillei subsp. hillebrandtii	subsp. hillebrandtii
Sempervivum hirtum subsp. glabrescens	subsp. glabrescens
Senecio aquaticus subsp. barbareifolius	subsp. barbareifolius
Senecio congestus subsp. palustris	subsp. palustris
Senecio doria subsp. umbrosus	subsp. umbrosus
Senecio erucifolius subsp. eucifolius	subsp. eucifolius
Senecio nemorensis subsp. jacquinianus	subsp. jacquinianus
Seseli elatum subsp. austriacum	subsp. austriacum
Seseli elatum subsp. osseum	subsp. osseum
Sesleria caerulae subsp. calcarea	subsp. calcarea
Sesleria heufleriana subsp. hungarica	subsp. hungarica
Setaria viridis subsp. italica	subsp. italica
Sideritis montana subsp. comosa	subsp. comosa
Silene italica subsp. nemoralis	subsp. nemoralis
Silene latifolia subsp. alba	subsp. alba
Silene otites agg.	agg.
Silene otites subsp. hungarica	subsp. hungarica
Silene otites subsp. pseudotites	subsp. pseudotites
Silene vulgaris subsp. antelopum	subsp. antelopum
Silene vulgaris subsp. commutata	subsp. commutata
Solanum dulcamara subsp. pusztarum	subsp. pusztarum
Solanum nigrum subsp. humile	subsp. humile
Soldanella hungarica subsp. major	subsp. major
Sorbus aria agg.	agg.
Sorbus aria subsp. cretica	subsp. cretica
Sorbus mougeotii subsp. austriaca	subsp. austriaca
Sparganium emersum subsp. simplex	subsp. simplex
Sparganium ramosum subsp. neglectum	subsp. neglectum
Sparganium ramosum subsp. polyedrum	subsp. polyedrum
Spergula arvensis subsp. maxima	subsp. maxima
Spergula arvensis subsp. sativa	subsp. sativa
Stachys recta subsp. hirta	subsp. hirta
Stachys recta subsp. labiosa	subsp. labiosa
Stipa crassiculmis subsp. euroanatolica	subsp. euroanatolica
Stipa pennata subsp. eriocaulis	subsp. eriocaulis
Stipa pennata subsp. mediterranea	subsp. mediterranea
Suaeda maritima subsp. pannonica	subsp. pannonica
Suaeda maritima subsp. prostrata	subsp. prostrata
Suaeda maritima subsp. salsa	subsp. salsa
Symphytum officinale subsp. officinale	subsp. officinale
Symphytum tuberosum subsp. nodosum	subsp. nodosum
Synharmonia lyncea agnata	agnata
Tachys diabrachys bisbimaculatus	bisbimaculatus
Tanacetum corymbosum subsp. clusii	subsp. clusii
Tanacetum corymbosum subsp. corymbosum	subsp. corymbosum
Tanacetum corymbosum subsp. subcorymbosum	subsp. subcorymbosum
Taraxacum sect. Erythrosperma	Erythrosperma
Taraxacum sect. Ruderalia	Ruderalia
Tephroseris integrifolia subsp. capitata	subsp. capitata
Tephroseris integrifolia subsp. integrifolia	subsp. integrifolia
Tephroseris longifolia subsp. gaudinii	subsp. gaudinii
Tephroseris longifolia subsp. longifolia	subsp. longifolia
Teucrium montanum subsp. jailae	subsp. jailae
Teucrium montanum subsp. montanum	subsp. montanum
Teucrium montanum subsp. pannonicum	subsp. pannonicum
Teucrium montanum subsp. villosum	subsp. villosum
Thalictrum minus subsp. elatum	subsp. elatum
Thalictrum minus subsp. minus	subsp. minus
Thalictrum minus subsp. olympicum	subsp. olympicum
Thalictrum minus subsp. saxatile	subsp. saxatile
Thalictrum simplex subsp. simplex	subsp. simplex
Thlaspi caerulescens subsp. tatrense	subsp. tatrense
Thymus collinus subsp. austriacus	subsp. austriacus
Thymus glabrescens subsp. austriacus	subsp. austriacus
Thymus glabrescens subsp. degenianus	subsp. degenianus
Thymus glabrescens subsp. subhirsutus	subsp. subhirsutus
Thymus oenipontanus subsp. decipiens	subsp. decipiens
Thymus pannonicus subsp. auctus	subsp. auctus
Thymus praecox subsp. badensis	subsp. badensis
Thymus praecox subsp. clivorum	subsp. clivorum
Thymus pulegioides subsp. chamaedrys	subsp. chamaedrys
Thymus pulegioides subsp. pannonicus	subsp. pannonicus
Tilia platyphyllos subsp. caucasica	subsp. caucasica
Tilia platyphyllos subsp. grandifolia	subsp. grandifolia
Tilia platyphyllos subsp. rubra	subsp. rubra
Tilia rubra subsp. caucasica	subsp. caucasica
Torilis arvensis subsp. neglecta	subsp. neglecta
Torilis japonica subsp. ucranica	subsp. ucranica
Trachypteris picta decastigma	decastigma
Tragopogon pratensis subsp. orientalis	subsp. orientalis
Trapa natans subsp. muzzanensis	subsp. muzzanensis
Trapa natans subsp. verbanensis	subsp. verbanensis
Trichotichnus laevicollis carpathicus	carpathicus
Trifolium dubium subsp. dubium	subsp. dubium
Trifolium dubium subsp. microphyllum	subsp. microphyllum
Trifolium fragiferum subsp. fragiferum	subsp. fragiferum
Trifolium hybridum subsp. hybridum	subsp. hybridum
Trifolium incarnatum subsp. incarnatum	subsp. incarnatum
Trifolium incarnatum subsp. molinerii	subsp. molinerii
Trifolium medium subsp. medium	subsp. medium
Trifolium pratense subsp. pratense	subsp. pratense
Trifolium pratense subsp. serotinum	subsp. serotinum
Trisetum flavescens subsp. alpestre	subsp. alpestre
Triturus cristatus carnifex	carnifex
Triturus cristatus dobrogicus	dobrogicus
Trollius europaeus subsp. europaeus	subsp. europaeus
Trollius europaeus subsp. transsylvanicus	subsp. transsylvanicus
Turdus ruficollis atrogularis	atrogularis
Ulmus minor subsp. procera	subsp. procera
Unio crassus albensis	albensis
Unio crassus cytherea	cytherea
Unio crassus ondovensis	ondovensis
Unio pictorum balatonicus	balatonicus
Unio pictorum latirostris	latirostris
Unio pictorum plathyrhyncus	plathyrhyncus
Unio pictorum tisianus	tisianus
Unio tumidus solidus	solidus
Unio tumidus zelebori	zelebori
Vaccaria hispanica subsp. hispanica	subsp. hispanica
Valeriana collina subsp. angustifolia	subsp. angustifolia
Valeriana collina subsp. intermedia	subsp. intermedia
Valeriana officinalis subsp. collina	subsp. collina
Valeriana officinalis subsp. officinalis	subsp. officinalis
Valeriana officinalis subsp. sambucifolia	subsp. sambucifolia
Valeriana officinalis subsp. sarkanyi	subsp. sarkanyi
Valeriana officinalis subsp. tenuifolia	subsp. tenuifolia
Valeriana stolonifera subsp. angustifolia	subsp. angustifolia
Valerianella dentata subsp. dentata	subsp. dentata
Valerianella dentata subsp. eriosperma	subsp. eriosperma
Veratrum album subsp. album	subsp. album
Verbascum lychnitis subsp. kanitzianum	subsp. kanitzianum
Veronica anagallis-aquatica subsp. anagallis-aquatica	subsp. anagallis-aquatica
Veronica anagallis-aquatica subsp. divaricata	subsp. divaricata
Veronica austriaca agg.	agg.
Veronica austriaca subsp. austriaca	subsp. austriaca
Veronica austriaca subsp. bihariensis	subsp. bihariensis
Veronica austriaca subsp. jacquini	subsp. jacquini
Veronica austriaca subsp. teucrium	subsp. teucrium
Veronica chamaedrys subsp. chamaedrys	subsp. chamaedrys
Veronica longifolia subsp. maritima	subsp. maritima
Veronica paniculata subsp. foliosa	subsp. foliosa
Veronica serpyllifolia subsp. humifusa	subsp. humifusa
Veronica serpyllifolia subsp. nummularioides	subsp. nummularioides
Veronica spicata subsp. carpatica	subsp. carpatica
Veronica spicata subsp. incana	subsp. incana
Veronica spicata subsp. orchidea	subsp. orchidea
Veronica spicata subsp. spicata	subsp. spicata
Veronica spuria subsp. foliosa	subsp. foliosa
Veronica teucrium subsp. pseudochamaedrys	subsp. pseudochamaedrys
Veronica verna subsp. dillenii	subsp. dillenii
Vicia angustifolia subsp. angustifolia	subsp. angustifolia
Viola odorata subsp. wiedemannii	subsp. wiedemannii
Viola rupestris subsp. arenaria	subsp. arenaria
Viola tricolor subsp. arvensis	subsp. arvensis
Viola tricolor subsp. tricolor	subsp. tricolor
Viscum album subsp. album	subsp. album
Vitis vinifera subsp. sylvestris	subsp. sylvestris
Xanthium albinum subsp. albinum	subsp. albinum
Zannichellia palustris subsp. palustris	subsp. palustris
Barbus meridionalis petenyi	petenyi
Caspialosa kessleri pontica	pontica
Charadrius dubius curonicus	curonicus
Larus cachinnans michahellis	michahellis
Apus pallidus illyricus	illyricus
Motacilla alba yarrelli	yarrelli
Motacilla flava iberiae	iberiae
Acrocephalus arundinaceus arundindinaceus	arundindinaceus
Helianthemum nummularium subsp. grandiflorum	subsp. grandiflorum
Helianthemum nummularium subsp. ovatum	subsp. ovatum
Acer campestre subsp. marsicum	subsp. marsicum
Acer pseudoplatanus subsp. subobtusum	subsp. subobtusum
Anthemis tinctoria subsp. subtinctoria	subsp. subtinctoria
Crepis mollis subsp. hieracioides	subsp. hieracioides
Hieracium bifidum subsp. psammogenes	subsp. psammogenes
Hieracium lachenali subsp. acuminatum	subsp. acuminatum
Hieracium maculatum subsp. approximatum	subsp. approximatum
Hieracium maculatum subsp. arenarium	subsp. arenarium
Centaurea scabiosa subsp. vertesensis	subsp. vertesensis
Polygonum aviculare subsp. depressum	subsp. depressum
Polygonum aviculare subsp. rurivagum	subsp. rurivagum
Althaea officinalis subsp. pseudarmeniaca	subsp. pseudarmeniaca
Verbascum chaixii subsp. austriacum	subsp. austriacum
Odontites vernus subsp. serotinus	subsp. serotinus
Rhinanthus angustifolius subsp. grandiflorus	subsp. grandiflorus
Rhinanthus minor subsp. elatior	subsp. elatior
Rhinanthus minor subsp. stenophyllus	subsp. stenophyllus
Phragmites communis subsp. maritima	subsp. maritima
Acer campestre subsp. campestre	subsp. campestre
Acer pseudoplatanus subsp. pseudoplatanus	subsp. pseudoplatanus
Achillea distans subsp. distans	subsp. distans
Achillea millefolium subsp. millefolium	subsp. millefolium
Aconitum variegatum subsp. variegatum	subsp. variegatum
Aethusa cynapium subsp. cynapium	subsp. cynapium
Ajuga chamaepitys subsp. chamaepitys	subsp. chamaepitys
Allium scorodoprasum subsp. scorodoprasum	subsp. scorodoprasum
Angelica sylvestris subsp. sylvestris	subsp. sylvestris
Anthemis tinctoria subsp. tinctoria	subsp. tinctoria
Anthriscus cerefolium subsp. cerefolium	subsp. cerefolium
Anthyllis vulneraria subsp. vulneraria	subsp. vulneraria
Apium graveolens subsp. graveolens	subsp. graveolens
Arenaria serpyllifolia subsp. serpyllifolia	subsp. serpyllifolia
Arrhenatherum elatius subsp. elatius	subsp. elatius
Artemisia campestris subsp. campestris	subsp. campestris
Artemisia santonicum subsp. santonicum	subsp. santonicum
Asarum europaeum subsp. europaeum	subsp. europaeum
Asplenium trichomanes subsp. trichomanes	subsp. trichomanes
Asyneuma canescens subsp. canescens	subsp. canescens
Cardaminopsis arenosa subsp. arenosa	subsp. arenosa
Carduus collinus subsp. collinus	subsp. collinus
Carex acuta subsp. acuta	subsp. acuta
Carex divulsa subsp. divulsa	subsp. divulsa
Carex praecox subsp. praecox	subsp. praecox
Caucalis platycarpos subsp. platycarpos	subsp. platycarpos
Centaurea biebersteinii subsp. biebersteinii	subsp. biebersteinii
Centaurea jacea subsp. jacea	subsp. jacea
Centaurea macroptilon subsp. macroptilon	subsp. macroptilon
Centaurea nigrescens subsp. nigrescens	subsp. nigrescens
Centaurea scabiosa subsp. scabiosa	subsp. scabiosa
Centaurea stoebe subsp. stoebe	subsp. stoebe
Centaurea triumfetti subsp. triumfetti	subsp. triumfetti
Centaurium erythraea subsp. erythraea	subsp. erythraea
Cerastium arvense subsp. arvense	subsp. arvense
Cerastium brachypetalum subsp. brachypetalum	subsp. brachypetalum
Cerastium pumilum subsp. pumilum	subsp. pumilum
Chamaecytisus austriacus subsp. austriacus	subsp. austriacus
Chamaecytisus hirsutus subsp. hirsutus	subsp. hirsutus
Chamaecytisus supinus subsp. supinus	subsp. supinus
Chenopodium album subsp. album	subsp. album
Chenopodium strictum subsp. strictum	subsp. strictum
Cirsium eriophorum subsp. eriophorum	subsp. eriophorum
Cladium mariscus subsp. mariscus	subsp. mariscus
Consolida regalis subsp. regalis	subsp. regalis
Cornus sanguinea subsp. sanguinea	subsp. sanguinea
Cortusa matthioli subsp. matthioli	subsp. matthioli
Corydalis solida subsp. solida	subsp. solida
Crepis foetida subsp. foetida	subsp. foetida
Cuscuta epithymum subsp. epithymum	subsp. epithymum
Cuscuta scandens subsp. scandens	subsp. scandens
Dactylorhiza fuchsii subsp. fuchsii	subsp. fuchsii
Doronicum hungaricum subsp. hungaricum	subsp. hungaricum
Draba lasiocarpa subsp. lasiocarpa	subsp. lasiocarpa
Dryopteris affinis subsp. affinis	subsp. affinis
Ephedra distachya subsp. distachya	subsp. distachya
Epilobium tetragonum subsp. tetragonum	subsp. tetragonum
Epipactis atrorubens subsp. atrorubens	subsp. atrorubens
Epipactis leptochila subsp. leptochila	subsp. leptochila
Erigeron acris subsp. acris	subsp. acris
Erigeron annuus subsp. annuus	subsp. annuus
Erophila verna subsp. verna	subsp. verna
Euphorbia dulcis subsp. dulcis	subsp. dulcis
Euphorbia falcata subsp. falcata	subsp. falcata
Euphorbia platyphyllos subsp. platyphyllos	subsp. platyphyllos
Euphorbia seguieriana subsp. seguieriana	subsp. seguieriana
Fagus sylvatica subsp. sylvatica	subsp. sylvatica
Festuca amethystina subsp. amethystina	subsp. amethystina
Festuca rubra subsp. rubra	subsp. rubra
Galeobdolon luteum subsp. luteum	subsp. luteum
Galium album subsp. album	subsp. album
Galium boreale subsp. boreale	subsp. boreale
Galium glaucum subsp. glaucum	subsp. glaucum
Galium mollugo subsp. mollugo	subsp. mollugo
Galium parisiense subsp. parisiense	subsp. parisiense
Galium spurium subsp. spurium	subsp. spurium
Galium verum subsp. verum	subsp. verum
Genista tinctoria subsp. tinctoria	subsp. tinctoria
Geranium molle subsp. molle	subsp. molle
Glechoma hederacea subsp. hederacea	subsp. hederacea
Helianthemum nummularium subsp. nummularium	subsp. nummularium
Heliopsis helianthoides subsp. helianthoides	subsp. helianthoides
Holosteum umbellatum subsp. umbellatum	subsp. umbellatum
Hordeum murinum subsp. murinum	subsp. murinum
Hypericum maculatum subsp. maculatum	subsp. maculatum
Hypericum perforatum subsp. perforatum	subsp. perforatum
Inula salicina subsp. salicina	subsp. salicina
Iris graminea subsp. graminea	subsp. graminea
Iris variegata subsp. variegata	subsp. variegata
Isatis tinctoria subsp. tinctoria	subsp. tinctoria
Juncus articulatus subsp. articulatus	subsp. articulatus
Jurinea mollis subsp. mollis	subsp. mollis
Knautia arvensis subsp. arvensis	subsp. arvensis
Knautia dipsacifolia subsp. dipsacifolia	subsp. dipsacifolia
Lathyrus pannonicus subsp. pannonicus	subsp. pannonicus
Leontodon hispidus subsp. hispidus	subsp. hispidus
Leonurus cardiaca subsp. cardiaca	subsp. cardiaca
Lilium martagon subsp. martagon	subsp. martagon
Linaria genistifolia subsp. genistifolia	subsp. genistifolia
Linum catharticum subsp. catharticum	subsp. catharticum
Linum flavum subsp. flavum	subsp. flavum
Linum hirsutum subsp. hirsutum	subsp. hirsutum
Lotus corniculatus subsp. corniculatus	subsp. corniculatus
Lunaria annua subsp. annua	subsp. annua
Malva sylvestris subsp. sylvestris	subsp. sylvestris
Melampyrum arvense subsp. arvense	subsp. arvense
Melampyrum nemorosum subsp. nemorosum	subsp. nemorosum
Melampyrum pratense subsp. pratense	subsp. pratense
Melilotus altissimus subsp. altissimus	subsp. altissimus
Mentha longifolia subsp. longifolia	subsp. longifolia
Molinia caerulea subsp. caerulea	subsp. caerulea
Myosotis arvensis subsp. arvensis	subsp. arvensis
Narcissus poeticus subsp. poeticus	subsp. poeticus
Odontites vernus subsp. vernus	subsp. vernus
Ononis spinosa subsp. spinosa	subsp. spinosa
Onosma arenaria subsp. arenaria	subsp. arenaria
Ophrys fuciflora subsp. fuciflora	subsp. fuciflora
Orchis ustulata subsp. ustulata	subsp. ustulata
Ornithogalum orthophyllum subsp. orthophyllum	subsp. orthophyllum
Orobanche alba subsp. alba	subsp. alba
Orobanche ramosa subsp. ramosa	subsp. ramosa
Orobanche reticulata subsp. reticulata	subsp. reticulata
Paeonia officinalis subsp. officinalis	subsp. officinalis
Pastinaca sativa subsp. sativa	subsp. sativa
Persicaria lapathifolia subsp. lapathifolia	subsp. lapathifolia
Picris hieracioides subsp. hieracioides	subsp. hieracioides
Pimpinella saxifraga subsp. saxifraga	subsp. saxifraga
Pisum sativum subsp. sativum	subsp. sativum
Plantago lanceolata subsp. lanceolata	subsp. lanceolata
Plantago major subsp. major	subsp. major
Plantago media subsp. media	subsp. media
Poa compressa subsp. compressa	subsp. compressa
Polygala amara subsp. amara	subsp. amara
Polygala amarella subsp. amarella	subsp. amarella
Polygala comosa subsp. comosa	subsp. comosa
Polygala vulgaris subsp. vulgaris	subsp. vulgaris
Portulaca oleracea subsp. oleracea	subsp. oleracea
Potamogeton gramineus subsp. gramineus	subsp. gramineus
Potamogeton pectinatus subsp. pectinatus	subsp. pectinatus
Potentilla arenaria subsp. arenaria	subsp. arenaria
Potentilla argentea subsp. argentea	subsp. argentea
Potentilla impolita subsp. impolita	subsp. impolita
Potentilla recta subsp. recta	subsp. recta
Potentilla thyrsiflora subsp. thyrsiflora	subsp. thyrsiflora
Primula veris subsp. veris	subsp. veris
Prunus avium subsp. avium	subsp. avium
Prunus spinosa subsp. spinosa	subsp. spinosa
Pseudolysimachion spurium subsp. spurium	subsp. spurium
Pyrus communis subsp. communis	subsp. communis
Pyrus pyraster subsp. pyraster	subsp. pyraster
Ranunculus bulbosus subsp. bulbosus	subsp. bulbosus
Ranunculus polyanthemos subsp. polyanthemos	subsp. polyanthemos
Rhinanthus minor subsp. minor	subsp. minor
Rubus hirtus subsp. hirtus	subsp. hirtus
Rumex acetosella subsp. acetosella	subsp. acetosella
Rumex patientia subsp. patientia	subsp. patientia
Ruscus aculeatus subsp. aculeatus	subsp. aculeatus
Sagina apetala subsp. apetala	subsp. apetala
Salicornia prostrata subsp. prostrata	subsp. prostrata
Salix triandra subsp. triandra	subsp. triandra
Salvia pratensis subsp. pratensis	subsp. pratensis
Sanguisorba minor subsp. minor	subsp. minor
Scabiosa columbaria subsp. columbaria	subsp. columbaria
Scleranthus perennis subsp. perennis	subsp. perennis
Scorzonera purpurea subsp. purpurea	subsp. purpurea
Scrophularia umbrosa subsp. umbrosa	subsp. umbrosa
Senecio nemorensis subsp. nemorensis	subsp. nemorensis
Senecio paludosus subsp. paludosus	subsp. paludosus
Silene otites subsp. otites	subsp. otites
Silene vulgaris subsp. vulgaris	subsp. vulgaris
Sinapis alba subsp. alba	subsp. alba
Solanum nigrum subsp. nigrum	subsp. nigrum
Sorbus austriaca subsp. austriaca	subsp. austriaca
Sparganium emersum subsp. emersum	subsp. emersum
Sparganium erectum subsp. erectum	subsp. erectum
Spergula arvensis subsp. arvensis	subsp. arvensis
Stachys recta subsp. recta	subsp. recta
Thesium dollineri subsp. dollineri	subsp. dollineri
Thymus glabrescens subsp. glabrescens	subsp. glabrescens
Thymus praecox subsp. praecox	subsp. praecox
Tilia platyphyllos subsp. platyphyllos	subsp. platyphyllos
Torilis arvensis subsp. arvensis	subsp. arvensis
Torilis japonica subsp. japonica	subsp. japonica
Tragopogon dubius subsp. dubius	subsp. dubius
Trapa natans subsp. natans	subsp. natans
Tribulus terrestris subsp. terrestris	subsp. terrestris
Trifolium arvense subsp. arvense	subsp. arvense
\.


--
-- Name: ssp_subspecies ssp_subspecies_pkey; Type: CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_subspecies
    ADD CONSTRAINT ssp_subspecies_pkey PRIMARY KEY (species_name);


--
-- Name: ssp_subspecies ssp_subspecies_species_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_subspecies
    ADD CONSTRAINT ssp_subspecies_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.ssp_speciesnames(species_name);


--
-- PostgreSQL database dump complete
--

