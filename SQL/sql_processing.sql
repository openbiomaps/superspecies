ALTER TABLE anpi_biotika ADD COLUMN rank character varying (12);

-- infrataxon hozzáadása
UPDATE anpi_biotika SET rank='Infrataxon' FROM (
	WITH foo AS (
     	SELECT felteljes_fajnev as faj,
		unnest((regexp_split_to_array(felteljes_fajnev, E'\\s+'))[2:]) AS a, (regexp_split_to_array(felteljes_fajnev, E'\\s+'))[2:] AS b FROM anpi_biotika
	)
	SELECT DISTINCT ON (felteljes_fajnev,foo.a ) a,felteljes_fajnev,array_length(b,1) as b,count(a)
		FROM anpi_biotika 
		LEFT JOIN foo ON foo.faj=felteljes_fajnev
		GROUP BY a,felteljes_fajnev,b
	) foo
WHERE foo.felteljes_fajnev=anpi_biotika.felteljes_fajnev AND foo.count=2 AND b=2


ALTER TABLE anpi_biotika ADD COLUMN ssp character varying (32);
-- ssp hozzáadása
UPDATE anpi_biotika SET ssp=array_to_string(foo.a,' ') FROM (
	WITH foo AS (
     	SELECT felteljes_fajnev as faj,
		(regexp_split_to_array(felteljes_fajnev, E'\\s+'))[3:] AS a, (regexp_split_to_array(felteljes_fajnev, E'\\s+'))[2:] AS b FROM anpi_biotika
	)
	SELECT DISTINCT ON (felteljes_fajnev,foo.a ) a,felteljes_fajnev,array_length(b,1) as b,count(a)
		FROM anpi_biotika 
		LEFT JOIN foo ON foo.faj=felteljes_fajnev
		GROUP BY a,felteljes_fajnev,b
	) foo
WHERE foo.felteljes_fajnev=anpi_biotika.felteljes_fajnev AND b>1


-- speciesnames
CREATE TABLE public.01_ssp_speciesnames (
	species_id integer NOT NULL,
	species_name character varying(128) NOT NULL
);

CREATE SEQUENCE public.01_ssp_speciesnames_species_id_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER SEQUENCE public.01_ssp_speciesnames_species_id_seq OWNED BY public.01_ssp_speciesnames.species_id;
ALTER TABLE ONLY public.01_ssp_speciesnames ALTER COLUMN species_id SET DEFAULT nextval('public.01_ssp_speciesnames_species_id_seq'::regclass);
ALTER TABLE ONLY public.01_ssp_speciesnames
    ADD CONSTRAINT 01_ssp_speciesnames_pkey PRIMARY KEY (species_id);
ALTER TABLE ONLY public.01_ssp_speciesnames
	ADD CONSTRAINT 01_ssp_speciesnames_species_name_key UNIQUE (species_name);

INSERT INTO 01_ssp_speciesnames (species_name)
	SELECT latin_nev FROM bnpi_flora
ON CONFLICT (species_name) DO NOTHING;

INSERT INTO 01_ssp_speciesnames (species_name)
	SELECT latin_nev FROM bnpi_fauna
ON CONFLICT (species_name) DO NOTHING;

INSERT INTO 01_ssp_speciesnames (species_name)
	SELECT felteljes_fajnev FROM anpi_biotika WHERE felteljes_fajnev ~ '\s+' AND rank IS NULL
ON CONFLICT (species_name) DO NOTHING;

INSERT INTO 01_ssp_speciesnames (species_name)
	SELECT faj FROM vedett_fajok
ON CONFLICT (species_name) DO NOTHING;

INSERT INTO 01_ssp_speciesnames (species_name)
	SELECT fajnev FROM szitakotok
ON CONFLICT (species_name) DO NOTHING;


-- ssp_magyarnevek
CREATE TABLE public.ssp_magyarnevek (
    species_name character varying(64) NOT NULL,
    magyarnev character varying(64)[] NOT NULL
);
ALTER TABLE ONLY public.ssp_magyarnevek
    ADD CONSTRAINT ssp_magyarnevek_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_magyarnevek
    ADD CONSTRAINT ssp_magyarnevek_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);


INSERT INTO ssp_magyarnevek (species_name,magyarnev)
	SELECT latin_nev, ARRAY_AGG(magyar_nev) as magyar_nev 
	FROM 01_ssp_speciesnames a LEFT JOIN bnpi_fauna b ON a.species_name=b.latin_nev
	WHERE magyar_nev!='' AND magyar_nev!='nincs magyar neve'
	GROUP BY latin_nev
	ORDER BY latin_nev;

INSERT INTO ssp_magyarnevek (species_name,magyarnev)
	SELECT latin_nev, ARRAY_AGG(magyar_nev) as magyar_nev 
	FROM 01_ssp_speciesnames a LEFT JOIN bnpi_flora b ON a.species_name=b.latin_nev
	WHERE magyar_nev!='' AND magyar_nev!='nincs magyar neve'
	GROUP BY latin_nev
	ORDER BY latin_nev;

INSERT INTO ssp_magyarnevek (species_name,magyarnev)
	SELECT felteljes_fajnev,'{}' as m FROM anpi_biotika WHERE magyarnev IS NOT NULL AND rank IS NULL
ON CONFLICT (species_name) DO NOTHING;

UPDATE ssp_magyarnevek SET magyarnev = (select array_agg(distinct e) from unnest(magyarnev || magyar_nev) e)
FROM (
	SELECT felteljes_fajnev, ARRAY_AGG(magyarnev) as magyar_nev 
	FROM 01_ssp_speciesnames a LEFT JOIN anpi_biotika b ON a.species_name=b.felteljes_fajnev
	WHERE magyarnev!='' AND magyarnev IS NOT NULL
	GROUP BY felteljes_fajnev) foo
WHERE foo.felteljes_fajnev=species_name AND NOT magyarnev @> magyar_nev;

INSERT INTO ssp_magyarnevek (species_name,magyarnev)
	SELECT faj,'{}' as m FROM vedett_fajok WHERE faj_magyar IS NOT NULL
ON CONFLICT (species_name) DO NOTHING;

UPDATE ssp_magyarnevek SET magyarnev = (select array_agg(distinct e) from unnest(magyarnev || magyar_nev) e)
FROM (
	SELECT faj, ARRAY_AGG(faj_magyar) as magyar_nev 
	FROM 01_ssp_speciesnames a LEFT JOIN vedett_fajok b ON a.species_name=b.faj
	WHERE faj_magyar!='' AND faj_magyar IS NOT NULL
	GROUP BY faj) foo
WHERE foo.faj=species_name AND NOT magyarnev @> magyar_nev;

DELETE FROM ssp_magyarnevek WHERE magyarnev='{}';
DELETE FROM ssp_magyarnevek WHERE magyarnev='{--}';

-- ssp_shortnames
CREATE TABLE public.ssp_shortnames (
	species_name character varying(64) NOT NULL,
	shortname character varying(6) NOT NULL
);
ALTER TABLE ONLY public.ssp_shortnames
    ADD CONSTRAINT ssp_shortnames_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_shortnames
    ADD CONSTRAINT ssp_shortnames_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);

INSERT INTO ssp_shortnames (species_name,shortname)
	SELECT species_name,  upper(concat_ws('',substr(substring(species_name from '^(\w+) '),0,4),substr(substring(species_name from ' (\w+)'),0,4))) FROM 01_ssp_speciesnames
ON CONFLICT (species_name) DO NOTHING;

-- HURING kivételek
UPDATE ssp_shortnames SET shortname=upper('cornix') WHERE species_name='Corvus corone cornix';
UPDATE ssp_shortnames SET shortname=upper('cornix') WHERE species_name='Corvus cornix';
UPDATE ssp_shortnames SET shortname=upper('corrax') WHERE species_name='Corvus corax';
UPDATE ssp_shortnames SET shortname=upper('carris') WHERE species_name='Carduelis flavirostris';
UPDATE ssp_shortnames SET shortname=upper('porana') WHERE species_name='Porzana  porzana';
UPDATE ssp_shortnames SET shortname=upper('saxtra') WHERE species_name='Saxicola  rubetra';
UPDATE ssp_shortnames SET shortname=upper('saxola') WHERE species_name='Saxicola  rubicola';
UPDATE ssp_shortnames SET shortname=upper('acrola') WHERE species_name='Acrocephalus  paludicola';
UPDATE ssp_shortnames SET shortname=upper('acrris') WHERE species_name='Acrocephalus palustris';
UPDATE ssp_shortnames SET shortname=upper('carmea') WHERE species_name='Carduelis flammea';
UPDATE ssp_shortnames SET shortname=upper('carcab') WHERE species_name='Carduelis flammea cabaret';
UPDATE ssp_shortnames SET shortname=upper('pyrula') WHERE species_name='Pyrrhula pyrrhula';


-- ssp_vedettseg
CREATE TABLE public.ssp_vedettseg (
	species_name character varying(64) NOT NULL,
	vedettseg character varying(24) NOT NULL
);
ALTER TABLE ONLY public.ssp_vedettseg
	ADD CONSTRAINT ssp_vedettseg_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_vedettseg
    ADD CONSTRAINT ssp_vedettseg_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);

INSERT INTO ssp_vedettseg (species_name,vedettseg)
SELECT latin_nev, 
	CASE 
		WHEN vedkod=1 THEN 'Fokozottan védett'
		WHEN vedkod=2 THEN 'Védett'
		WHEN vedkod=3 THEN 'Nem védett / jelentős'
		WHEN vedkod=4 THEN 'Nem védett / indifferens'
		WHEN vedkod=5 THEN 'Inváziós'
	END
FROM bnpi_fauna
ON CONFLICT (species_name) DO NOTHING;

INSERT INTO ssp_vedettseg (species_name,vedettseg)
SELECT latin_nev, 
	CASE 
		WHEN tv=1 THEN 'Fokozottan védett'
		WHEN tv=2 THEN 'Védett'
		WHEN tv=3 THEN 'Nem védett / jelentős'
		WHEN tv=4 THEN 'Nem védett / indifferens'
		WHEN tv=5 THEN 'Inváziós'
	END
FROM bnpi_flora
ON CONFLICT (species_name) DO NOTHING;

INSERT INTO ssp_vedettseg (species_name,vedettseg)
	SELECT faj,vedettseg FROM vedett_fajok
	WHERE vedettseg IS NOT NULL
ON CONFLICT (species_name) DO NOTHING;

-- ssp_eszmeiertek
CREATE TABLE public.ssp_eszmeiertek (
	species_name character varying(64) NOT NULL,
	eszmei_ertek integer NOT NULL
);
ALTER TABLE ONLY public.ssp_eszmeiertek
	ADD CONSTRAINT ssp_eszmeiertek_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_eszmeiertek
    ADD CONSTRAINT ssp_eszmeiertek_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);

INSERT INTO ssp_eszmeiertek (species_name,eszmei_ertek)
	SELECT latin_nev,eszmei_ertek
	FROM bnpi_fauna WHERE eszmei_ertek IS NOT NULL
	ON CONFLICT (species_name) DO NOTHING;

INSERT INTO ssp_eszmeiertek (species_name,eszmei_ertek)
	SELECT latin_nev,protect
	FROM bnpi_flora WHERE protect IS NOT NULL
	ON CONFLICT (species_name) DO NOTHING;

-- ssp_subspecies
CREATE TABLE public.ssp_subspecies (
	species_name character varying(64) NOT NULL,
	subspecies character varying(32) NOT NULL
);
ALTER TABLE ONLY public.ssp_subspecies
	ADD CONSTRAINT ssp_subspecies_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_subspecies
    ADD CONSTRAINT ssp_subspecies_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);

INSERT INTO ssp_subspecies (species_name,subspecies)
	SELECT latin_nev, ssp FROM bnpi_flora
	WHERE ssp!='';

INSERT INTO ssp_subspecies (species_name,subspecies)
	SELECT latin_nev, ssp FROM bnpi_fauna
	WHERE ssp!='';

INSERT INTO ssp_subspecies (species_name,subspecies)
	SELECT felteljes_fajnev, ssp FROM anpi_biotika
	WHERE ssp!=''  AND rank IS NULL
ON CONFLICT (species_name) DO NOTHING;

-- ssp_namestatus
CREATE TABLE public.ssp_namestatus (
	species_name character varying(64) NOT NULL,
	namestatus character varying(128) NOT NULL
);
ALTER TABLE ONLY public.ssp_namestatus
	ADD CONSTRAINT ssp_namestatus_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_namestatus
    ADD CONSTRAINT ssp_namestatus_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);

INSERT INTO ssp_namestatus (species_name,namestatus)
	SELECT species_name,
	CASE 
		WHEN ervenyes=TRUE THEN 'accepted name'
	END as e
	FROM 01_ssp_speciesnames a LEFT JOIN anpi_biotika b ON species_name=felteljes_fajnev
	WHERE ervenyes IS NOT NULL AND ervenyes=TRUE AND b.rank IS NULL
ON CONFLICT (species_name) DO NOTHING;

INSERT INTO ssp_namestatus (species_name,namestatus)
	SELECT species_name,
        CONCAT_WS(' ',
        CASE 
		WHEN b.ervenyes=FALSE THEN 'synonym for'
	END, 	c.felteljes_fajnev )
	FROM 01_ssp_speciesnames a LEFT JOIN anpi_biotika b ON species_name=b.felteljes_fajnev
        LEFT JOIN anpi_biotika c ON b.erv_fkod=c.fkod
	WHERE b.ervenyes IS NOT NULL AND b.ervenyes=FALSE AND b.rank IS NULL
ON CONFLICT (species_name) DO NOTHING;

-- ssp_natura2000
CREATE TABLE public.ssp_natura2000 (
	species_name character varying(64) NOT NULL,
	values character varying(24) NOT NULL
);
ALTER TABLE ONLY public.ssp_natura2000
	ADD CONSTRAINT ssp_natura2000_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_natura2000
    ADD CONSTRAINT ssp_natura2000_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);

INSERT INTO ssp_natura2000 (species_name,values)
	SELECT faj,natura2000
	FROM vedett_fajok
	WHERE natura2000 IS NOT NULL;
--	SELECT species_name,natura2000
--	FROM  vedett_fajok a LEFT JOIN 01_ssp_speciesnames b ON species_name=faj
--	WHERE natura2000 IS NOT NULL;

-- ssp_international_agreements
CREATE TABLE public.ssp_international_agreements (
	species_name character varying(64) NOT NULL,
	values character varying(64)[] NOT NULL
);
ALTER TABLE ONLY public.ssp_international_agreements
	ADD CONSTRAINT ssp_international_agreements_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_international_agreements
    ADD CONSTRAINT ssp_international_agreements_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);

-- mvk
INSERT INTO ssp_international_agreements (species_name,values)
	SELECT latin_nev,ARRAY[CONCAT_WS(':','mvk',mvk)] FROM bnpi_flora
	WHERE mvk!='';
INSERT INTO ssp_international_agreements (species_name,values)
	SELECT latin_nev,ARRAY[CONCAT_WS(':','mvk',mvk)] FROM bnpi_fauna
	WHERE mvk!=''
	ON CONFLICT (species_name) DO NOTHING;

-- Bonn
INSERT INTO ssp_international_agreements (species_name,values)
	SELECT DISTINCT latin_nev,ARRAY[CONCAT_WS(':','bonn',bonn)] FROM bnpi_fauna b
	WHERE bonn!=''
ON CONFLICT (species_name) DO UPDATE 
	SET values = ssp_international_agreements.values || (SELECT  DISTINCT CONCAT_WS(':','bonn',bonn)::character varying FROM bnpi_fauna WHERE bonn!='' AND latin_nev=EXCLUDED.species_name);

-- Bern
INSERT INTO ssp_international_agreements (species_name,values)
	SELECT DISTINCT latin_nev,ARRAY[CONCAT_WS(':','bern',bern)] FROM bnpi_fauna b
	WHERE bern!=''
ON CONFLICT (species_name) DO UPDATE 
	SET values = ssp_international_agreements.values || (SELECT DISTINCT CONCAT_WS(':','bern',bern)::character varying FROM bnpi_fauna WHERE bern!='' AND latin_nev=EXCLUDED.species_name);
INSERT INTO ssp_international_agreements (species_name,values)
	SELECT DISTINCT latin_nev,ARRAY[CONCAT_WS(':','bern',bern)] FROM bnpi_flora b
	WHERE bern!=''
ON CONFLICT (species_name) DO UPDATE 
	SET values = ssp_international_agreements.values || (SELECT DISTINCT CONCAT_WS(':','bern',bern)::character varying FROM bnpi_flora WHERE bern!='' AND latin_nev=EXCLUDED.species_name);

-- Washington
INSERT INTO ssp_international_agreements (species_name,values)
	SELECT DISTINCT latin_nev,ARRAY[CONCAT_WS(':','washington',washington)] FROM bnpi_fauna b
	WHERE washington!=''
ON CONFLICT (species_name) DO UPDATE 
	SET values = ssp_international_agreements.values || (SELECT DISTINCT CONCAT_WS(':','washington',washington)::character varying FROM bnpi_fauna WHERE washington!='' AND latin_nev=EXCLUDED.species_name);
INSERT INTO ssp_international_agreements (species_name,values)
	SELECT DISTINCT latin_nev,ARRAY[CONCAT_WS(':','washington',washingtoni)] FROM bnpi_flora b
	WHERE washingtoni!=''
ON CONFLICT (species_name) DO UPDATE 
	SET values = ssp_international_agreements.values || (SELECT DISTINCT CONCAT_WS(':','washington',washingtoni)::character varying FROM bnpi_flora WHERE washingtoni!='' AND latin_nev=EXCLUDED.species_name);

-- IUCN
INSERT INTO ssp_international_agreements (species_name,values)
	SELECT DISTINCT latin_nev,ARRAY[CONCAT_WS(':','iucn',iucn)] FROM bnpi_flora b
	WHERE iucn!=''
ON CONFLICT (species_name) DO UPDATE 
	SET values = ssp_international_agreements.values || (SELECT DISTINCT CONCAT_WS(':','iucn',iucn)::character varying FROM bnpi_flora WHERE iucn!='' AND latin_nev=EXCLUDED.species_name);
INSERT INTO ssp_international_agreements (species_name,values)
	SELECT DISTINCT latin_nev,ARRAY[CONCAT_WS(':','iucn',iucn)] FROM bnpi_fauna b
	WHERE iucn!=''
ON CONFLICT (species_name) DO UPDATE 
	SET values = ssp_international_agreements.values || (SELECT DISTINCT CONCAT_WS(':','iucn',iucn)::character varying FROM bnpi_fauna WHERE iucn!='' AND latin_nev=EXCLUDED.species_name);

-- voros_lista
INSERT INTO ssp_international_agreements (species_name,values)
	SELECT DISTINCT latin_nev,ARRAY[CONCAT_WS(':','voros_lista',voros_lista)] FROM bnpi_flora b
	WHERE voros_lista!=''
ON CONFLICT (species_name) DO UPDATE 
	SET values = ssp_international_agreements.values || (SELECT DISTINCT CONCAT_WS(':','voros_lista',voros_lista)::character varying FROM bnpi_flora WHERE voros_lista!='' AND latin_nev=EXCLUDED.species_name);

-- corine
INSERT INTO ssp_international_agreements (species_name,values)
	SELECT DISTINCT latin_nev,ARRAY[CONCAT_WS(':','corine',corine)] FROM bnpi_flora b
	WHERE corine!=''
ON CONFLICT (species_name) DO UPDATE 
	SET values = ssp_international_agreements.values || (SELECT DISTINCT CONCAT_WS(':','corine',corine)::character varying FROM bnpi_flora WHERE corine!='' AND latin_nev=EXCLUDED.species_name);

-- ssp_odonata_hun
CREATE TABLE public.ssp_odonata_hun (
	species_name character varying(64) NOT NULL,
	status character varying(12) NOT NULL
);
ALTER TABLE ONLY public.ssp_odonata_hun
	ADD CONSTRAINT ssp_odonata_hun_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_odonata_hun
    ADD CONSTRAINT ssp_odonata_hun_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);

--beszúrja az új fajneveket
INSERT INTO ssp_odonata_hun (species_name,status)
	SELECT fajnev,status FROM szitakotok 
	WHERE fajnev!='';

--null fajneves változat, eltörik az ismeretlen fajneveken
--INSERT INTO ssp_odonata_hun (species_name,status)
--	SELECT species_name,status FROM szitakotok a LEFT JOIN 01_ssp_speciesnames b ON a.fajnev=b.species_name
--	WHERE fajnev!='';
	
-- ssp_taxon_hun
CREATE TABLE public.ssp_taxon_hun (
	species_name character varying(64) NOT NULL,
	taxon character varying(24) NOT NULL
);
ALTER TABLE ONLY public.ssp_taxon_hun
	ADD CONSTRAINT ssp_taxon_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_taxon_hun
    ADD CONSTRAINT ssp_taxon_hun_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);

INSERT INTO ssp_taxon_hun (species_name,taxon)
	SELECT faj,taxon FROM vedett_fajok WHERE taxon!='';

INSERT INTO ssp_taxon_hun (species_name,taxon) 
	SELECT latin_nev,'növények' FROM bnpi_flora
ON CONFLICT (species_name) DO NOTHING;

INSERT INTO ssp_taxon_hun (species_name,taxon) 
	SELECT felteljes_fajnev,lower(taxoncsoport) FROM anpi_biotika WHERE felteljes_fajnev ~ '\s+' AND rank IS NULL
ON CONFLICT (species_name) DO NOTHING;

-- ssp_auctor
CREATE TABLE public.ssp_auctor (
	species_name character varying(64) NOT NULL,
	auctor character varying(64) NOT NULL
);
ALTER TABLE ONLY public.ssp_auctor
	ADD CONSTRAINT ssp_auctor_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_auctor
    ADD CONSTRAINT ssp_auctor_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);

INSERT INTO ssp_auctor (species_name,auctor) 
	SELECT latin_nev,auctor FROM bnpi_fauna
ON CONFLICT (species_name) DO NOTHING;

INSERT INTO ssp_auctor (species_name,auctor) 
	SELECT latin_nev,auct FROM bnpi_flora
ON CONFLICT (species_name) DO NOTHING;

-- ssp_common_mispells
CREATE TABLE public.ssp_common_mispells (
	species_name character varying(64) NOT NULL,
	mispell character varying(64)[] NOT NULL
);
ALTER TABLE ONLY public.ssp_common_mispells
	ADD CONSTRAINT ssp_common_mispells_pkey PRIMARY KEY (species_name);
ALTER TABLE ONLY public.ssp_common_mispells
    ADD CONSTRAINT ssp_common_mispells_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.01_ssp_speciesnames(species_name);

INSERT INTO ssp_common_mispells (species_name,mispell) VALUES 
	('Platanthera bifolia','{Plathantera bifolia}'),
	('Hemaris tityus','{Hemaris tytius}'),
	('Cerambyx cerdo','{Cerambix cerdo}'),
	('Melitaea athalia','{Melithea athalia}');
