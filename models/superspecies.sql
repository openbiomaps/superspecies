
{{
config(
    materialized='table',
    post_hook=["{{ postgres.index(this, 'subspecies')}}",
               'CREATE INDEX trgm_idx_species_name ON {{ this }} USING gin (species_name gin_trgm_ops)',
               'CREATE INDEX trgm_idx_magyarnev ON {{ this }} USING gin (hun_magyarnev gin_trgm_ops)',
               "{{ postgres.index(this, 'namestatus')}}",
               "{{ postgres.index(this, 'hun_taxon')}}",
               "{{ postgres.index(this, 'hun_vedettseg')}}",],
    )
}}


SELECT s.species_name,
        subspecies,
        namestatus,
        auctor,
        shortname,
        array_to_string({{target.schema}}.f_array_unique(array_agg(m.mispell)),',') AS "common_mispells",
	g1.value AS bern,
	g2.value AS bonn,
	g3.value AS corine,
	g4.value AS iucn,
	g5.value AS mvk,
	g6.value AS voros_lista,
	g7.value AS washington, 
        --string_agg(b1.mispell,',') AS "hun_common_mispells",
        array_to_string({{target.schema}}.f_array_unique(array_agg(b1.mispell)),',') AS "hun_common_mispells",
        array_to_string({{target.schema}}.f_array_unique(array_agg(magyarnev)),',') AS "hun_magyarnev",
        --string_agg(magyarnev,',') AS "hun_magyarnev",
        vedettseg AS "hun_vedettseg",
        f.values AS "hun_natura2000",
        eszmei_ertek AS "hun_eszmeiertek",
        taxon AS "hun_taxon",
        j.species_name AS "hun_odonata", 
        b2.species_name AS "hun_odonata_newnames", 
        n.species_name AS "hun_fungi", 
        o.species_name AS "hun_orthoptera",
        j4.species_name AS "hun_chiroptera",
        j3.species_name AS "hun_coleoptera",
        j2.species_name AS "hun_lepidoptera",
        j1.species_name AS "hun_spider"

FROM ssp_speciesnames s
    -- General tables
        FULL JOIN ssp_subspecies c ON s.species_name=c.species_name
        FULL JOIN ssp_namestatus d ON s.species_name=d.species_name
        FULL JOIN ssp_auctor l ON s.species_name=l.species_name
        FULL JOIN ssp_iagreements_bern g1 ON s.species_name=g1.species_name
        FULL JOIN ssp_iagreements_bonn g2 ON s.species_name=g2.species_name
        FULL JOIN ssp_iagreements_corine g3 ON s.species_name=g3.species_name
        FULL JOIN ssp_iagreements_iucn g4 ON s.species_name=g4.species_name
        FULL JOIN ssp_iagreements_mvk g5 ON s.species_name=g5.species_name
        FULL JOIN ssp_iagreements_voros_lista g6 ON s.species_name=g6.species_name
        FULL JOIN ssp_iagreements_washington g7 ON s.species_name=g7.species_name
        FULL JOIN ssp_common_mispells m ON s.species_name=m.species_name
        FULL JOIN ssp_shortnames i ON s.species_name=i.species_name
    -- Hungary special tables
        FULL JOIN ssp_nationalnames_hun b ON s.species_name=b.species_name
        FULL JOIN ssp_nationalnames_hun_mispells b1 ON s.species_name=b1.species_name
        FULL JOIN ssp_nationalnames_hun_newodonata b2 ON s.species_name=b2.species_name
        FULL JOIN ssp_protected_hun e ON s.species_name=e.species_name
        FULL JOIN ssp_natura2000 f ON s.species_name=f.species_name
        FULL JOIN ssp_conservation_value_hun h ON s.species_name=h.species_name
        FULL JOIN ssp_taxon_hun k ON s.species_name=k.species_name
        FULL JOIN ssp_odonata_hun j ON s.species_name=j.species_name
        FULL JOIN ssp_spider_hun j1 ON s.species_name=j1.species_name
        FULL JOIN ssp_lepidoptera_hun j2 ON s.species_name=j2.species_name
        FULL JOIN ssp_coleoptera_hun j3 ON s.species_name=j3.species_name
        FULL JOIN ssp_chiroptera_hun j4 ON s.species_name=j4.species_name
        FULL JOIN ssp_fungi_hun n ON s.species_name=n.species_name
        FULL JOIN ssp_orthoptera_hun o ON s.species_name=o.species_name
    GROUP BY s.species_name,c.subspecies, d.namestatus, e.vedettseg, f.values, g1.value,g2.value, g3.value,g4.value, g5.value, g6.value, g7.value, 
    h.eszmei_ertek, i.shortname, j.species_name,k.taxon, l.auctor, n.species_name, o.species_name, b2.species_name, j4.species_name, j3.species_name,
    j2.species_name,j1.species_name

