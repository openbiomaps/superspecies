--
-- OBM database create from csv
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

CREATE TABLE bnpi_flora(
    obm_id integer NOT NULL,
    obm_geometry geometry,
    obm_datum timestamp with time zone DEFAULT now(),
    obm_uploading_id integer,
    obm_validation numeric,
    obm_comments text[],
    obm_modifier_id integer,
    obm_files_id character varying(32),
    CONSTRAINT enforce_dims_obm_geometry CHECK ((st_ndims(obm_geometry) = 2)),
    CONSTRAINT enforce_geotype_obm_geometry CHECK (((((geometrytype(obm_geometry) = 'POINT'::text) OR (geometrytype(obm_geometry) = 'LINE'::text)) OR (geometrytype(obm_geometry) = 'POLYGON'::text)) OR (obm_geometry IS NULL))),
    CONSTRAINT enforce_srid_obm_geometry CHECK ((st_srid(obm_geometry) = 4326))
);

ALTER TABLE bnpi_flora OWNER TO gisadmin;

--
-- Name: TABLE bnpi_flora; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON TABLE bnpi_flora IS 'user defined table:banm';

--
-- Name: bnpi_flora_obm_id_seq; Type: SEQUENCE; Schema: public; Owner: gisadmin
--

CREATE SEQUENCE bnpi_flora_obm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE bnpi_flora_obm_id_seq OWNER TO gisadmin;

--
-- Name: bnpi_flora_obm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gisadmin
--

ALTER SEQUENCE bnpi_flora_obm_id_seq OWNED BY bnpi_flora.obm_id;

--
-- Name: obm_id; Type: DEFAULT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY bnpi_flora ALTER COLUMN obm_id SET DEFAULT nextval('bnpi_flora_obm_id_seq'::regclass);

--
-- Name: bnpi_flora_pkey; Type: CONSTRAINT; Schema: public; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY bnpi_flora
    ADD CONSTRAINT bnpi_flora_pkey PRIMARY KEY (obm_id);

--
-- Name: obm_uploading_id; Type: FK CONSTRAINT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY bnpi_flora
    ADD CONSTRAINT obm_uploading_id FOREIGN KEY (obm_uploading_id) REFERENCES uploadings(id);

--
-- Name: bnpi_flora; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON TABLE bnpi_flora FROM PUBLIC;
REVOKE ALL ON TABLE bnpi_flora FROM gisadmin;
GRANT ALL ON TABLE bnpi_flora TO gisadmin;
GRANT ALL ON TABLE bnpi_flora TO bnpi_flora_admin;

--
-- Name: bnpi_flora_obm_id_seq; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE bnpi_flora_obm_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bnpi_flora_obm_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE bnpi_flora_obm_id_seq TO gisadmin;
GRANT SELECT,USAGE ON SEQUENCE bnpi_flora_obm_id_seq TO bnpi_flora_admin;

--
-- OBM add processed columns
--    

ALTER TABLE bnpi_flora ADD COLUMN "ogc_fid" smallint;
ALTER TABLE bnpi_flora ADD COLUMN "field_1" smallint;
ALTER TABLE bnpi_flora ADD COLUMN "objectid" smallint;
ALTER TABLE bnpi_flora ADD COLUMN "soo_no" character varying(17);
ALTER TABLE bnpi_flora ADD COLUMN "taxonkod" character varying(8);
ALTER TABLE bnpi_flora ADD COLUMN "genus" character varying(17);
ALTER TABLE bnpi_flora ADD COLUMN "species" character varying(24);
ALTER TABLE bnpi_flora ADD COLUMN "latin_nev" character varying(48);
ALTER TABLE bnpi_flora ADD COLUMN "auct" character varying(47);
ALTER TABLE bnpi_flora ADD COLUMN "nemzetsegnev" character varying(19);
ALTER TABLE bnpi_flora ADD COLUMN "fajnev" character varying(27);
ALTER TABLE bnpi_flora ADD COLUMN "magyar_nev" character varying(47);
ALTER TABLE bnpi_flora ADD COLUMN "ssp" character varying(27);
ALTER TABLE bnpi_flora ADD COLUMN "sspauct" character varying(31);
ALTER TABLE bnpi_flora ADD COLUMN "simon_1992_genus" character varying(17);
ALTER TABLE bnpi_flora ADD COLUMN "simon_1992_species" character varying(24);
ALTER TABLE bnpi_flora ADD COLUMN "simon_1992_ssp" character varying(27);
ALTER TABLE bnpi_flora ADD COLUMN "simon_1992_auct" character varying(47);
ALTER TABLE bnpi_flora ADD COLUMN "simon_1992_sspauct" character varying(31);
ALTER TABLE bnpi_flora ADD COLUMN "simon_2000_genus" character varying(17);
ALTER TABLE bnpi_flora ADD COLUMN "simon_2000_species" character varying(24);
ALTER TABLE bnpi_flora ADD COLUMN "simon_2000_ssp" character varying(27);
ALTER TABLE bnpi_flora ADD COLUMN "simon_2000_auct" character varying(47);
ALTER TABLE bnpi_flora ADD COLUMN "simon_2000_sspauct" character varying(31);
ALTER TABLE bnpi_flora ADD COLUMN "tir_genus" character varying(17);
ALTER TABLE bnpi_flora ADD COLUMN "tir_species" character varying(24);
ALTER TABLE bnpi_flora ADD COLUMN "tir_ssp" character varying(27);
ALTER TABLE bnpi_flora ADD COLUMN "tir_auct" character varying(54);
ALTER TABLE bnpi_flora ADD COLUMN "tir_sspauct" character varying(49);
ALTER TABLE bnpi_flora ADD COLUMN "fuvesz_no" character varying(17);
ALTER TABLE bnpi_flora ADD COLUMN "fuvesz_genus" character varying(17);
ALTER TABLE bnpi_flora ADD COLUMN "fuvesz_species" character varying(24);
ALTER TABLE bnpi_flora ADD COLUMN "fuvesz_ssp" character varying(23);
ALTER TABLE bnpi_flora ADD COLUMN "fuvesz_auct" character varying(50);
ALTER TABLE bnpi_flora ADD COLUMN "fuvesz_sspauct" character varying(61);
ALTER TABLE bnpi_flora ADD COLUMN "tv" smallint;
ALTER TABLE bnpi_flora ADD COLUMN "protect" character varying(8);
ALTER TABLE bnpi_flora ADD COLUMN "mvk" character varying(2);
ALTER TABLE bnpi_flora ADD COLUMN "voros_lista" character varying(2);
ALTER TABLE bnpi_flora ADD COLUMN "corine" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "bern" character varying(4);
ALTER TABLE bnpi_flora ADD COLUMN "iucn" character varying(2);
ALTER TABLE bnpi_flora ADD COLUMN "washingtoni" character varying(2);
ALTER TABLE bnpi_flora ADD COLUMN "natura2000" character varying(2);
ALTER TABLE bnpi_flora ADD COLUMN "tz" character varying(2);
ALTER TABLE bnpi_flora ADD COLUMN "wz" character varying(4);
ALTER TABLE bnpi_flora ADD COLUMN "rz" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "fs" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "ts" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "rs" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "ns" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "tb" character varying(4);
ALTER TABLE bnpi_flora ADD COLUMN "wb" character varying(4);
ALTER TABLE bnpi_flora ADD COLUMN "rb" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "nb" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "lb" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "kb" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "sb" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "life" character varying(6);
ALTER TABLE bnpi_flora ADD COLUMN "vert" character varying(3);
ALTER TABLE bnpi_flora ADD COLUMN "fle" character varying(4);
ALTER TABLE bnpi_flora ADD COLUMN "tvk" character varying(2);
ALTER TABLE bnpi_flora ADD COLUMN "sbt" character varying(2);
ALTER TABLE bnpi_flora ADD COLUMN "val" character varying(4);
ALTER TABLE bnpi_flora ADD COLUMN "familia" character varying(16);
ALTER TABLE bnpi_flora ADD COLUMN "bnpi_fontossag" smallint;
ALTER TABLE bnpi_flora ADD COLUMN "egyseg" smallint;
ALTER TABLE bnpi_flora ADD COLUMN "setup" smallint;
