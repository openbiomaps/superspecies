--
-- OBM database create from csv
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

CREATE TABLE bnpi_fauna(
    obm_id integer NOT NULL,
    obm_geometry geometry,
    obm_datum timestamp with time zone DEFAULT now(),
    obm_uploading_id integer,
    obm_validation numeric,
    obm_comments text[],
    obm_modifier_id integer,
    obm_files_id character varying(32),
    CONSTRAINT enforce_dims_obm_geometry CHECK ((st_ndims(obm_geometry) = 2)),
    CONSTRAINT enforce_geotype_obm_geometry CHECK (((((geometrytype(obm_geometry) = 'POINT'::text) OR (geometrytype(obm_geometry) = 'LINE'::text)) OR (geometrytype(obm_geometry) = 'POLYGON'::text)) OR (obm_geometry IS NULL))),
    CONSTRAINT enforce_srid_obm_geometry CHECK ((st_srid(obm_geometry) = 4326))
);

ALTER TABLE bnpi_fauna OWNER TO gisadmin;

--
-- Name: TABLE bnpi_fauna; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON TABLE bnpi_fauna IS 'user defined table:banm';

--
-- Name: bnpi_fauna_obm_id_seq; Type: SEQUENCE; Schema: public; Owner: gisadmin
--

CREATE SEQUENCE bnpi_fauna_obm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE bnpi_fauna_obm_id_seq OWNER TO gisadmin;

--
-- Name: bnpi_fauna_obm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gisadmin
--

ALTER SEQUENCE bnpi_fauna_obm_id_seq OWNED BY bnpi_fauna.obm_id;

--
-- Name: obm_id; Type: DEFAULT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY bnpi_fauna ALTER COLUMN obm_id SET DEFAULT nextval('bnpi_fauna_obm_id_seq'::regclass);

--
-- Name: bnpi_fauna_pkey; Type: CONSTRAINT; Schema: public; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY bnpi_fauna
    ADD CONSTRAINT bnpi_fauna_pkey PRIMARY KEY (obm_id);

--
-- Name: obm_uploading_id; Type: FK CONSTRAINT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY bnpi_fauna
    ADD CONSTRAINT obm_uploading_id FOREIGN KEY (obm_uploading_id) REFERENCES uploadings(id);

--
-- Name: bnpi_fauna; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON TABLE bnpi_fauna FROM PUBLIC;
REVOKE ALL ON TABLE bnpi_fauna FROM gisadmin;
GRANT ALL ON TABLE bnpi_fauna TO gisadmin;
GRANT ALL ON TABLE bnpi_fauna TO bnpi_fauna_admin;

--
-- Name: bnpi_fauna_obm_id_seq; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE bnpi_fauna_obm_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bnpi_fauna_obm_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE bnpi_fauna_obm_id_seq TO gisadmin;
GRANT SELECT,USAGE ON SEQUENCE bnpi_fauna_obm_id_seq TO bnpi_fauna_admin;

--
-- OBM add processed columns
--    

ALTER TABLE bnpi_fauna ADD COLUMN "ogc_fid" smallint;
ALTER TABLE bnpi_fauna ADD COLUMN "field_1" smallint;
ALTER TABLE bnpi_fauna ADD COLUMN "objectid" smallint;
ALTER TABLE bnpi_fauna ADD COLUMN "taxonkod" integer;
ALTER TABLE bnpi_fauna ADD COLUMN "genus" character varying(21);
ALTER TABLE bnpi_fauna ADD COLUMN "species" character varying(23);
ALTER TABLE bnpi_fauna ADD COLUMN "auctor" character varying(41);
ALTER TABLE bnpi_fauna ADD COLUMN "ssp" character varying(20);
ALTER TABLE bnpi_fauna ADD COLUMN "sspauct" character varying(35);
ALTER TABLE bnpi_fauna ADD COLUMN "latin_nev" character varying(45);
ALTER TABLE bnpi_fauna ADD COLUMN "fajnev_jogszabaly" text;
ALTER TABLE bnpi_fauna ADD COLUMN "staxon" smallint;
ALTER TABLE bnpi_fauna ADD COLUMN "staxon1" smallint;
ALTER TABLE bnpi_fauna ADD COLUMN "tipus" character varying(5);
ALTER TABLE bnpi_fauna ADD COLUMN "magyar_nev" character varying(42);
ALTER TABLE bnpi_fauna ADD COLUMN "gyakkod" character varying(3);
ALTER TABLE bnpi_fauna ADD COLUMN "vedkod" smallint;
ALTER TABLE bnpi_fauna ADD COLUMN "eszmei_ertek" character varying(9);
ALTER TABLE bnpi_fauna ADD COLUMN "nat2000_ii" character varying(1);
ALTER TABLE bnpi_fauna ADD COLUMN "nat2000_iv" character varying(1);
ALTER TABLE bnpi_fauna ADD COLUMN "nat2000_v" character varying(1);
ALTER TABLE bnpi_fauna ADD COLUMN "mvk" character varying(2);
ALTER TABLE bnpi_fauna ADD COLUMN "bonn" character varying(2);
ALTER TABLE bnpi_fauna ADD COLUMN "washington" character varying(3);
ALTER TABLE bnpi_fauna ADD COLUMN "bern" character varying(2);
ALTER TABLE bnpi_fauna ADD COLUMN "iucn" character varying(2);
ALTER TABLE bnpi_fauna ADD COLUMN "area" character varying(6);
ALTER TABLE bnpi_fauna ADD COLUMN "karpatmedence" character varying(6);
ALTER TABLE bnpi_fauna ADD COLUMN "megjegyzes" character varying(170);
