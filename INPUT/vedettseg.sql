--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Debian 11.7-0+deb10u1)
-- Dumped by pg_dump version 11.7 (Debian 11.7-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: vedett_fajok; Type: TABLE; Schema: public; Owner: gisadmin
--

CREATE TABLE public.vedett_fajok (
    faj character varying(128) NOT NULL,
    faj_magyar character varying(128),
    natura2000 character varying(32),
    vedettseg character varying(32),
    obm_uploading_id integer,
    obm_id integer NOT NULL,
    taxon character varying(32)
);


ALTER TABLE public.vedett_fajok OWNER TO gisadmin;

--
-- Name: TABLE vedett_fajok; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON TABLE public.vedett_fajok IS 'OBM Védett fajok listája';


--
-- Name: vedett_fajok_obm_id_seq; Type: SEQUENCE; Schema: public; Owner: gisadmin
--

CREATE SEQUENCE public.vedett_fajok_obm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vedett_fajok_obm_id_seq OWNER TO gisadmin;

--
-- Name: vedett_fajok_obm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gisadmin
--

ALTER SEQUENCE public.vedett_fajok_obm_id_seq OWNED BY public.vedett_fajok.obm_id;


--
-- Name: vedett_fajok obm_id; Type: DEFAULT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY public.vedett_fajok ALTER COLUMN obm_id SET DEFAULT nextval('public.vedett_fajok_obm_id_seq'::regclass);


--
-- Data for Name: vedett_fajok; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.vedett_fajok (faj, faj_magyar, natura2000, vedettseg, obm_uploading_id, obm_id, taxon) FROM stdin;
Fallopia x bohemica	cseh óriáskeserűfű	\N	Inváziós	1554	46121	növények
Bison bison	amerikai bölény	\N	Inváziós	1554	44211	emlősök
Callosciurus erythraeus	csinos tarkamókus	IAS	Inváziós	1554	44212	emlősök
Canis aureus	aranysakál	HD V	Nem védett	1554	44213	emlősök
Castor fiber	eurázsiai hód	HD II, IV	Védett	1554	44215	emlősök
Citellus citellus	ürge	HD II, IV	Fokozottan védett	1554	44216	emlősök
Crocidura leucodon	mezei cickány	\N	Védett	1554	44218	emlősök
Crocidura sp.	cickány	\N	Védett	1554	44219	emlősök
Crocidura suaveolens	keleti cickány	\N	Védett	1554	44220	emlősök
Dama dama	dámvad	\N	Inváziós	1554	44221	emlősök
Dryomys nitedula	erdei pele	HD IV	Fokozottan védett	1554	44222	emlősök
Erinaceus concolor	kis-ázsiai sün	\N	Védett	1554	44223	emlősök
Erinaceus europaeus	nyugati sün	\N	Védett	1554	44224	emlősök
Erinaceus roumanicus	keleti sün	\N	Védett	1554	44225	emlősök
Felis silvestris	vadmacska	HD IV	Fokozottan védett	1554	44226	emlősök
Glis glis	nagy pele	\N	Védett	1554	44227	emlősök
Herpestes javanicus	jávai mongúz	IAS	Inváziós	1554	44228	emlősök
Lutra lutra	vidra	HD II, IV	Fokozottan védett	1554	44229	emlősök
Lynx lynx	hiúz	HD II, IV	Fokozottan védett	1554	44230	emlősök
Martes martes	nyuszt	HD V	Védett	1554	44231	emlősök
Micromys minutus	törpeegér	\N	Védett	1554	44232	emlősök
Microtus agrestis	csalitjáró pocok	\N	Védett	1554	44233	emlősök
Microtus oeconomus	északi pocok	HD II kiemelt, IV	Fokozottan védett	1554	44234	emlősök
Microtus oeconomus mehelyi	északi pocok	HD II kiemelt, IV	Fokozottan védett	1554	44235	emlősök
Muntiacus reevesii	indiai muntjákszarvas	IAS	Inváziós	1554	44236	emlősök
Muscardinus avellanarius	mogyorós pele	HD IV	Védett	1554	44237	emlősök
Mustela erminea	hermelin	\N	Védett	1554	44238	emlősök
Mustela eversmanii	molnárgörény	HD II, IV	Védett	1554	44239	emlősök
Mustela eversmannii	molnárgörény	HD II, IV	Védett	1554	44240	emlősök
Mustela nivalis	menyét	\N	Védett	1554	44241	emlősök
Mustela putorius	házi görény	HD V	Nem védett	1554	44242	emlősök
Myocastor coypus	nutria	IAS	Inváziós	1554	44243	emlősök
Myoxus glis	nagy pele	\N	Védett	1554	44244	emlősök
Nannospalax leucodon	nyugati földikutya	\N	Fokozottan védett	1554	44245	emlősök
Nasua nasua	vörösorrú koáti (ormányosmedve)	IAS	Inváziós	1554	44246	emlősök
Nasua nasua	ormányos medve	\N	Inváziós	1554	44247	emlősök
Neomys anomalus	Miller-vízicickány	\N	Védett	1554	44248	emlősök
Neomys fodiens	közönséges vízicickány	\N	Védett	1554	44249	emlősök
Neomys sp.	vízicickány	\N	Védett	1554	44250	emlősök
Nyctereutes procyonoides	nyestkutya	IAS	Inváziós	1554	44251	emlősök
Ondatra zibethicus	pézsmapocok	IAS	Inváziós	1554	44252	emlősök
Ovis musimon	muflon	\N	Inváziós	1554	44253	emlősök
Procyon lotor	mosómedve	IAS	Inváziós	1554	44254	emlősök
Sciurus carolinensis	keleti szürkemókus	IAS	Inváziós	1554	44255	emlősök
Sciurus niger	vörös rókamókus	IAS	Inváziós	1554	44256	emlősök
Sciurus vulgaris	mókus	\N	Védett	1554	44257	emlősök
Sicista subtilis	csíkos szöcskeegér	HD II, IV	Fokozottan védett	1554	44258	emlősök
Sorex alpinus	havasi cickány	\N	Védett	1554	44259	emlősök
Sorex araneus	erdei cickány	\N	Védett	1554	44260	emlősök
Sorex minutus	törpecickány	\N	Védett	1554	44261	emlősök
Sorex sp.	cickány	\N	Védett	1554	44262	emlősök
Spalax leucodon	nyugati földikutya	\N	Fokozottan védett	1554	44263	emlősök
Spermophilus citellus	ürge	HD II, IV	Fokozottan védett	1554	44264	emlősök
Talpa europaea	vakond	\N	Védett	1554	44265	emlősök
Tamias sibiricus	szibériai csíkosmókus, vagy burunduk	IAS	Inváziós	1554	44266	emlősök
Ursus arctos	barna medve	\N	Fokozottan védett	1554	44267	emlősök
Barbastella barbastellus	nyugati piszedenevér (piszedenevér)	HD II, IV	Fokozottan védett	1554	44268	emlősök (denevér)
Barbastella sp.	piszedenevér	\N	Védett	1554	44269	emlősök (denevér)
Chiroptera	denevér	\N	Védett	1554	44270	emlősök (denevér)
Eptesicus nilssonii	északi késeidenevér	HD IV	Védett	1554	44271	emlősök (denevér)
Eptesicus serotinus	közönséges késeidenevér	HD IV	Védett	1554	44272	emlősök (denevér)
Eptesicus sp.	késeidenevér-fajok	\N	Védett	1554	44273	emlősök (denevér)
Hypsugo savii	alpesi denevér	HD IV	Védett	1554	44274	emlősök (denevér)
Miniopterus schreibersii	hosszúszárnyú denevér	HD II, IV	Fokozottan védett	1554	44275	emlősök (denevér)
Miniopterus sp.	denevér-fajok	\N	Védett	1554	44276	emlősök (denevér)
Myotis alcathoe	nimfadenevér	HD IV	Védett	1554	44277	emlősök (denevér)
Myotis bechsteinii	nagyfülű denevér	HD II, IV	Fokozottan védett	1554	44278	emlősök (denevér)
Myotis blythii	hegyesorrú denevér	HD II, IV	Védett	1554	44279	emlősök (denevér)
Myotis brandtii	Brandt-denevér	HD IV	Védett	1554	44280	emlősök (denevér)
Myotis dasycneme	tavi denevér	HD II, IV	Fokozottan védett	1554	44281	emlősök (denevér)
Myotis daubentonii	vízi denevér	HD IV	Védett	1554	44282	emlősök (denevér)
Myotis emarginatus	csonkafülű denevér	HD II, IV	Fokozottan védett	1554	44283	emlősök (denevér)
Myotis myotis	közönséges denevér	HD II, IV	Védett	1554	44284	emlősök (denevér)
Myotis mystacinus	bajuszos denevér	HD IV	Védett	1554	44285	emlősök (denevér)
Myotis nattereri	horgasszőrű denevér	HD IV	Védett	1554	44286	emlősök (denevér)
Myotis oxygnathus	hegyesorrú denevér	HD II, IV	Védett	1554	44287	emlősök (denevér)
Myotis sp.	denevér-fajok	\N	Védett	1554	44288	emlősök (denevér)
Cricetus cricetus	hörcsög	HD V	8_melleklet	1554	44217	emlősök
Nyctalus lasiopterus	óriás-koraidenevér	HD IV	Fokozottan védett	1554	44289	emlősök (denevér)
Nyctalus leisleri	szőröskarú koraidenevér	HD IV	Védett	1554	44290	emlősök (denevér)
Nyctalus noctula	rőt koraidenevér	HD IV	Védett	1554	44291	emlősök (denevér)
Nyctalus sp.	koraidenevér-fajok	\N	Védett	1554	44292	emlősök (denevér)
Pipistrellus kuhlii	fehérszélű törpedenevér	HD IV	Védett	1554	44293	emlősök (denevér)
Pipistrellus nathusii	durvavitorlájú törpedenevér	HD IV	Védett	1554	44294	emlősök (denevér)
Pipistrellus pipistrellus	közönséges törpedenevér	HD IV	Védett	1554	44295	emlősök (denevér)
Pipistrellus pygmaeus	szoprán törpedenevér	HD IV	Védett	1554	44296	emlősök (denevér)
Pipistrellus savii	alpesi denevér	HD IV	Védett	1554	44297	emlősök (denevér)
Pipistrellus sp.	törpedenevér-fajok	\N	Védett	1554	44298	emlősök (denevér)
Plecotus auritus	barna hosszúfülű-denevér	HD IV	Védett	1554	44299	emlősök (denevér)
Plecotus austriacus	szürke hosszúfülű-denevér	HD IV	Védett	1554	44300	emlősök (denevér)
Plecotus sp.	denevér-fajok	\N	Védett	1554	44301	emlősök (denevér)
Rhinolophus euryale	kereknyergű patkósdenevér	HD II, IV	Fokozottan védett	1554	44302	emlősök (denevér)
Rhinolophus ferrumequinum	nagy patkósdenevér	HD II, IV	Fokozottan védett	1554	44303	emlősök (denevér)
Rhinolophus hipposideros	kis patkósdenevér	HD II, IV	Védett	1554	44304	emlősök (denevér)
Rhinolophus sp.	denevér-fajok	\N	Védett	1554	44305	emlősök (denevér)
Vespertilio murinus	fehértorkú denevér	HD IV	Védett	1554	44306	emlősök (denevér)
Vespertilio sp.	denevér-fajok	\N	Védett	1554	44307	emlősök (denevér)
Abrostola agnorista	sziklalakó csalánbagoly	\N	Védett	1554	44308	lepkék
Acanthaclisis occitanica	pusztai hangyaleső	\N	Védett	1554	44309	recésszárnyúak
Acanthocinus aedilis	nagy daliáscincér	\N	Védett	1554	44310	bogarak
Acasis appensata	békabogyó-araszoló	\N	Védett	1554	44311	lepkék
Acherontia atropos	halálfejes lepke	\N	Védett	1554	44312	lepkék
Acinopus ammophilus	nagy aknásfutó	\N	Védett	1554	44313	bogarak
Acinopus picipes	kis aknásfutó	\N	Védett	1554	44314	bogarak
Acmaeodera degener	sárgafoltos zömökdíszbogár	\N	Védett	1554	44315	bogarak
Acmaeoderella mimonti	homoki zömökdíszbogár	\N	Védett	1554	44316	bogarak
Acrida hungarica	sisakos sáska	\N	Védett	1554	44317	egyenesszárnyúak
Acrida ungarica	sisakos sáska	\N	Védett	1554	44318	egyenesszárnyúak
Acrotylus longipes	önbeásó sáska	\N	Védett	1554	44319	egyenesszárnyúak
Actebia fugax	pusztai földibagoly	\N	Védett	1554	44320	lepkék
Actebia praecox	homoki zöldbagoly	\N	Védett	1554	44321	lepkék
Adscita geryon	ritka fémlepke	\N	Védett	1554	44322	lepkék
Aegosoma scabricorne	diófacincér	\N	Védett	1554	44323	bogarak
Aesalus scarabaeoides	szőrös szarvasbogár	\N	Védett	1554	44324	bogarak
Aeshna viridis	zöld acsa	HD IV	Fokozottan védett	1554	44325	szitakötők
Agapanthia maculicornis	harangvirágcincér	\N	Védett	1554	44326	bogarak
Agapanthiola leucaspis	magyar zsályacincér	\N	Védett	1554	44327	bogarak
Agdistis intermedia	magyar egytollúmoly	\N	Védett	1554	44328	lepkék
Aglais io	nappali pávaszem	\N	Védett	1554	44329	lepkék
Aglais urticae	kis rókalepke	\N	Védett	1554	44330	lepkék
Aglia tau	t-betűs pávaszem	\N	Védett	1554	44331	lepkék
Agnathus decoratus	éger-bíborbogár	\N	Védett	1554	44332	bogarak
Agnetina elegantula	díszes nagyálkérész	\N	Védett	1554	44333	álkérészek
Agrilus guerini	Guerin-karcsúdíszbogár	\N	Védett	1554	44334	bogarak
Aiolopus strepens	áttelelő sáska	\N	Védett	1554	44335	egyenesszárnyúak
Akimerus schaefferi	szilfacincér	\N	Védett	1554	44336	bogarak
Algedonia luctualis	fehérfoltos kormosmoly	\N	Védett	1554	44337	lepkék
Ametropus fragilis	vágotthasú kérész	\N	Védett	1554	44338	kérészek
Ampedus quadrisignatus	négyfoltos pattanó	\N	Védett	1554	44339	bogarak
Amphimelania holandri	szávai vízicsiga	\N	Védett	1554	44340	puhatestűek
Amphipoea lucens	törékeny lápibagoly	\N	Védett	1554	44341	lepkék
Amphipyra cinnamomea	ritka fahéjbagoly	\N	Védett	1554	44342	lepkék
Anaciaeschna isosceles	lápi acsa	\N	Védett	1554	44343	szitakötők
Anarta myrtilli	csarabbagoly	\N	Védett	1554	44344	lepkék
Anisus vorticulus	apró fillércsiga (kis lemezcsiga)	HD II, IV	Védett	1554	44345	puhatestűek
Anodonta woodiana	amuri kagyló	\N	Inváziós	1554	44346	puhatestűek
Anthaxia candens	cseresznyefa-virágdíszbogár	\N	Védett	1554	44347	bogarak
Anthaxia hackeri	Hacker-virágdíszbogár	\N	Védett	1554	44348	bogarak
Anthaxia hungarica	magyar virágdíszbogár	\N	Védett	1554	44349	bogarak
Anthaxia plicata	redős virágdíszbogár	\N	Védett	1554	44350	bogarak
Anthaxia tuerki	Türk-virágdíszbogár	\N	Védett	1554	44351	bogarak
Antheraea yamamai	tölgy-selyemlepke	\N	Inváziós	1554	44352	lepkék
Apamea platinea	platina-dudvabagoly	\N	Védett	1554	44353	lepkék
Apatania muliebris	páratlan alpesitegzes	\N	Védett	1554	44354	tegzesek
Apatura ilia	kis színjátszólepke	\N	Védett	1554	44355	lepkék
Apatura iris	nagy színjátszólepke	\N	Védett	1554	44356	lepkék
Apatura metis	magyar színjátszólepke	HD IV	Fokozottan védett	1554	44357	lepkék
Apatura sp.	színjátszólepke-fajok	\N	Védett	1554	44358	lepkék
Apaustis rupicola	szirti törpebagoly	\N	Védett	1554	44359	lepkék
Araneus grossus	óriás-keresztespók	\N	Védett	1554	44360	pókok
Archiearis notha	vörhenyes nappaliaraszoló	\N	Védett	1554	44361	lepkék
Archiearis parthenias	nagy nappaliaraszoló	\N	Védett	1554	44362	lepkék
Archiearis puella	kis nappaliaraszoló (kis tavasziaraszoló)	\N	Védett	1554	44363	lepkék
Arctia festiva	díszes medvelepke	\N	Védett	1554	44364	lepkék
Arcyptera fusca	szép hegyisáska	\N	Védett	1554	44365	egyenesszárnyúak
Arcyptera microptera	rövidszárnyú hegyisáska	\N	Védett	1554	44366	egyenesszárnyúak
Arethusana arethusa	közönséges szemeslepke	\N	Védett	1554	44367	lepkék
Argiope lobata	karéjos keresztespók	\N	Védett	1554	44368	pókok
Argynnis laodice	keleti gyöngyházlepke	\N	Védett	1554	44369	lepkék
Argynnis niobe	ibolya-gyöngyházlepke	\N	Védett	1554	44370	lepkék
Argynnis pandora	zöldes gyöngyházlepke	\N	Védett	1554	44371	lepkék
Argynnis paphia	nagy gyöngyházlepke	\N	Védett	1554	44372	lepkék
Argynnis paphia f. valesiana	nagy gyöngyházlepke	\N	Védett	1554	44373	lepkék
Argyroneta aquatica	búvárpók	\N	Védett	1554	44374	pókok
Arichanna melanaria	tőzegáfonya-araszoló	\N	Védett	1554	44375	lepkék
Aricia agestis	szerecsenboglárka	\N	Védett	1554	44376	lepkék
Aricia artaxerxes	bükki szerecsenboglárka	\N	Védett	1554	44377	lepkék
Aricia eumedon	gólyaorr-boglárka	\N	Védett	1554	44378	lepkék
Arion lusitanicus	spanyol meztelencsiga	\N	Inváziós	1554	44379	puhatestűek
Arion rufus	barna csupaszcsiga	\N	Inváziós	1554	44380	puhatestűek
Arion vulgaris	spanyol meztelencsiga	\N	Inváziós	1554	44381	puhatestűek
Aromia moschata	pézsmacincér	\N	Védett	1554	44382	bogarak
Arytrura musculus	keleti lápibagoly	HD II, IV	Fokozottan védett	1554	44383	lepkék
Astacus astacus	folyami rák	HD V	Védett	1554	44384	rákok
Astacus leptodactylus	kecskerák	\N	Védett	1554	44385	rákok
Asteroscopus syriacus	magyar őszi-fésűsbagoly (Kovács-bundásbagoly)	\N	Fokozottan védett	1554	44386	lepkék
Astiotes dilecta	nagy övesbagoly	\N	Védett	1554	44387	lepkék
Atethmia ambusta	körtebagoly	\N	Védett	1554	44388	lepkék
Atypus affinis	tölgyestorzpók	\N	Védett	1554	44389	pókok
Atypus muralis	kövi torzpók	\N	Védett	1554	44390	pókok
Atypus piceus	szurkos torzpók	\N	Védett	1554	44391	pókok
Austropotamobius torrentium	kövi rák	HD II kiemelt, V	Védett	1554	44392	rákok
Autographa bractea	aranyfoltú aranybagoly	\N	Védett	1554	44393	lepkék
Autographa jota	i-betűs aranybagoly	\N	Védett	1554	44394	lepkék
Batazonellus lacerticida	pompás útonállódarázs	\N	Védett	1554	44395	hártyásszárnyúak
Besdolus ventralis	Frivaldszky-álkérész	\N	Védett	1554	44396	álkérészek
Bielzia coerulans	kék meztelencsiga	\N	Védett	1554	44397	puhatestűek
Bittacus hageni	Hagen-csőrösrovar	\N	Védett	1554	44398	csőrösrovarok
Bittacus italicus	hosszúlábú csőrösrovar	\N	Védett	1554	44399	csőrösrovarok
Blaps abbreviata	déli bűzbogár	\N	Védett	1554	44400	bogarak
Bolbelasmus unicornis	szarvas álganéjtúró	HD II, IV	Védett	1554	44401	bogarak
Boloria euphrosyne	árvácska-gyöngyházlepke	\N	Védett	1554	44402	lepkék
Boloria selene	fakó gyöngyházlepke	\N	Védett	1554	44403	lepkék
Bombus argillaceus	délvidéki poszméh	\N	Védett	1554	44404	hártyásszárnyúak
Bombus confusus	bársonyos poszméh	\N	Védett	1554	44405	hártyásszárnyúak
Bombus fragrans	óriásposzméh	\N	Fokozottan védett	1554	44406	hártyásszárnyúak
Bombus humilis	változékony poszméh	\N	Védett	1554	44407	hártyásszárnyúak
Bombus laesus	rozsdássárga poszméh	\N	Védett	1554	44408	hártyásszárnyúak
Bombus muscorum	sárga poszméh	\N	Védett	1554	44409	hártyásszárnyúak
Bombus paradoxus	ritka poszméh	\N	Védett	1554	44410	hártyásszárnyúak
Bombus pomorum	vörhenyes poszméh	\N	Védett	1554	44411	hártyásszárnyúak
Bombus ruderatus	ligeti poszméh	\N	Védett	1554	44412	hártyásszárnyúak
Bombus silvarum	erdei poszméh	\N	Védett	1554	44413	hártyásszárnyúak
Bombus soroeensis	bogáncsposzméh	\N	Védett	1554	44414	hártyásszárnyúak
Bombus subterraneus	rövidszőrű poszméh	\N	Védett	1554	44415	hártyásszárnyúak
Borysthenia naticina	kúpos kerekszájúcsiga	\N	Védett	1554	44416	puhatestűek
Brachinus bipustulatus	kétfoltos pöfögőfutó	\N	Védett	1554	44417	bogarak
Brachycercus europaeus	rövidhátú paránykérész	\N	Védett	1554	44418	kérészek
Brachyptera braueri	Brauer-álkérész	\N	Védett	1554	44419	álkérészek
Brachysomus mihoki	bakonyi gyepormányos	\N	Védett	1554	44420	bogarak
Brenthis daphne	málna-gyöngyházlepke	\N	Védett	1554	44421	lepkék
Brenthis hecate	rozsdaszínű gyöngyházlepke	\N	Védett	1554	44422	lepkék
Brenthis ino	lápi gyöngyházlepke	\N	Védett	1554	44423	lepkék
Bythiospeum hungaricum	magyar vakcsiga	HD II kiemelt, IV	Védett	1554	44424	puhatestűek
Calamobius filum	hosszúcsápú szalmacincér	\N	Védett	1554	44425	bogarak
Callimorpha quadripunctaria	csíkos medvelepke	HD II kiemelt	Védett	1554	44426	lepkék
Callimorpha quadripunctata	csíkos medvelepke	HD II kiemelt	Védett	1554	44427	lepkék
Calliptamus barbarus	barbársáska	\N	Védett	1554	44428	egyenesszárnyúak
Calomera littoralis	sziki homokfutrinka	\N	Védett	1554	44429	bogarak
Calomera littoralis nemoralis	sziki homokfutrinka	\N	Védett	1554	44430	bogarak
Calopteryx virgo	kisasszony-szitakötő	\N	Védett	1554	44431	szitakötők
Calosoma auropunctatum	aranypettyes bábrabló	\N	Védett	1554	44432	bogarak
Calosoma inquisitor	kis bábrabló	\N	Védett	1554	44433	bogarak
Calosoma sycophanta	aranyos bábrabló	\N	Védett	1554	44434	bogarak
Calyciphora xanthodactyla	hangyabogáncs-tollasmoly	\N	Védett	1554	44435	lepkék
Camptogramma scripturata	vonalkás hegyiaraszoló	\N	Védett	1554	44436	lepkék
Camptorrhinus simplex	sima holttettetős-ormányos	\N	Védett	1554	44437	bogarak
Camptorrhinus statua	bordás holttettetős-ormányos	\N	Védett	1554	44438	bogarak
Capnodis tenebrionis	kökény-tükrösdíszbogár	\N	Védett	1554	44439	bogarak
Carabus arvensis	sokszínű futrinka	\N	Védett	1554	44440	bogarak
Carabus auronitens	aranyfutrinka	\N	Védett	1554	44441	bogarak
Carabus cancellatus	ragyás futrinka	\N	Védett	1554	44442	bogarak
Carabus cancellatus adeptus	ragyás futrinka	\N	Védett	1554	44443	bogarak
Carabus clathratus	szárnyas futrinka	\N	Védett	1554	44444	bogarak
Carabus convexus	selymes futrinka	\N	Védett	1554	44445	bogarak
Carabus convexus convexus	selymes futrinka	\N	Védett	1554	44446	bogarak
Carabus convexus kiskunensis	selymes futrinka	\N	Védett	1554	44447	bogarak
Carabus coriaceus	bőrfutrinka	\N	Védett	1554	44448	bogarak
Carabus germarii	dunántúli kékfutrinka	\N	Védett	1554	44449	bogarak
Carabus glabratus	domború futrinka	\N	Védett	1554	44450	bogarak
Carabus granulatus	mezei futrinka	\N	Védett	1554	44451	bogarak
Carabus hampei	sokbordás futrinka	HD II, IV	Fokozottan védett	1554	44452	bogarak
Carabus hortensis	aranypettyes futrinka	\N	Védett	1554	44453	bogarak
Carabus hungaricus	magyar futrinka	HD II, IV	Fokozottan védett	1554	44454	bogarak
Carabus intricatus	kék laposfutrinka	\N	Védett	1554	44455	bogarak
Carabus irregularis	alhavasi futrinka	\N	Védett	1554	44456	bogarak
Carabus linnei	Linné-futrinka	\N	Védett	1554	44457	bogarak
Carabus marginalis	szegélyes futrinka	\N	Védett	1554	44458	bogarak
Carabus montivagus	balkáni futrinka	\N	Védett	1554	44459	bogarak
Carabus nemoralis	ligeti futrinka	\N	Védett	1554	44460	bogarak
Carabus obsoletus	pompás futrinka	\N	Védett	1554	44462	bogarak
Carabus problematicus	láncos futrinka	\N	Védett	1554	44463	bogarak
Carabus scabriusculus	érdes futrinka	\N	Védett	1554	44464	bogarak
Carabus scabriusculus scabriusculus	érdes futrinka	\N	Védett	1554	44465	bogarak
Carabus scheidleri	változó futrinka	\N	Védett	1554	44466	bogarak
Carabus sp.	futrinka-fajok	\N	Védett	1554	44467	bogarak
Carabus ullrichi	rezes futrinka	\N	Védett	1554	44468	bogarak
Carabus variolosus	kárpáti vízifutrinka	HD II, IV	Fokozottan védett	1554	44469	bogarak
Carabus violaceus	keleti kékfutrinka	\N	Védett	1554	44470	bogarak
Carabus violaceus rakosiensis	keleti kékfutrinka	\N	Védett	1554	44471	bogarak
Carabus zawadszkii	zempléni futrinka	HD II, IV	Fokozottan védett	1554	44472	bogarak
Caradrina gilva	platina-vacakbagoly (ezüstös apróbagoly)	\N	Védett	1554	44473	lepkék
Carcharodus lavatherae	tisztesfű-busalepke	\N	Védett	1554	44474	lepkék
Cardoria scutellata	sarlófűcincér	\N	Védett	1554	44475	bogarak
Carinatodorcadion fulvum	barna gyalogcincér	\N	Védett	1554	44476	bogarak
Catocala conversa	füstös övesbagoly	\N	Védett	1554	44477	lepkék
Catocala dilecta	nagy övesbagoly	\N	Védett	1554	44478	lepkék
Catocala diversa	kőrisfa-övesbagoly	\N	Védett	1554	44479	lepkék
Catocala fraxini	kék övesbagoly	\N	Védett	1554	44480	lepkék
Catopta thrips	sztyeplepke	HD II, IV	Fokozottan védett	1554	44481	lepkék
Celes variabilis	változó sáska	\N	Védett	1554	44482	egyenesszárnyúak
Cellariopsis deubeli	keleti kristálycsiga	\N	Védett	1554	44483	puhatestűek
Cepaea hortensis	kerti csiga	\N	Védett	1554	44484	puhatestűek
Cepaea nemoralis	ligeti csiga	\N	Védett	1554	44485	puhatestűek
Ceraclea nigronervosa	szürke hosszúcsápú-tegzes	\N	Védett	1554	44486	tegzesek
Cerambyx cerdo	nagy hőscincér	HD II, IV	Védett	1554	44487	bogarak
Cerambyx miles	katonás hőscincér	\N	Védett	1554	44488	bogarak
Cerambyx scopolii	kis hőscincér	\N	Védett	1554	44489	bogarak
Cerambyx welensii	molyhos hőscincér	\N	Védett	1554	44490	bogarak
Chaetopteryx rugulosa	nyugati őszitegzes	\N	Fokozottan védett	1554	44491	tegzesek
Chalcophora mariana	nagy fenyvesdíszbogár	\N	Védett	1554	44492	bogarak
Chamaesphecia colpiformis	délvidéki szitkár	\N	Védett	1554	44493	lepkék
Chamaesphecia hungarica	magyar szitkár	\N	Védett	1554	44494	lepkék
Chamaesphecia palustris	mocsári szitkár	\N	Védett	1554	44495	lepkék
Chariaspilates formosarius	pompás lápiaraszoló (lápi tarkaaraszoló)	\N	Védett	1554	44496	lepkék
Charissa ambiguata	havasi sziklaaraszoló	\N	Védett	1554	44497	lepkék
Charissa intermedia	változó sziklaaraszoló	\N	Védett	1554	44498	lepkék
Charissa pullata	mészkő-sziklaaraszoló	\N	Védett	1554	44499	lepkék
Charissa variegata	tarka sziklaaraszoló	\N	Védett	1554	44500	lepkék
Chazara briseis	tarka szemeslepke	\N	Védett	1554	44501	lepkék
Cheironitis ungaricus	magyarföldi ganéjtúró	\N	Védett	1554	44502	bogarak
Chersotis cuprea	rézfényű földibagoly	\N	Védett	1554	44503	lepkék
Chersotis fimbriola	kökörcsinvirág-földibagoly	\N	Védett	1554	44504	lepkék
Chesias legatella	szürkés zanótaraszoló	\N	Védett	1554	44505	lepkék
Chesias rufata	vöröses zanótaraszoló	\N	Védett	1554	44506	lepkék
Chilostoma banaticum	bánáti csiga	HD II, IV	Védett	1554	44507	puhatestűek
Chlaenius decipiens	azúr bűzfutó	\N	Védett	1554	44508	bogarak
Chlaenius festivus	díszes bűzfutó	\N	Védett	1554	44509	bogarak
Chlaenius sulcicollis	szomorú bűzfutó	\N	Védett	1554	44510	bogarak
Chlorophorus hungaricus	magyar darázscincér	\N	Védett	1554	44511	bogarak
Chondrosoma fiduciarium	magyar ősziaraszoló	HD II, IV	Fokozottan védett	1554	44512	lepkék
Cicindela campestris	mezei homokfutrinka	\N	Védett	1554	44514	bogarak
Cicindela hybrida	öves homokfutrinka	\N	Védett	1554	44515	bogarak
Cicindela soluta	alföldi homokfutrinka	\N	Védett	1554	44516	bogarak
Cicindela soluta pannonica	alföldi homokfutrinka	\N	Védett	1554	44517	bogarak
Cicindela sylvicola	erdei homokfutrinka	\N	Védett	1554	44518	bogarak
Cicindela transversalis	nyugati homokfutrinka	\N	Védett	1554	44519	bogarak
Clytus tropicus	tölgy-darázscincér	\N	Védett	1554	44520	bogarak
Cochlodina cerata	sima orsócsiga	\N	Védett	1554	44521	puhatestűek
Cochlodina orthostoma	kis orsócsiga	\N	Védett	1554	44522	puhatestűek
Coenagrion hastulatum	lándzsás légivadász	\N	Védett	1554	44523	szitakötők
Coenagrion lunulatum	holdkék légivadász	\N	Védett	1554	44524	szitakötők
Coenagrion ornatum	díszes légivadász	HD II	Védett	1554	44525	szitakötők
Coenagrion scitulum	ritka légivadász	\N	Védett	1554	44526	szitakötők
Coenocalpe lapidata	csipkés iszalagaraszoló	\N	Védett	1554	44527	lepkék
Coenonympha oedippus	ezüstsávos szénalepke	HD II, IV	Fokozottan védett	1554	44528	lepkék
Coleophora hungariae	magyar zsákosmoly	\N	Védett	1554	44529	lepkék
Colias chrysotheme	dolomit-kéneslepke	\N	Fokozottan védett	1554	44530	lepkék
Colias myrmidone	narancsszínű kéneslepke (narancslepke)	HD II, IV	Fokozottan védett	1554	44531	lepkék
Copris lunaris	közönséges holdszarvú-ganéjtúró	\N	Védett	1554	44532	bogarak
Copris umbilicatus	déli holdszarvú-ganéjtúró	\N	Védett	1554	44533	bogarak
Coraebus fasciatus	szalagos díszbogár	\N	Védett	1554	44534	bogarak
Coraebus undatus	hullámos díszbogár	\N	Védett	1554	44535	bogarak
Corbicula fluminea	ázsiai kagyló	\N	Inváziós	1554	44536	puhatestűek
Cordulegaster bidentata	hegyiszitakötő	\N	Fokozottan védett	1554	44537	szitakötők
Cordulegaster heros	kétcsíkos hegyiszitakötő (ritka hegyiszitakötő)	HD II, IV	Fokozottan védett	1554	44538	szitakötők
Cornu aspersum	pettyes éticsiga	\N	Inváziós	1554	44539	puhatestűek
Cortodera flavimana	boglárkacincér	\N	Védett	1554	44540	bogarak
Cortodera holosericea	selymes cserjecincér	\N	Védett	1554	44541	bogarak
Coscinia cribraria	pettyes molyszövő	\N	Védett	1554	44542	lepkék
Creoleon plumbeus	rozsdás hangyaleső	\N	Védett	1554	44543	recésszárnyúak
Cryphaeus cornutus	szarvas gyászbogár	\N	Védett	1554	44544	bogarak
Cucullia argentea	ezüstfoltos csuklyásbagoly	\N	Védett	1554	44546	lepkék
Cucullia asteris	őszirózsa-csuklyásbagoly	\N	Védett	1554	44547	lepkék
Cucullia balsamitae	homoki-csuklyásbagoly	\N	Védett	1554	44548	lepkék
Cucullia campanulae	harangvirág-csuklyásbagoly	\N	Védett	1554	44549	lepkék
Cucullia chamomillae	székfű-csuklyásbagoly	\N	Védett	1554	44550	lepkék
Cucullia dracunculi	lilásszürke csuklyásbagoly	\N	Védett	1554	44551	lepkék
Cucullia formosa	díszes csuklyásbagoly	\N	Fokozottan védett	1554	44552	lepkék
Cucullia gnaphalii	aranyvessző-csuklyásbagoly	\N	Védett	1554	44553	lepkék
Cucullia lucifuga	hamvas csuklyásbagoly	\N	Védett	1554	44554	lepkék
Cucullia mixta	vértesi csuklyásbagoly	HD II, IV	Fokozottan védett	1554	44555	lepkék
Cucullia scopariae	seprősüröm-csuklyásbagoly	\N	Védett	1554	44556	lepkék
Cucullia tanaceti	vonalkás csuklyásbagoly	\N	Védett	1554	44557	lepkék
Cucullia xeranthemi	vasvirág-csuklyásbagoly	\N	Védett	1554	44558	lepkék
Cupido alcetas	palakék boglárka	\N	Védett	1554	44559	lepkék
Cupido decolorata	fakó boglárka	\N	Védett	1554	44560	lepkék
Cupido osiris	hegyi törpeboglárka	\N	Védett	1554	44561	lepkék
Cychrus attenuatus	sárgalábú cirpelőfutó	\N	Védett	1554	44562	bogarak
Cychrus caraboides	fekete cirpelőfutó	\N	Védett	1554	44563	bogarak
Cylindera arenaria	parti homokfutrinka	\N	Védett	1554	44564	bogarak
Cylindera germanica	parlagi homokfutrinka	\N	Védett	1554	44565	bogarak
Cymindis miliaris	kékes laposfutó	\N	Védett	1554	44566	bogarak
Dasypoda mixta	ritka gatyásméh	\N	Védett	1554	44567	hártyásszárnyúak
Dendroleon pantherinus	párducfoltos hangyaleső	\N	Védett	1554	44568	recésszárnyúak
Dermestoides sanguinicollis	hengeres szúfarkas	\N	Védett	1554	44569	bogarak
Deroplia genei	keskeny tölgycincér	\N	Védett	1554	44570	bogarak
Diabrotica virgifera virgifera	amerikai kukoricabogár	\N	Inváziós	1554	44571	bogarak
Diachrysia chryson	nagyfoltú aranybagoly	\N	Védett	1554	44572	lepkék
Diachrysia nadeja	szélessávú aranybagoly	\N	Védett	1554	44573	lepkék
Diachrysia zosimi	vérfű-aranybagoly (nemes aranybagoly)	\N	Védett	1554	44574	lepkék
Diaphora luctuosa	gyászos medvelepke	\N	Védett	1554	44575	lepkék
Diarsia dahlii	barnáspiros földibagoly	\N	Védett	1554	44576	lepkék
Dicerca aenea	nyárfa-díszbogár	\N	Védett	1554	44577	bogarak
Dicerca alni	égerfa-díszbogár	\N	Védett	1554	44578	bogarak
Dicerca berolinensis	bükkfa-díszbogár	\N	Védett	1554	44579	bogarak
Dicerca furcata	nyírfa-díszbogár	\N	Védett	1554	44580	bogarak
Dichagyris candelisequa	szigonyos földibagoly	\N	Védett	1554	44581	lepkék
Dichagyris musiva	szegélyes földibagoly	\N	Védett	1554	44582	lepkék
Dichonia aeruginea	hamvas tölgybagoly	\N	Védett	1554	44583	lepkék
Dicranura ulmi	szilfa-púposszövő	\N	Védett	1554	44584	lepkék
Dioszeghyana schmidtii	magyar tavaszi-fésűsbagoly (magyar barkabagoly)	HD II, IV	Fokozottan védett	1554	44585	lepkék
Discoloxia blomeri	szilfaaraszoló	\N	Védett	1554	44586	lepkék
Discus ruderatus	barna korongcsiga	\N	Védett	1554	44587	puhatestűek
Distoleon tetragrammicus	négyfoltos hangyaleső	\N	Védett	1554	44588	recésszárnyúak
Divaena haywardi	Hayward-sárgafűbagoly	\N	Védett	1554	44589	lepkék
Dixus clypeatus	nagyfejű futó	\N	Védett	1554	44590	bogarak
Dolomedes fimbriatus	szegélyes vidrapók	\N	Védett	1554	44591	pókok
Dolomedes plantarius	parti vidrapók	\N	Védett	1554	44592	pókok
Dorcadion decipiens	homoki gyalogcincér	\N	Védett	1554	44593	bogarak
Dorcadion fulvum	barna gyalogcincér	\N	Védett	1554	44594	bogarak
Dorcadion fulvum cervae	pusztai gyalogcincér	HD II, IV	Fokozottan védett	1554	44595	bogarak
Dorcadion fulvum fulvum	barna gyalogcincér	\N	Védett	1554	44596	bogarak
Dorcus parallelipipedus	kis szarvasbogár	\N	Védett	1554	44597	bogarak
Dreissena polymorpha	vándorkagyló	\N	Inváziós	1554	44598	puhatestűek
Drobacia banatica	bánáti csiga	HD II, IV	Védett	1554	44599	puhatestűek
Drusus trifidus	karsztforrástegzes	\N	Védett	1554	44600	tegzesek
Drymonia velitaris	hegyi púposszövő	\N	Védett	1554	44601	lepkék
Duvalius gebhardti	Gebhardt-vakfutinka	HD II, IV	Védett	1554	44602	bogarak
Duvalius hungaricus	magyar vakfutinka	HD II, IV	Védett	1554	44603	bogarak
Dyscia conspersaria	sziklaüröm-araszoló	\N	Védett	1554	44604	lepkék
Dytiscus latissimus	óriás-csíkbogár	\N	Védett	1554	44605	bogarak
Elater ferrugineus	fűzfapattanó	\N	Védett	1554	44606	bogarak
Ena montana	hegyi csavarcsiga	\N	Védett	1554	44607	puhatestűek
Enargia abluta	fakó nyárfabagoly	\N	Védett	1554	44608	lepkék
Endromis versicolora	tarkaszövő	\N	Védett	1554	44609	lepkék
Ennomos quercarius	molyhostölgy-levélaraszoló	\N	Védett	1554	44610	lepkék
Entephria caesiata	szürke hegyiaraszoló	\N	Védett	1554	44611	lepkék
Entephria cyanata	bükki hegyiaraszoló	\N	Fokozottan védett	1554	44612	lepkék
Epacromius coerulipes	pannon sáska	\N	Védett	1554	44613	egyenesszárnyúak
Epatolmis luctifera	füstös medvelepke	\N	Védett	1554	44614	lepkék
Ephemerella mesoleuca	fehérfoltú kérész	\N	Védett	1554	44615	kérészek
Ephesia diversa	kőrisfa-övesbagoly	\N	Védett	1554	44616	lepkék
Epimecia ustula	ördögszem-apróbagoly	\N	Védett	1554	44617	lepkék
Epirranthis diversata	rezgőnyár-araszoló	\N	Védett	1554	44618	lepkék
Epitheca bimaculata	kétfoltú szitakötő	\N	Védett	1554	44619	szitakötők
Erannis ankeraria	Anker-araszoló	HD II, IV	Fokozottan védett	1554	44620	lepkék
Erebia aethiops	közönséges szerecsenlepke	\N	Védett	1554	44621	lepkék
Erebia ligea	fehércsíkú szerecsenlepke	\N	Védett	1554	44622	lepkék
Erebia medusa	tavaszi szerecsenlepke	\N	Védett	1554	44623	lepkék
Eremodrina gilva	platina-vacakbagoly (ezüstös apróbagoly)	\N	Védett	1554	44624	lepkék
Eresus cinnaberinus	bikapók	\N	Védett	1554	44625	pókok
Ergates faber	ácscincér	\N	Védett	1554	44626	bogarak
Eriocheir sinensis	kínai gyapjasollósrák	IAS	Inváziós	1554	44627	rákok
Eriogaster catax	sárga gyapjasszövő	HD II, IV	Védett	1554	44628	lepkék
Eriogaster lanestris	tavaszi gyapjasszövő	\N	Védett	1554	44629	lepkék
Eriogaster rimicola	vörhenyes gyapjasszövő	\N	Védett	1554	44630	lepkék
Eublemma pannonica	magyar gyopárbagoly	\N	Fokozottan védett	1554	44631	lepkék
Eublemma rosea	rózsaszínű törpebagoly	\N	Védett	1554	44632	lepkék
Euchalcia modestoides	zöld aranybagoly	\N	Védett	1554	44633	lepkék
Euchalcia variabilis	sisakvirág-aranybagoly	\N	Védett	1554	44634	lepkék
Eucrostes indigenata	homoki zöldaraszoló	\N	Védett	1554	44635	lepkék
Eudia pavonia	kis pávaszem	\N	Védett	1554	44636	lepkék
Eudia spini	közepes pávaszem	\N	Védett	1554	44637	lepkék
Eumannia lepraria	sávos zuzmóaraszoló	\N	Védett	1554	44638	lepkék
Euphya scripturata	vonalkás hegyiaraszoló	\N	Védett	1554	44639	lepkék
Euphydryas aurinia	lápi tarkalepke	HD II	Védett	1554	44640	lepkék
Euphydryas maturna	díszes tarkalepke	HD II, IV	Védett	1554	44641	lepkék
Eupithecia actaeata	békabogyó-törpearaszoló	\N	Védett	1554	44642	lepkék
Eupithecia denticulata	sárgásszürke törpearaszoló	\N	Védett	1554	44643	lepkék
Eupithecia graphata	hangyabogáncs-törpearaszoló	\N	Védett	1554	44644	lepkék
Euplagia quadripunctaria	csíkos medvelepke	HD II kiemelt	Védett	1554	44645	lepkék
Eurylophella karelica	karéliai kérész	\N	Fokozottan védett	1554	44646	kérészek
Eurythyrea aurata	aranyos díszbogár	\N	Védett	1554	44647	bogarak
Eurythyrea quercus	tölgyfa-díszbogár	\N	Fokozottan védett	1554	44648	bogarak
Euxoa birivia	ezüstös földibagoly	\N	Védett	1554	44649	lepkék
Euxoa decora	selymes földibagoly	\N	Védett	1554	44650	lepkék
Euxoa distinguenda	keleti földibagoly	\N	Védett	1554	44651	lepkék
Euxoa hastifera	fehérsávos földibagoly	\N	Védett	1554	44652	lepkék
Euxoa vitta	dolomit-földibagoly	\N	Védett	1554	44653	lepkék
Everes alcetas	palakék boglárka	\N	Védett	1554	44654	lepkék
Everes decolorata	fakó boglárka	\N	Védett	1554	44655	lepkék
Fabula zollikoferi	óriás-szürkebagoly	\N	Védett	1554	44656	lepkék
Fagotia acicularis	folyamcsiga	\N	Védett	1554	44657	puhatestűek
Fagotia daudebartii	folyamcsiga	\N	Védett	1554	44658	puhatestűek
Fagotia daudebartii acicularis	dunai csúcsos szurokcsiga (folyamcsiga alfaj)	\N	Védett	1554	44659	puhatestűek
Fagotia esperi	pettyescsiga	\N	Védett	1554	44660	puhatestűek
Formica execta	fészeképítő hangyafajok védett fészkei	\N	5_melleklet	1554	44661	hártyásszárnyúak
Formica polyctenarufa	fészeképítő hangyafajok védett fészkei	\N	5_melleklet	1554	44662	hártyásszárnyúak
Formica pratensis	fészeképítő hangyafajok védett fészkei	\N	5_melleklet	1554	44663	hártyásszárnyúak
Formica pressilabris	fészeképítő hangyafajok védett fészkei	\N	5_melleklet	1554	44664	hártyásszárnyúak
Formica rufa	fészeképítő hangyafajok védett fészkei	\N	5_melleklet	1554	44665	hártyásszárnyúak
Formica sp.	fészeképítő hangyafajok védett fészkei	\N	5_melleklet	1554	44666	hártyásszárnyúak
Formica truncorum	fészeképítő hangyafajok védett fészkei	\N	5_melleklet	1554	44667	hártyásszárnyúak
Furcula bicuspis	apáca-púposszövő	\N	Védett	1554	44668	lepkék
Gagitodes sagittata	nyílfoltos tarkaaraszoló	\N	Védett	1554	44669	lepkék
Gampsocleis glabra	tőrös szöcske	\N	Védett	1554	44670	egyenesszárnyúak
Gasterocercus depressirostris	laposorrú ormányos	\N	Védett	1554	44671	bogarak
Gerris najas	nagy molnárpoloska	\N	Védett	1554	44672	poloskák
Glaucopsyche alexis	nagyszemes boglárka	\N	Védett	1554	44673	lepkék
Glyphipterix loricatella	budai szakállasmoly	HD II, IV	Fokozottan védett	1554	44674	lepkék
Gnorimus variabilis	nyolcpettyes virágbogár	\N	Védett	1554	44675	bogarak
Gomphus vulgatissimus	feketelábú szitakötő	\N	Védett	1554	44676	szitakötők
Gonepteryx rhamni	citromlepke	\N	Védett	1554	44677	lepkék
Gortyna borelii	nagy szikibagoly	HD II, IV	Fokozottan védett	1554	44678	lepkék
Gortyna borelii lunata	nagy szikibagoly	HD II, IV	Fokozottan védett	1554	44679	lepkék
Graphoderus bilineatus	széles tavicsíkbogár	HD II, IV	Fokozottan védett	1554	44680	bogarak
Haitia acuta	\N	\N	Inváziós	1554	44681	puhatestűek
Harmonia axyridis	harlekinkatica	\N	Inváziós	1554	44682	bogarak
Helix lucorum	fehérsávos éticsiga	\N	Inváziós	1554	44683	puhatestűek
Helix lutescens	ugarcsiga	\N	Védett	1554	44684	puhatestűek
Helix pomatia	éti csiga	HD V	Védett	1554	44685	puhatestűek
Hemaris fuciformis	dongószender	\N	Védett	1554	44686	lepkék
Hemaris tityus	pöszörszender	\N	Védett	1554	44687	lepkék
Herophila tristis	selymes alkonycincér	\N	Védett	1554	44688	bogarak
Herpes porcellus	bütyköshátú ormányos	\N	Védett	1554	44689	bogarak
Heteropterus morpheus	tükrös busalepke	\N	Védett	1554	44690	lepkék
Hipparchia semele	barna szemeslepke	\N	Védett	1554	44691	lepkék
Hipparchia statilinus	homoki szemeslepke	\N	Védett	1554	44692	lepkék
Hirudo medicinalis	orvosi pióca	HD V	8_melleklet	1554	44693	gyűrűsférgek
Homarus americanus	amerikai homár	\N	Inváziós	1554	44694	rákok
Hycleus tenerus	kis hólyaghúzó	\N	Védett	1554	44695	bogarak
Hydraecia petasitis	nagy vízibagoly	\N	Védett	1554	44696	lepkék
Hydrelia sylvata	havasi lápiaraszoló	\N	Védett	1554	44697	lepkék
Hydria undulata	hullámvonalas araszoló	\N	Védett	1554	44698	lepkék
Hygromia kovacsi	dobozi pikkelyescsiga	HD II, IV	Fokozottan védett	1554	44699	puhatestűek
Hygromia transsylvanica	erdélyi pikkelyescsiga	\N	Védett	1554	44700	puhatestűek
Hyles galii	galajszender	\N	Védett	1554	44701	lepkék
Hypantria cunea	amerikai fehér medvelepke v. amerikai szövőlepke	\N	Inváziós	1554	44703	lepkék
Hypenodes pannonica	pannon karcsúbagoly (lápi karcsúbagoly)	\N	Védett	1554	44704	lepkék
Hyponephele lupina	homoki ökörszemlepke	\N	Védett	1554	44705	lepkék
Hyponephele lycaon	erdei ökörszemlepke	\N	Védett	1554	44706	lepkék
Hyssia cavernosa	feketejegyű rétibagoly	\N	Védett	1554	44707	lepkék
Ichneumon dispar	gyapjaslepke-fürkész	\N	Védett	1554	44708	hártyásszárnyúak
Idia calvaria	sárgafoltú kuszabagoly	\N	Védett	1554	44709	lepkék
Inachis io	nappali pávaszem	\N	Védett	1554	44710	lepkék
Inocellia braueri	déli kurta-tevenyakú	\N	Védett	1554	44711	recésszárnyúak
Iphiclides podalirius	kardoslepke	\N	Védett	1554	44712	lepkék
Isogenus nubecula	füstös álkérész	\N	Védett	1554	44713	álkérészek
Isognomostoma isognomostoma	háromfogú csiga	\N	Védett	1554	44714	puhatestűek
Isonychia ignota	vastagkarmú kérész	\N	Védett	1554	44715	kérészek
Isophya brevipennis	kárpáti tarsza	\N	Védett	1554	44716	egyenesszárnyúak
Isophya camptoxypha	kárpáti tarsza	\N	Védett	1554	44717	egyenesszárnyúak
Isophya costata	magyar tarsza	HD II, IV	Fokozottan védett	1554	44718	egyenesszárnyúak
Isophya modesta	pusztai tarsza	\N	Védett	1554	44719	egyenesszárnyúak
Isophya modestior	illír tarsza	\N	Védett	1554	44720	egyenesszárnyúak
Isophya pienensis	pienini tarsza	\N	Védett	1554	44721	egyenesszárnyúak
Isophya stysi	Stys-tarsza	HD II, IV	Fokozottan védett	1554	44722	egyenesszárnyúak
Isoptena serricornis	homokásó álkérész	\N	Védett	1554	44723	álkérészek
Jolana iolas	magyar boglárka	\N	Fokozottan védett	1554	44724	lepkék
Jordanita graeca	görög fémlepke	\N	Védett	1554	44725	lepkék
Kisanthobia ariasi	Arias-díszbogár	\N	Védett	1554	44726	bogarak
Kovacsia kovacsi	dobozi pikkelyescsiga	HD II, IV	Fokozottan védett	1554	44727	puhatestűek
Lacon querceus	tarka pikkelyespattanó	\N	Védett	1554	44728	bogarak
Lamellocossus terebrus	nyárfarontólepke	\N	Védett	1554	44729	lepkék
Lampra rutilans	hársfa-tarkadíszbogár	\N	Védett	1554	44730	bogarak
Lamprodila decipiens	nyírfa-tarkadíszbogár	\N	Védett	1554	44731	bogarak
Lamprodila festiva	boróka-tarkadíszbogár	\N	Védett	1554	44732	bogarak
Lamprodila mirifica	szilfa-tarkadíszbogár	\N	Védett	1554	44733	bogarak
Lamprodila rutilans	hársfa-tarkadíszbogár	\N	Védett	1554	44734	bogarak
Lamprotes c-aureum	c-betűs aranybagoly	\N	Védett	1554	44735	lepkék
Larentia clavaria	nagy mályvaaraszoló	\N	Védett	1554	44736	lepkék
Leiopus punctulatus	feketemintás gesztcincér	\N	Védett	1554	44737	bogarak
Leistus terminatus	vöröslő szívnyakúfutó	\N	Védett	1554	44738	bogarak
Lemonia dumi	sávos pohók	\N	Védett	1554	44739	lepkék
Lemonia taraxaci	pitypangszövő	\N	Védett	1554	44740	lepkék
Leptidea morsei	keleti mustárlepke	HD II, IV	Védett	1554	44741	lepkék
Leptoglossus occidentalis	keleti levéllábú-poloska	\N	Inváziós	1554	44742	poloskák
Leptophyes discoidalis	erdélyi virágszöcske	\N	Védett	1554	44743	egyenesszárnyúak
Leptura annularis	szalagos karcsúcincér	\N	Védett	1554	44744	bogarak
Lestes dryas	réti rabló	\N	Védett	1554	44745	szitakötők
Lestes macrostigma	nagy foltosrabló	\N	Védett	1554	44746	szitakötők
Lethrus apterus	nagyfejű csajkó	\N	Védett	1554	44747	bogarak
Leucodonta bicoloria	aranyfoltos púposszövő	\N	Védett	1554	44748	lepkék
Leucorrhinia caudalis	tócsaszitakötő	HD IV	Fokozottan védett	1554	44749	szitakötők
Leucorrhinia pectoralis	lápi szitakötő	HD II, IV	Fokozottan védett	1554	44750	szitakötők
Libelloides macaronius	keleti rablópille	\N	Fokozottan védett	1554	44751	recésszárnyúak
Libellula fulva	mocsári szitakötő	\N	Védett	1554	44752	szitakötők
Libythea celtis	csőröslepke	\N	Védett	1554	44753	lepkék
Lignyoptera fumidaria	füstös ősziaraszoló	HD II, IV	Fokozottan védett	1554	44754	lepkék
Limenitis camilla	kis lonclepke	\N	Védett	1554	44755	lepkék
Limenitis populi	nagy nyárfalepke	\N	Fokozottan védett	1554	44756	lepkék
Limenitis reducta	kék lonclepke	\N	Védett	1554	44757	lepkék
Limnephilus elegans	elegáns mocsáritegzes	\N	Védett	1554	44758	tegzesek
Limoniscus violaceus	kék pattanó	HD II	Fokozottan védett	1554	44759	bogarak
Liocola lugubris	márványos virágbogár	\N	Védett	1554	44760	bogarak
Lioderina linearis	mandulacincér	\N	Védett	1554	44761	bogarak
Lithophane semibrunnea	keskenyszárnyú fabagoly	\N	Védett	1554	44762	lepkék
Locusta migratoria	keleti vándorsáska	\N	Védett	1554	44763	egyenesszárnyúak
Lopinga achine	sápadt szemeslepke	HD IV	Fokozottan védett	1554	44764	lepkék
Lozekia transsylvanica	erdélyi pikkelyescsiga	\N	Védett	1554	44765	puhatestűek
Lucanus cervus	nagy szarvasbogár	HD II	Védett	1554	44766	bogarak
Luperina zollikoferi	óriás-szürkebagoly	\N	Védett	1554	44767	lepkék
Lycaena alciphron	ibolyás tűzlepke	\N	Védett	1554	44768	lepkék
Lycaena dispar	nagy tűzlepke	HD II, IV	Védett	1554	44769	lepkék
Lycaena dispar hungarica	nagy tűzlepke	HD II, IV	Védett	1554	44770	lepkék
Lycaena dispar rutila	nagy tűzlepke	HD II, IV	Védett	1554	44771	lepkék
Lycaena helle	lápi tűzlepke	\N	8_melleklet	1554	44772	lepkék
Lycaena hippothoe	havasi tűzlepke	\N	Védett	1554	44773	lepkék
Lycaena thersamon	kis tűzlepke	\N	Védett	1554	44774	lepkék
Lycophotia porphyrea	porfírbagoly	\N	Védett	1554	44775	lepkék
Lycosa singoriensis	szongáriai cselőpók	\N	Védett	1554	44776	pókok
Lycosa vultuosa	pokoli cselőpók	\N	Védett	1554	44777	pókok
Lygephila ludicra	keskenyszárnyú csüdfűbagoly	\N	Védett	1554	44778	lepkék
Macronychus quadrituberculatus	négypúpú karmosbogár	\N	Védett	1554	44779	bogarak
Macroplea mutica	balatoni hínárbogár	\N	Védett	1554	44780	bogarak
Macrosiagon bimaculata	sarkantyús fészekbogár	\N	Védett	1554	44781	bogarak
Maculinea alcon	szürkés hangyaboglárka	\N	Védett	1554	44782	lepkék
Maculinea alcon xerophila	szürkés hangyaboglárka	\N	Védett	1554	44783	lepkék
Maculinea arion	nagyfoltú hangyaboglárka	HD IV	Védett	1554	44784	lepkék
Maculinea nausithous	sötét hangyaboglárka (zanótboglárka)	HD II, IV	Védett	1554	44785	lepkék
Maculinea teleius	vérfű-hangyaboglárka	HD II, IV	Védett	1554	44786	lepkék
Mantis religiosa	imádkozósáska	\N	Védett	1554	44787	egyenesszárnyúak
Mantispa aphavexelle	mediterrán fogólábú-fátyolka	\N	Védett	1554	44788	recésszárnyúak
Mantispa perla	füstösszárnyú fogólábú-fátyolka	\N	Védett	1554	44789	recésszárnyúak
Mantispa styriaca	kétszínű fogólábú-fátyolka	\N	Védett	1554	44790	recésszárnyúak
Marthamea vitripennis	folyólakó nagyálkérész	\N	Védett	1554	44791	álkérészek
Marumba quercus	tölgyfaszender	\N	Védett	1554	44792	lepkék
Mediterranea depressa	lapos kristálycsiga	\N	Védett	1554	44793	puhatestűek
Megascolia maculata	óriás-tőrösdarázs	\N	Védett	1554	44794	hártyásszárnyúak
Megistopus flavicornis	kétfoltos hangyaleső	\N	Védett	1554	44795	recésszárnyúak
Megopis scabricornis	diófacincér	\N	Védett	1554	44796	bogarak
Melampophylax nepos	kárpáti forrástegzes	\N	Védett	1554	44797	tegzesek
Melitaea aurelia	recés tarkalepke	\N	Védett	1554	44798	lepkék
Melitaea britomartis	barnás tarkalepke	\N	Védett	1554	44799	lepkék
Melitaea diamina	kockás tarkalepke	\N	Védett	1554	44800	lepkék
Melitaea ornata	magyar tarkalepke	\N	Védett	1554	44801	lepkék
Melitaea trivia	kis tarkalepke	\N	Védett	1554	44802	lepkék
Meloe autumnalis	őszi nünüke	\N	Védett	1554	44803	bogarak
Meloe brevicollis	tar nünüke	\N	Védett	1554	44804	bogarak
Meloe cicatricosus	óriásnünüke	\N	Védett	1554	44805	bogarak
Meloe decorus	díszes nünüke	\N	Védett	1554	44806	bogarak
Meloe hungarus	magyar nünüke	\N	Védett	1554	44807	bogarak
Meloe mediterraneus	déli nünüke	\N	Védett	1554	44808	bogarak
Meloe rufiventris	vöröshasú nünüke	\N	Védett	1554	44809	bogarak
Meloe rugosus	ráncos nünüke	\N	Védett	1554	44810	bogarak
Meloe scabriusculus	érdes nünüke	\N	Védett	1554	44811	bogarak
Meloe tuccius	gödörkés nünüke	\N	Védett	1554	44812	bogarak
Meloe uralensis	uráli nünüke	\N	Védett	1554	44813	bogarak
Meloe variegatus	pompás nünüke	\N	Védett	1554	44814	bogarak
Mesotrosta signalis	fehérjegyű törpebagoly	\N	Védett	1554	44815	lepkék
Mesotype didymata	sötétfoltos araszoló	\N	Védett	1554	44816	lepkék
Metachrostis dardouini	palakék törpebagoly	\N	Védett	1554	44817	lepkék
Methorasa latreillei	gyöngypettyes bagoly	\N	Védett	1554	44819	lepkék
Modicogryllus truncatus	déli homlokjegyestücsök	\N	Védett	1554	44820	egyenesszárnyúak
Monachoides vicinus	pikkelyes csiga	\N	Védett	1554	44821	puhatestűek
Morimus funereus	gyászcincér	HD II	Védett	1554	44822	bogarak
Mormo maura	gyászbagoly	\N	Védett	1554	44823	lepkék
Musaria argus	árgusszemű cincér	\N	Védett	1554	44824	bogarak
Mylabris pannonica	pannon hólyaghúzó	\N	Védett	1554	44825	bogarak
Myrmecaelurus punctulatus	kunsági hangyafarkas	\N	Védett	1554	44826	recésszárnyúak
Myrmeleon bore	északi hangyaleső	\N	Védett	1554	44827	recésszárnyúak
Myrmeleon formicarius	erdei hangyaleső	\N	Védett	1554	44828	recésszárnyúak
Myrmeleon inconspicuus	homoki hangyaleső	\N	Védett	1554	44829	recésszárnyúak
Naenia typica	hálózatos sóskabagoly	\N	Védett	1554	44830	lepkék
Necydalis major	nagy fürkészcincér	\N	Védett	1554	44831	bogarak
Necydalis ulmi	aranyszőrű fürkészcincér	\N	Védett	1554	44832	bogarak
Nemesia pannonica	magyar aknászpók	\N	Védett	1554	44833	pókok
Nemobius sylvestris	erdei tücsök	\N	Védett	1554	44834	egyenesszárnyúak
Neodorcadion bilineatum	kétsávos földicincér	\N	Védett	1554	44835	bogarak
Neoephemera maxima	rábai kérész	\N	Védett	1554	44836	kérészek
Neozephyrus quercus	tölgyfalepke	\N	Védett	1554	44837	lepkék
Neptis rivularis	nagy fehérsávoslepke	\N	Védett	1554	44838	lepkék
Neptis sappho	kis fehérsávoslepke	\N	Védett	1554	44839	lepkék
Netocia hungarica	magyar virágbogár	\N	Védett	1554	44840	bogarak
Netocia ungarica	magyar virágbogár	\N	Védett	1554	44841	bogarak
Potosia ungarica	magyar virágbogár	\N	Védett	1554	44842	bogarak
Neuroleon nemausiensis	kis hangyaleső	\N	Védett	1554	44843	recésszárnyúak
Nezara viridula	zöld vándorpoloska	\N	Inváziós	1554	44844	poloskák
Notodonta torva	ritka púposszövő (kormos púposszövő)	\N	Védett	1554	44845	lepkék
Notonecta lutea	sárgapajzsú hanyattúszó-poloska	\N	Védett	1554	44846	poloskák
Nudaria mundana	csupasz medvelepke	\N	Védett	1554	44847	lepkék
Nymphalis antiopa	gyászlepke	\N	Védett	1554	44848	lepkék
Nymphalis atalanta	atalantalepke	\N	Védett	1554	44849	lepkék
Nymphalis c-album	c-betűs lepke	\N	Védett	1554	44850	lepkék
Nymphalis io	nappali pávaszem	\N	Védett	1554	44851	lepkék
Nymphalis polychloros	nagy rókalepke	\N	Védett	1554	44852	lepkék
Nymphalis urticae	kis rókalepke	\N	Védett	1554	44853	lepkék
Nymphalis vau-album	l-betűs rókalepke	\N	Védett	1554	44854	lepkék
Nymphalis xanthomelas	vörös rókalepke	\N	Védett	1554	44855	lepkék
Oberea euphorbiae	magyar kutyatejcincér	\N	Védett	1554	44856	bogarak
Oberea pedemontana	varjútöviscincér	\N	Védett	1554	44857	bogarak
Ocneria rubea	rőt gyapjaslepke	\N	Védett	1554	44858	lepkék
Ocnogyna parasita	csonkaszárnyú medvelepke	\N	Védett	1554	44859	lepkék
Odezia atrata	fekete araszoló	\N	Védett	1554	44860	lepkék
Odice arcuinna	réti törpebagoly	\N	Védett	1554	44861	lepkék
Odontognophos dumetata	csücskös sziklaaraszoló	\N	Védett	1554	44862	lepkék
Odontognophos dumetatus	csücskös sziklaaraszoló	\N	Védett	1554	44863	lepkék
Odontopodisma rubripes	vöröslábú hegyisáska	HD II, IV	Védett	1554	44864	egyenesszárnyúak
Odontoscelis hispidula	szőrös pajzsospoloska	\N	Védett	1554	44865	poloskák
Odontosia carmelita	barátka-púposszövő	\N	Védett	1554	44866	lepkék
Oligolimax annularis	gyűrűs üvegcsiga	\N	Védett	1554	44867	puhatestűek
Oligoneuriella keffermuellerae	Keffermüller-denevérszárnyú-kérész	\N	Védett	1554	44868	kérészek
Oligoneuriella pallida	sápadt denevérszárnyú-kérész	\N	Védett	1554	44869	kérészek
Oligoneuriella rhenana	rajnai denevérszárnyú-kérész	\N	Védett	1554	44870	kérészek
Oligotricha striata	lomha lápipozdorján	\N	Védett	1554	44871	tegzesek
Omoglymmius germari	fogasvállú állasbogár	\N	Védett	1554	44872	bogarak
Onychogomphus forcipatus	csermelyszitakötő	\N	Védett	1554	44873	szitakötők
Oodescelis melas	alföldi gyászbogár	\N	Védett	1554	44874	bogarak
Oodescelis polita	sima gyászbogár	\N	Védett	1554	44875	bogarak
Ophiogomphus cecilia	erdei szitakötő	HD II, IV	Védett	1554	44876	szitakötők
Ophonus sabulicola	homoki bársonyfutó	\N	Védett	1554	44877	bogarak
Orbona fragariae	óriás-télibagoly	\N	Védett	1554	44878	lepkék
Orconectes limosus	cifra rák	IAS	Inváziós	1554	44879	rákok
Orconectes virilis	északi cifrarák	IAS	Inváziós	1554	44880	rákok
Orcula dolium	hordócsiga	\N	Védett	1554	44881	puhatestűek
Oria musculosa	szalmasárga búzabagoly	\N	Védett	1554	44882	lepkék
Orthetrum brunneum	pataki szitakötő	\N	Védett	1554	44883	szitakötők
Orthostixis cribraria	pettyes fehéraraszoló	\N	Védett	1554	44884	lepkék
Oryctes nasicornis	orrszarvúbogár	\N	Védett	1554	44885	bogarak
Oryctes nasicornis holdhausi	orrszarvúbogár	\N	Védett	1554	44886	bogarak
Osmoderma eremita	remetebogár	HD II kiemelt, IV	Fokozottan védett	1554	44887	bogarak
Osmylus fulvicephalus	foltosszárnyú partifátyolka	\N	Védett	1554	44888	recésszárnyúak
Ostrinia palustralis	mocsári tűzmoly	\N	Védett	1554	44889	lepkék
Otho sphondyloides	vaskos tövisnyakúbogár	\N	Védett	1554	44890	bogarak
Otiorhynchus roubali	Roubal-gyalogormányos	\N	Védett	1554	44891	bogarak
Oxychilus depressus	lapos kristálycsiga	\N	Védett	1554	44892	puhatestűek
Oxychilus orientalis	keleti kristálycsiga	\N	Védett	1554	44893	puhatestűek
Oxytripia orbiculosa	nagyfoltú bagoly	\N	Fokozottan védett	1554	44894	lepkék
Oxytrypia orbiculosa	nagyfoltú bagoly	\N	Fokozottan védett	1554	44895	lepkék
Pacifastacus leniusculus	jelzőrák, vagy amerikai folyamirák	IAS	Inváziós	1554	44896	rákok
Pagodulina pagodula	pagodacsiga	\N	Védett	1554	44897	puhatestűek
Paidia rica	májmoha-medvelepke	\N	Védett	1554	44898	lepkék
Paladilhia hungarica	magyar vakcsiga	HD II kiemelt, IV	Védett	1554	44899	puhatestűek
Palingenia longicauda	tiszavirág	\N	Védett	1554	44900	kérészek
Palmitia massilialis	cifra fényilonca	\N	Védett	1554	44901	lepkék
Pammene querceti	magyar tölgymakkmoly	\N	Védett	1554	44902	lepkék
Panchrysia deaurata	pompás aranybagoly	\N	Védett	1554	44903	lepkék
Papilio machaon	fecskefarkú lepke	\N	Védett	1554	44904	lepkék
Paraboarmia viertlii	magyar faaraszoló	\N	Védett	1554	44905	lepkék
Paracaloptenus caloptenoides	álolaszsáska	HD II, IV	Fokozottan védett	1554	44906	egyenesszárnyúak
Parasemia plantaginis	útifű-medvelepke	\N	Védett	1554	44907	lepkék
Parexarnis fugax	pusztai földibagoly	\N	Védett	1554	44908	lepkék
Parnassius apollo	nagy apollólepke	\N	8_melleklet	1554	44909	lepkék
Parnassius mnemosyne	kis apollólepke	HD IV	Védett	1554	44910	lepkék
Parnopes grandior	pompás fémdarázs	\N	Védett	1554	44911	hártyásszárnyúak
Pedinus hungaricus	pannóniai gyászbogár	\N	Védett	1554	44912	bogarak
Perconia strigillaria	fehérszárnyú aranyaraszoló	\N	Védett	1554	44913	lepkék
Perforatella bidentata	nagyfogú csiga	\N	Védett	1554	44914	puhatestűek
Perforatella dibothrion	dibothrion-csiga	\N	Védett	1554	44915	puhatestűek
Perforatella vicinus	pikkelyes csiga	\N	Védett	1554	44916	puhatestűek
Peribatodes umbraria	fagyal-faaraszoló	\N	Védett	1554	44917	lepkék
Pericallia matronula	óriás-medvelepke	\N	Fokozottan védett	1554	44918	lepkék
Periphanes delphinii	szarkalábbagoly	\N	Védett	1554	44919	lepkék
Perizoma minorata	szemvidító araszoló	\N	Védett	1554	44920	lepkék
Perizoma sagittata	nyílfoltos tarkaaraszoló	\N	Védett	1554	44921	lepkék
Petasina unidentata	egyfogú szőrőscsiga	\N	Védett	1554	44922	puhatestűek
Phalera bucephaloides	magyar púposszövő (sárgaholdas púposszövő)	\N	Védett	1554	44923	lepkék
Pharmacis fusconebulosus	északi gyökérrágólepke	\N	Védett	1554	44924	lepkék
Phenacolimax annularis	gyűrűs üvegcsiga	\N	Védett	1554	44925	puhatestűek
Phengaris alcon	szürkés hangyaboglárka	\N	Védett	1554	44926	lepkék
Phengaris arion	nagyfoltú hangyaboglárka	HD IV	Védett	1554	44927	lepkék
Phengaris nausithous	sötét hangyaboglárka (zanótboglárka)	HD II, IV	Védett	1554	44928	lepkék
Phengaris teleius	vérfű-hangyaboglárka	HD II, IV	Védett	1554	44929	lepkék
Pheosia gnoma	nyírfa-púposszövő	\N	Védett	1554	44930	lepkék
Phlogophora scita	halványzöld csipkésbagoly	\N	Védett	1554	44931	lepkék
Pholidoptera littoralis	bújkáló avarszöcske	\N	Védett	1554	44932	egyenesszárnyúak
Pholidoptera transsylvanica	erdélyi avarszöcske	HD II, IV	Védett	1554	44933	egyenesszárnyúak
Photedes captiuncula	hegyi törpebagoly	\N	Védett	1554	44934	lepkék
Phragmatiphila nexa	lángszínű nádibagoly	\N	Védett	1554	44935	lepkék
Phragmatobia fuliginosa	füstös medvelepke	\N	Védett	1554	44936	lepkék
Phragmatobia luctifera	füstös medvelepke	\N	Védett	1554	44937	lepkék
Phyllodesma ilicifolia	cserlevélpohók	\N	Védett	1554	44938	lepkék
Phyllometra culminaria	csüngőaraszoló	HD II, IV	Fokozottan védett	1554	44939	lepkék
Phyllomorpha laciniata	lándzsás karimáspoloska	\N	Védett	1554	44940	poloskák
Pieris bryoniae	hegyi fehérlepke	\N	Védett	1554	44941	lepkék
Pieris ergane	sziklai fehérlepke	\N	Védett	1554	44942	lepkék
Pieris mannii	magyar fehérlepke	\N	Védett	1554	44943	lepkék
Pilemia hirsutula	macskaherecincér	\N	Védett	1554	44944	bogarak
Pilemia tigrina	atracélcincér	HD II, IV	Fokozottan védett	1554	44945	bogarak
Platycerus caprea	nagy fémesszarvasbogár	\N	Védett	1554	44946	bogarak
Platycerus caraboides	kis fémesszarvasbogár	\N	Védett	1554	44947	bogarak
Platyphylax frauenfeldi	drávai tegzes	\N	Fokozottan védett	1554	44948	tegzesek
Platyscelis hungarica	pusztai gyászbogár	\N	Védett	1554	44949	bogarak
Plebeius idas	északi boglárka	\N	Védett	1554	44950	lepkék
Plebeius sephirus	fóti boglárka	\N	Fokozottan védett	1554	44951	lepkék
Plebejus idas	északi boglárka	\N	Védett	1554	44952	lepkék
Plebejus sephirus	fóti boglárka	\N	Fokozottan védett	1554	44953	lepkék
Plectrocnemia minima	balkáni pálcástegzes	\N	Védett	1554	44954	tegzesek
Podisma pedestris	tarka hegyisáska	\N	Védett	1554	44955	egyenesszárnyúak
Poecilimon brunneri	Brunner-pókszöcske	\N	Fokozottan védett	1554	44956	egyenesszárnyúak
Poecilimon fussii	Fuss-pókszöcske	\N	Védett	1554	44957	egyenesszárnyúak
Poecilimon intermedius	keleti pókszöcske	\N	Védett	1554	44958	egyenesszárnyúak
Poecilimon schmidtii	Schmidt-pókszöcske	\N	Védett	1554	44959	egyenesszárnyúak
Poecilus kekesiensis	hortobágyi gyászfutó	\N	Védett	1554	44960	bogarak
Polychrysia moneta	szélesszárnyú aranybagoly	\N	Védett	1554	44961	lepkék
Polygonia c-album	c-betűs lepke	\N	Védett	1554	44962	lepkék
Polymixis rufocincta	villányi télibagoly	HD II, IV	Fokozottan védett	1554	44964	lepkék
Polymixis rufocincta isolata	villányi télibagoly	HD II, IV	Fokozottan védett	1554	44965	lepkék
Polyommatus admetus	bundás boglárka	\N	Védett	1554	44966	lepkék
Polyommatus amandus	csillogó boglárka	\N	Védett	1554	44967	lepkék
Polyommatus damon	csíkos boglárka	\N	Fokozottan védett	1554	44968	lepkék
Polyommatus dorylas	fénylő boglárka	\N	Védett	1554	44969	lepkék
Polyommatus thersites	ibolyaszín boglárka	\N	Védett	1554	44970	lepkék
Polypogon gryphalis	láperdei karcsúbagoly	\N	Védett	1554	44971	lepkék
Polysarcus denticauda	fogasfarkú szöcske	\N	Védett	1554	44972	egyenesszárnyúak
Pomatias elegans	nyugati ajtóscsiga	\N	Védett	1554	44973	puhatestűek
Pomatias rivulare	keleti ajtóscsiga	\N	Védett	1554	44974	puhatestűek
Porphyrophora polonica	lengyel bíborpajzstetű	\N	Védett	1554	44975	pajzstetvek
Potamophilus acuminatus	nagy karmosbogár	\N	Védett	1554	44976	bogarak
Potosia aeruginosa	pompás virágbogár	\N	Védett	1554	44977	bogarak
Potosia fieberi	rezes virágbogár	\N	Védett	1554	44978	bogarak
Probaticus subrugosus	ráncos gyászbogár	HD II, IV	Fokozottan védett	1554	44979	bogarak
Procambarus clarkii	kaliforniai vörösrák	IAS	Inváziós	1554	44980	rákok
Procambarus falax	virginiai márványrák	IAS	Inváziós	1554	44981	rákok
Procambarus falax virginalis	virginiai márványrák	IAS	Inváziós	1554	44982	rákok
Procambarus falax forma virginalis	virginiai márványrák	IAS	Inváziós	1554	44983	rákok
Procambarus fallax	virginiai márványrák	IAS	Inváziós	1554	44984	rákok
Procambarus sp.	virginiai márványrák	IAS	Inváziós	1554	44985	rákok
Pronocera angusta	keskeny luccincér	\N	Védett	1554	44986	bogarak
Proserpinus proserpina	törpeszender	HD IV	Védett	1554	44987	lepkék
Prostomis mandibularis	európai fogasállúbogár	\N	Védett	1554	44988	bogarak
Protaetia aeruginosa	pompás virágbogár	\N	Védett	1554	44989	bogarak
Protaetia affinis	smaragdzöld virágbogár	\N	Védett	1554	44990	bogarak
Potosia affinis	smaragdzöld virágbogár	\N	Védett	1554	44991	bogarak
Protaetia fieberi	rezes virágbogár	\N	Védett	1554	44992	bogarak
Protaetia lugubris	márványos virágbogár	\N	Védett	1554	44993	bogarak
Protaetia ungarica	magyar virágbogár	\N	Védett	1554	44994	bogarak
Protichneumon pisarius	nagy szenderfürkész	\N	Védett	1554	44995	hártyásszárnyúak
Pseudanodonta complanata	lapos tavikagyló	\N	Védett	1554	44996	puhatestűek
Pseudophilotes schiffermuelleri	apró boglárka	\N	Védett	1554	44997	lepkék
Psilothrix femoralis	pusztai karimásbogár	\N	Védett	1554	44998	bogarak
Ptilophorus dufourii	szürke darázsbogár	\N	Védett	1554	44999	bogarak
Purpuricenus budensis	bíborcincér	\N	Védett	1554	45000	bogarak
Purpuricenus globulicollis	kerekpajzsú vércincér	\N	Védett	1554	45001	bogarak
Purpuricenus kaehleri	hosszúcsápú vércincér	\N	Védett	1554	45002	bogarak
Pyrgus alveus	hegyi busalepke	\N	Védett	1554	45003	lepkék
Pyrgus serratulae	homályos busalepke	\N	Védett	1554	45004	lepkék
Pyrois cinnamomea	ritka fahéjbagoly	\N	Védett	1554	45005	lepkék
Pyronia tithonus	kis ökörszemlepke	\N	Védett	1554	45006	lepkék
Pyrrhia purpurina	ezerjófűbagoly	\N	Védett	1554	45007	lepkék
Pytho depressus	lapos sárkánybogár	\N	Védett	1554	45008	bogarak
Reskovitsia alborivularis	keleti kormosmoly	\N	Védett	1554	45009	lepkék
Rhabdiopteryx acuminata	dombvidéki álkérész	\N	Védett	1554	45010	álkérészek
Rhabdiopteryx hamulata	Mocsáry-álkérész	\N	Fokozottan védett	1554	45011	álkérészek
Rhamnusium bicolor	kétszínű nyárfacincér	\N	Védett	1554	45012	bogarak
Rheumaptera undulata	hullámvonalas araszoló	\N	Védett	1554	45013	lepkék
Rhyacophila hirticornis	márványos örvénytegzes	\N	Védett	1554	45014	tegzesek
Rhyparioides metelkanus	Metelka-medvelepke	\N	Fokozottan védett	1554	45015	lepkék
Rhysodes sulcatus	kerekvállú állasbogár	HD II	Védett	1554	45016	bogarak
Rhyssa persuasoria	óriás-fenyőfürkész	\N	Védett	1554	45017	hártyásszárnyúak
Rileyiana fovea	zörgőbagoly	\N	Védett	1554	45018	lepkék
Ropalopus insubricus	kékeszöld facincér	\N	Védett	1554	45019	bogarak
Ropalopus ungaricus	magyar facincér	\N	Védett	1554	45020	bogarak
Ropalopus varini	vöröscombú facincér	\N	Védett	1554	45021	bogarak
Rosalia alpina	havasi cincér	HD II kiemelt, IV	Védett	1554	45022	bogarak
Ruthenica filograna	karcsú orsócsiga	\N	Védett	1554	45023	puhatestűek
Sadleriana pannonica	tornai patakcsiga	HD II, IV	Védett	1554	45024	puhatestűek
Saga pedo	fűrészlábú szöcske	HD IV	Védett	1554	45025	egyenesszárnyúak
Saperda octopunctata	nyolcpontos cincér	\N	Védett	1554	45026	bogarak
Saperda perforata	díszes nyárfacincér	\N	Védett	1554	45027	bogarak
Saperda punctata	pettyes szilcincér	\N	Védett	1554	45028	bogarak
Saperda scalaris	létracincér	\N	Védett	1554	45029	bogarak
Saperda similis	kecskefűzcincér	\N	Védett	1554	45030	bogarak
Saragossa implexa	ázsiai szegfűbagoly	\N	Védett	1554	45031	lepkék
Saragossa porosa	sziki ürömbagoly	\N	Védett	1554	45032	lepkék
Saragossa porosa kenderesiensis	sziki ürömbagoly	\N	Védett	1554	45033	lepkék
Saturnia pavonia	kis pávaszem	\N	Védett	1554	45034	lepkék
Saturnia pyri	nagy pávaszem	\N	Védett	1554	45035	lepkék
Saturnia spini	közepes pávaszem	\N	Védett	1554	45036	lepkék
Satyrium ilicis	tölgyfa-csücsköslepke	\N	Védett	1554	45037	lepkék
Satyrium pruni	szilvafa-csücsköslepke	\N	Védett	1554	45038	lepkék
Satyrium spini	kökény-csücsköslepke	\N	Védett	1554	45039	lepkék
Satyrium w-album	szilfa-csücsköslepke	\N	Védett	1554	45040	lepkék
Scarabaeus pius	jámbor galacsinhajtó	\N	Védett	1554	45041	bogarak
Scarabaeus typhon	óriás-galacsinhajtó	\N	Védett	1554	45042	bogarak
Scarites terricola	vájárfutó	\N	Védett	1554	45043	bogarak
Schinia cardui	keserűgyökér-nappalibagoly	\N	Védett	1554	45044	lepkék
Schinia cognata	nyúlparéj-nappalibagoly	\N	Védett	1554	45045	lepkék
Schistostege decussata	hálós rétiaraszoló	\N	Védett	1554	45046	lepkék
Schizotus pectinicornis	kis bíborbogár	\N	Védett	1554	45047	bogarak
Scoliantides orion	szemes boglárka	\N	Védett	1554	45048	lepkék
Scopula nemoraria	ligeti sávosaraszoló	\N	Védett	1554	45050	lepkék
Scotochrosta pulla	sötét őszibagoly	\N	Védett	1554	45051	lepkék
Semanotus russicus	borókacincér	\N	Védett	1554	45052	bogarak
Shargacucullia gozmanyi	Gozmány-csuklyásbagoly	\N	Védett	1554	45053	lepkék
Shargacucullia prenanthis	tavaszigörvélyfű-csuklyásbagoly	\N	Védett	1554	45054	lepkék
Shargacucullia thapsiphaga	fakó csuklyásbagoly	\N	Védett	1554	45055	lepkék
Sinodendron cylindricum	tülkös szarvasbogár	\N	Védett	1554	45056	bogarak
Somatochlora flavomaculata	sárgafoltos szitakötő	\N	Védett	1554	45057	szitakötők
Spelaeodiscus triarius	észak-kárpáti csiga	\N	Védett	1554	45058	puhatestűek
Sphex rufocinctus	szöcskeölő darázs	\N	Védett	1554	45059	hártyásszárnyúak
Sphyracephala europaea	európai nyelesszemű-légy	\N	Védett	1554	45060	kétszárnyúak
Spialia orbifer	törpebusalepke	\N	Védett	1554	45061	lepkék
Spialia sertorius	lápi busalepke (nyugati törpebusalepke)	\N	Védett	1554	45062	lepkék
Spudaea ruticilla	tölgyfa-őszibagoly	\N	Védett	1554	45063	lepkék
Staurophora celsia	buckabagoly	\N	Védett	1554	45064	lepkék
Stenobothrus eurasius	eurázsiai rétisáska	HD II, IV	Védett	1554	45065	egyenesszárnyúak
Stenolophus steveni	Steven-turzásfutó	\N	Védett	1554	45066	bogarak
Stenoria apicalis	keskenyfedős élősdibogár	\N	Védett	1554	45067	bogarak
Stilbium cyanurum	nagy smaragdfémdarázs	\N	Védett	1554	45069	hártyásszárnyúak
Stylurus flavipes	sárgás szitakötő	HD IV	Védett	1554	45070	szitakötők
Sympetrum depressiusculum	lassú szitakötő	\N	Védett	1554	45071	szitakötők
Synansphecia affinis	napvirágszitkár	\N	Védett	1554	45072	lepkék
Taeniopteryx schoenemundi	Schönemund-álkérész	\N	Védett	1554	45073	álkérészek
Tenebrio opacus	fogastorkú lisztbogár	\N	Védett	1554	45074	bogarak
Tetragnatha reimoseri	farkos állaspók	\N	Védett	1554	45075	pókok
Tetragnatha shoshone	rejtett állaspók	\N	Védett	1554	45076	pókok
Tetragnatha striata	nádi állaspók	\N	Védett	1554	45077	pókok
Tettigonia caudata	farkos lombszöcske	\N	Védett	1554	45078	egyenesszárnyúak
Thecla betulae	nyírfa-csücsköslepke	\N	Védett	1554	45079	lepkék
Theodoxus danubialis	rajzos bödöncsiga	\N	Védett	1554	45080	puhatestűek
Theodoxus danubialis danubialis	rajzos bödöncsiga	\N	Védett	1554	45081	puhatestűek
Theodoxus fluviatilis	folyami bödöncsiga	\N	Inváziós	1554	45082	puhatestűek
Theodoxus prevostianus	fekete bödöncsiga	HD IV	Fokozottan védett	1554	45083	puhatestűek
Theodoxus transversalis	sávos bödöncsiga	HD II, IV	Védett	1554	45084	puhatestűek
Theophilea subcylindricollis	hengeres szalmacincér	\N	Védett	1554	45085	bogarak
Thymelicus acteon	csíkos busalepke	\N	Védett	1554	45086	lepkék
Tituboea macropus	dárdahere-zsákhordóbogár	\N	Védett	1554	45088	bogarak
Trebacosa europaea	európai ál-kalózpók	\N	Védett	1554	45089	pókok
Trichia lubomirskii	Lubomirski-csiga	\N	Védett	1554	45090	puhatestűek
Trichia striolata	nagy szőrőscsiga	\N	Védett	1554	45091	puhatestűek
Trichia unidentata	egyfogú szőrőscsiga	\N	Védett	1554	45092	puhatestűek
Trichoferus pallidus	sápadt éjicincér	\N	Védett	1554	45093	bogarak
Triodia amasina	balkáni gyökérrágólepke	\N	Védett	1554	45094	lepkék
Tyria jacobaeae	jakabfű-lepke	\N	Védett	1554	45095	lepkék
Unio crassus	tompa folyamkagyló	HD II, IV	Védett	1554	45096	puhatestűek
Vadonia steveni	alföldi virágcincér	\N	Védett	1554	45097	bogarak
Valvata naticina	kúpos kerekszájúcsiga	\N	Védett	1554	45098	puhatestűek
Vanessa atalanta	atalantalepke	\N	Védett	1554	45099	lepkék
Vertigo angustior	harántfogú törpecsiga	HD II	Védett	1554	45100	puhatestűek
Vertigo moulinsiana	hasas törpecsiga	HD II	Védett	1554	45101	puhatestűek
Vespa velutina nigrithorax	ázsiai lódarázs	IAS	Inváziós	1554	45102	hártyásszárnyúak
Vestia gulo	sudár orsócsiga	\N	Védett	1554	45103	puhatestűek
Vestia turgida	dagadt orsócsiga	\N	Védett	1554	45104	puhatestűek
Xerasia meschniggi	téli zuzmóbogár	\N	Védett	1554	45105	bogarak
Xestia sexstrigata	hatcsíkú földibagoly	\N	Védett	1554	45106	lepkék
Xylotrechus pantherinus	párducfoltos darázscincér	\N	Védett	1554	45107	bogarak
Zerynthia polyxena	farkasalmalepke	HD IV	Védett	1554	45108	lepkék
Zygaena fausta	nyugati csüngőlepke	\N	Védett	1554	45109	lepkék
Zygaena laeta	vörös csüngőlepke	\N	Fokozottan védett	1554	45110	lepkék
Agaricus bohusii	csoportos csiperke	\N	Védett	1554	45111	gombák, zuzmók
Scolopendra cingulata	öves szkolopendra	\N	Védett	1554	45049	soklábúak
Amanita caesarea	császárgalóca	\N	Védett	1554	45112	gombák, zuzmók
Amanita lepiotoides	húsbarna galóca	\N	Védett	1554	45113	gombák, zuzmók
Amanita vittadinii	őzlábgalóca	\N	Védett	1554	45114	gombák, zuzmók
Battarrea phalloides	álszömörcsög	\N	Védett	1554	45115	gombák, zuzmók
Boletus dupainii	kárminvörös tinóru	\N	Védett	1554	45116	gombák, zuzmók
Cantharellus melanoxeros	sötétedőhúsú rókagomba	\N	Védett	1554	45117	gombák, zuzmók
Cetraria aculeata	tüskés vértecs	\N	Védett	1554	45118	gombák, zuzmók
Cetraria islandica	izlandi zuzmó	\N	Védett	1554	45119	gombák, zuzmók
Cladonia arbuscula	erdei rénzuzmó	\N	Védett	1554	45120	gombák, zuzmók
Cladonia magyarica	magyar tölcsérzuzmó	\N	Védett	1554	45121	gombák, zuzmók
Cladonia mitis	szelíd rénzuzmó	\N	Védett	1554	45122	gombák, zuzmók
Cladonia rangiferina	valódi rénzuzmó	\N	Védett	1554	45123	gombák, zuzmók
Cortinarius (Phl.) paracephalixus	nyárfa-pókhálósgomba	\N	Védett	1554	45124	gombák, zuzmók
Cortinarius (Phl.) praestans	óriás pókhálósgomba	\N	Védett	1554	45125	gombák, zuzmók
Disciotis venosa	ráncos tárcsagomba	\N	Védett	1554	45126	gombák, zuzmók
Elaphomyces anthracinus	köldökös álszarvasgomba	\N	Védett	1554	45127	gombák, zuzmók
Elaphomyces leveillei	patinás álszarvasgomba	\N	Védett	1554	45128	gombák, zuzmók
Elaphomyces maculatus	foltos álszarvasgomba	\N	Védett	1554	45129	gombák, zuzmók
Elaphomyces mutabilis	bundás álszarvasgomba	\N	Védett	1554	45130	gombák, zuzmók
Elaphomyces persooni	kékbelű álszarvasgomba	\N	Védett	1554	45131	gombák, zuzmók
Elaphomyces virgatosporus	csíkosspórájú álszarvasgomba	\N	Védett	1554	45132	gombák, zuzmók
Endoptychum agaricoides	lemezes pöfeteg	\N	Védett	1554	45133	gombák, zuzmók
Entoloma porphyrophaeum	lilásbarna döggomba	\N	Védett	1554	45134	gombák, zuzmók
Flammulina ononidis	iglice-fülőke, mezei télifülőke	\N	Védett	1554	45135	gombák, zuzmók
Ganoderma cupreolaccatum	rézvörös lakkos tapló	\N	Védett	1554	45136	gombák, zuzmók
Geastrum hungaricum	honi csillaggomba	\N	Védett	1554	45137	gombák, zuzmók
Gomphidius roseus	rózsaszínű nyálkásgomba	\N	Védett	1554	45138	gombák, zuzmók
Gomphus clavatus	disznófülgomba	\N	Védett	1554	45139	gombák, zuzmók
Grifola frondosa	ágas tapló	\N	Védett	1554	45140	gombák, zuzmók
Gyrodon lividus	égertinóru	\N	Védett	1554	45141	gombák, zuzmók
Hapalopilus croceus	sáfrányszínű likacsgomba	\N	Védett	1554	45142	gombák, zuzmók
Hericium cirrhatum	tüskés sörénygomba	\N	Védett	1554	45143	gombák, zuzmók
Hericium erinaceum	közönséges süngomba	\N	Védett	1554	45144	gombák, zuzmók
Hygrocybe calyptriformis	rózsaszínű nedűgomba	\N	Védett	1554	45145	gombák, zuzmók
Hygrocybe punicea	vérvörös nedűgomba	\N	Védett	1554	45146	gombák, zuzmók
Hygrophorus marzuolus	tavaszi csigagomba	\N	Védett	1554	45147	gombák, zuzmók
Hygrophorus poetarum	izabellvöröses csigagomba	\N	Védett	1554	45148	gombák, zuzmók
Hypsizygus ulmarius	laskapereszke	\N	Védett	1554	45149	gombák, zuzmók
Lactarius helvus	daróc-tejelőgomba	\N	Védett	1554	45150	gombák, zuzmók
Leccinum variicolor	tarkahúsú érdestinóru	\N	Védett	1554	45151	gombák, zuzmók
Leucopaxillus compactus	háromszínű álpereszke	\N	Védett	1554	45152	gombák, zuzmók
Leucopaxillus lepistoides	tejpereszke	\N	Védett	1554	45153	gombák, zuzmók
Leucopaxillus macrocephalus	gyökeres álpereszke	\N	Védett	1554	45154	gombák, zuzmók
Lobaria pulmonaria	tüdőzuzmó	\N	Védett	1554	45155	gombák, zuzmók
Lycoperdon mammiforme	cafatos pöfeteg	\N	Védett	1554	45156	gombák, zuzmók
Peltigera leucophlebia	változó ebzuzmó	\N	Védett	1554	45157	gombák, zuzmók
Phellodon niger	fekete gereben, fekete szagosgereben	\N	Védett	1554	45158	gombák, zuzmók
Pholiota sqarrosoides	fakópikkelyes tőkegomba	\N	Védett	1554	45159	gombák, zuzmók
Phylloporus pelletieri	lemezes tinóru	\N	Védett	1554	45160	gombák, zuzmók
Pluteus umbrosus	pelyhes csengettyűgomba	\N	Védett	1554	45161	gombák, zuzmók
Polyporus rhizophilus	gyepi likacsosgomba	\N	Védett	1554	45162	gombák, zuzmók
Polyporus tuberaster	olaszgomba	\N	Védett	1554	45163	gombák, zuzmók
Polyporus umbellatus	tüskegomba	\N	Védett	1554	45164	gombák, zuzmók
Porphyrellus porphyrosporus	sötét tinóru	\N	Védett	1554	45165	gombák, zuzmók
Pseudoboletus parasiticus	élősdi tinóru	\N	Védett	1554	45166	gombák, zuzmók
Rhodotus palmatus	tönkös kacskagomba	\N	Védett	1554	45167	gombák, zuzmók
Russula claroflava	krómsárga galambgomba	\N	Védett	1554	45168	gombák, zuzmók
Sarcodon joeides	lilahúsú gereben	\N	Védett	1554	45169	gombák, zuzmók
Sarcodon scabrosus	korpás gereben	\N	Védett	1554	45170	gombák, zuzmók
Scutiger pescaprae	barnahátú zsemlegomba	\N	Védett	1554	45171	gombák, zuzmók
Solorina saccata	pettyegetett tárcsalapony	\N	Védett	1554	45172	gombák, zuzmók
Squamanita schreieri	sárga pikkelyesgalóca	\N	Védett	1554	45173	gombák, zuzmók
Strobilomyces strobilaceus	pikkelyes tinóru	\N	Védett	1554	45174	gombák, zuzmók
Tulostoma volvulatum	bocskoros nyelespöfeteg	\N	Védett	1554	45175	gombák, zuzmók
Umbilicaria deusta	korpás csigalapony	\N	Védett	1554	45176	gombák, zuzmók
Umbilicaria hirsuta	bozontos csigalapony	\N	Védett	1554	45177	gombák, zuzmók
Umbilicaria polyphylla	soklombú csigalapony	\N	Védett	1554	45178	gombák, zuzmók
Usnea florida	virágos szakállzuzmó	\N	Védett	1554	45179	gombák, zuzmók
Volvariella bombycina	óriás bocskorosgomba	\N	Védett	1554	45180	gombák, zuzmók
Xanthoparmelia pokornyi	Pokorny-bodrány	\N	Védett	1554	45181	gombák, zuzmók
Xanthoparmelia pseudohungarica	magyar bodrány	\N	Védett	1554	45182	gombák, zuzmók
Xanthoparmelia pulvinaris	magyar bodrány	\N	Védett	1554	45183	gombák, zuzmók
Xanthoparmelia ryssolea	homoki bodrány	\N	Védett	1554	45184	gombák, zuzmók
Xanthoparmelia subdiffluens	terülékeny bodrány	\N	Védett	1554	45185	gombák, zuzmók
Acipenser gueldenstaedti	vágó tok	\N	Védett	1554	45186	halak
Acipenser nudiventris	sima tok	\N	Védett	1554	45187	halak
Acipenser ruthenus	kecsege	HD V	Nem védett	1554	45188	halak
Acipenser stellatus	sőregtok	\N	Védett	1554	45189	halak
Alburnoides bipunctatus	sujtásos küsz	\N	Védett	1554	45190	halak
Alosa immaculata	dunai nagyhering	\N	Védett	1554	45191	halak
Ameiurus melas	fekete törpeharcsa	\N	Inváziós	1554	45192	halak
Ameiurus nebulosus	törpeharcsa	\N	Inváziós	1554	45193	halak
Aspius aspius	balin	HD II, V	Nem védett	1554	45194	halak
Barbatula barbatula	kövicsík	\N	Védett	1554	45195	halak
Barbus barbus	rózsás márna	HD V	Nem védett	1554	45196	halak
Barbus meridionalis	Petényi-márna (magyar márna)	HD II, V	Fokozottan védett	1554	45197	halak
Carassius auratus	ezüstkárász	\N	Inváziós	1554	45198	halak
Carassius auratus gibelio	ezüstkárász	\N	Inváziós	1554	45199	halak
Carassius gibelio	ezüstkárász	\N	Inváziós	1554	45200	halak
Carassius gibelio auratus	ezüstkárász	\N	Inváziós	1554	45201	halak
Cobitis elongatoides	vágó csík	HD II	Védett	1554	45202	halak
Cobitis taenia	vágó csík	HD II	Védett	1554	45203	halak
Cottus gobio	botos kölönte	HD II	Védett	1554	45204	halak
Cottus poecilopus	cifra kölönte	\N	Védett	1554	45205	halak
Ctenopharyngodon idella	amur	\N	Inváziós	1554	45206	halak
Eudontomyzon danfordi	tiszai ingola	HD II	Fokozottan védett	1554	45207	halak
Eudontomyzon mariae	dunai ingola	HD II	Fokozottan védett	1554	45208	halak
Eudontomyzon vladykovi	Vladykov-ingola	HD II	Fokozottan védett	1554	45209	halak
Gobio albipinnatus	halványfoltú küllő	HD II	Védett	1554	45210	halak
Gobio gobio	fenékjáró küllő	\N	Védett	1554	45211	halak
Gobio kesslerii	homoki küllő	HD II	Fokozottan védett	1554	45212	halak
Gobio uranoscopus	felpillantó küllő	HD II	Fokozottan védett	1554	45213	halak
Gymnocephalus baloni	széles durbincs	HD II, IV	Védett	1554	45214	halak
Gymnocephalus schraetser	selymes durbincs	HD II, V	Védett	1554	45215	halak
Gymnocephalus schraetzer	selymes durbincs	HD II, V	Védett	1554	45216	halak
Hucho hucho	dunai galóca	HD II, V	Fokozottan védett	1554	45217	halak
Huso huso	viza	\N	Védett	1554	45218	halak
Hypophthalmichthys molitrix	fehér busa	\N	Inváziós	1554	45219	halak
Hypophthalmichthys nobilis	pettyes busa	\N	Inváziós	1554	45220	halak
Leucaspius delineatus	kurta baing	\N	Védett	1554	45222	halak
Leuciscus leuciscus	nyúldomolykó	\N	Védett	1554	45223	halak
Leuciscus souffia	vaskos csabak	\N	Védett	1554	45224	halak
Misgurnus fossilis	réti csík	HD II	Védett	1554	45225	halak
Neogobius fluviatilis	folyami géb	\N	Inváziós	1554	45226	halak
Neogobius gymnotrachelus	csupasztorkú géb	\N	Inváziós	1554	45227	halak
Neogobius kessleri	kessler-géb	\N	Inváziós	1554	45228	halak
Neogobius melanostomus	feketeszájú géb	\N	Inváziós	1554	45229	halak
Oncorhynchus mykiss	szivárványos pisztráng	\N	Inváziós	1554	45230	halak
Pelecus cultratus	garda	HD II, V	Nem védett	1554	45231	halak
Perccottus glenii	amurgéb	IAS	Inváziós	1554	45232	halak
Phoxinus phoxinus	fürge cselle	\N	Védett	1554	45233	halak
Pseudorasbora parva	kínai razbóra	IAS	Inváziós	1554	45234	halak
Rhodeus sericeus	szivárványos ökle	HD II	Védett	1554	45235	halak
Rhodeus sericeus amarus	szivárványos ökle	HD II	Védett	1554	45236	halak
Romanogobio albipinnatus	halványfoltú küllő	HD II	Védett	1554	45237	halak
Romanogobio kesslerii	homoki küllő	HD II	Fokozottan védett	1554	45238	halak
Romanogobio uranoscopus	felpillantó küllő	HD II	Fokozottan védett	1554	45239	halak
Romanogobio vladykovi	halványfoltú küllő	HD II	Védett	1554	45240	halak
Rutilus frisii	gyöngyös koncér	\N	Védett	1554	45241	halak
Rutilus pigus	leánykoncér	HD II, V	Védett	1554	45242	halak
Rutilus pigus virgo	leánykoncér	HD II, V	Védett	1554	45243	halak
Rutilus virgo	leánykoncér	HD II, V	Védett	1554	45244	halak
Sabanejewia aurata	törpecsík	HD II	Védett	1554	45245	halak
Telestes souffia	vaskos csabak	\N	Védett	1554	45246	halak
Thymallus thymallus	pénzes pér	\N	Védett	1554	45247	halak
Umbra krameri	lápi póc	HD II	Fokozottan védett	1554	45248	halak
Zingel streber	német bucó	HD II	Fokozottan védett	1554	45249	halak
Zingel zingel	magyar bucó	HD II, V	Fokozottan védett	1554	45250	halak
Anguis colchica	lábatlangyík	\N	Védett	1554	45252	hüllők
Anguis fragilis	lábatlangyík	\N	Védett	1554	45253	hüllők
Lepomis gibbosus	naphal	IAS	Inváziós	1554	45221	halak
Coluber caspius	haragos sikló	HD IV	Fokozottan védett	1554	45254	hüllők
Coronella austriaca	rézsikló	HD IV	Védett	1554	45255	hüllők
Dolichophis caspius	haragos sikló	HD IV	Fokozottan védett	1554	45256	hüllők
Elaphe longissima	erdei sikló	HD IV	Védett	1554	45257	hüllők
Emys orbicularis	mocsári teknős	HD II, IV	Védett	1554	45258	hüllők
Lacerta agilis	fürge gyík	HD IV	Védett	1554	45259	hüllők
Lacerta agilis argus	fürge gyík	HD IV	Védett	1554	45260	hüllők
Lacerta sp.	gyík-fajok	\N	Védett	1554	45261	hüllők
Lacerta viridis	zöld gyík	HD IV	Védett	1554	45262	hüllők
Lacerta vivipara	elevenszülő gyík	HD IV	Fokozottan védett	1554	45263	hüllők
Lacerta vivipara pannonica	elevenszülő gyík	HD IV	Fokozottan védett	1554	45264	hüllők
Natrix natrix	vízisikló	\N	Védett	1554	45265	hüllők
Natrix sp.	sikló-fajok	\N	Védett	1554	45266	hüllők
Natrix tessellata	kockás sikló	HD IV	Védett	1554	45267	hüllők
Podarcis muralis	fali gyík	HD IV	Védett	1554	45268	hüllők
Podarcis sp.	gyík-fajok	\N	Védett	1554	45269	hüllők
Podarcis taurica	homoki gyík	HD IV	Védett	1554	45270	hüllők
Reptilia	hüllők	\N	Védett	1554	45271	hüllők
Trachemys scripta	ékszerteknős	IAS	Inváziós	1554	45272	hüllők
Trachemys scripta elegans	vörösfülű ékszerteknős	IAS	Inváziós	1554	45273	hüllők
Trachemys scripta scripta	sárgafülű ékszerteknős	IAS	Inváziós	1554	45274	hüllők
Vipera berus	keresztes vipera	\N	Fokozottan védett	1554	45275	hüllők
Vipera sp.	vipera-fajok	\N	Védett	1554	45276	hüllők
Vipera ursinii	rákosi vipera	HD II kiemelt, IV	Fokozottan védett	1554	45277	hüllők
Vipera ursinii rakosiensis	rákosi vipera	HD II kiemelt, IV	Fokozottan védett	1554	45278	hüllők
Zamenis longissimus	erdei sikló	HD IV	Védett	1554	45279	hüllők
Zootoca vivipara	elevenszülő gyík	HD IV	Fokozottan védett	1554	45280	hüllők
Amphibia	kétéltűek	\N	Védett	1554	45281	kétéltűek
Anura	békák	\N	Védett	1554	45282	kétéltűek
Bombina bombina	vöröshasú unka	HD II, IV	Védett	1554	45283	kétéltűek
Bombina sp.	unka-fajok	\N	Védett	1554	45284	kétéltűek
Bombina variegata	sárgahasú unka	HD II, IV	Védett	1554	45285	kétéltűek
Bufo bufo	barna varangy	\N	Védett	1554	45286	kétéltűek
Bufo sp.	varangy-fajok	\N	Védett	1554	45287	kétéltűek
Bufo viridis	zöld varangy	HD IV	Védett	1554	45288	kétéltűek
Hyla arborea	zöld levelibéka	HD IV	Védett	1554	45289	kétéltűek
Lissotriton sp.	gőte-fajok	\N	Védett	1554	45290	kétéltűek
Lissotriton vulgaris	pettyes gőte	\N	Védett	1554	45291	kétéltűek
Lithobates catesbeianus	amerikai ökörbéka	IAS	Inváziós	1554	45292	kétéltűek
Mesotriton alpestris	alpesi gőte	\N	Fokozottan védett	1554	45293	kétéltűek
Pelobates fuscus	barna ásóbéka	HD IV	Védett	1554	45294	kétéltűek
Pelophylax kl. esculentus	kecskebéka	HD V	Védett	1554	45295	kétéltűek
Pelophylax lessonae	kis tavibéka	HD IV	Védett	1554	45296	kétéltűek
Pelophylax ridibundus	kacagó béka (tavibéka)	HD V	Védett	1554	45297	kétéltűek
Pelophylax sp.	béka-fajok	\N	Védett	1554	45298	kétéltűek
Rana arvalis	mocsári béka	HD IV	Védett	1554	45299	kétéltűek
Rana catesbeianus	amerikai ökörbéka	IAS	Inváziós	1554	45300	kétéltűek
Rana dalmatina	erdei béka	HD IV	Védett	1554	45301	kétéltűek
Rana esculenta	kecskebéka	HD V	Védett	1554	45302	kétéltűek
Rana lessonae	kis tavibéka	HD IV	Védett	1554	45303	kétéltűek
Rana ridibunda	kacagó béka (tavibéka)	HD V	Védett	1554	45304	kétéltűek
Rana sp.	béka-fajok	\N	Védett	1554	45305	kétéltűek
Rana temporaria	gyepi béka	HD V	Védett	1554	45306	kétéltűek
Salamandra salamandra	foltos szalamandra	\N	Védett	1554	45307	kétéltűek
Triturus alpestris	alpesi gőte	\N	Fokozottan védett	1554	45308	kétéltűek
Triturus carnifex	alpesi tarajosgőte	HD II, IV	Védett	1554	45309	kétéltűek
Triturus cristatus	közönséges tarajosgőte	HD II, IV	Védett	1554	45310	kétéltűek
Triturus montandoni	kárpáti gőte	\N	8_melleklet	1554	45312	kétéltűek
Triturus sp.	gőte-fajok	\N	Védett	1554	45313	kétéltűek
Triturus vulgaris	pettyes gőte	\N	Védett	1554	45314	kétéltűek
Accipiter brevipes	kis héja	BD	Fokozottan védett	1554	45315	madarak
Accipiter gentilis	héja	BD_2	Védett	1554	45316	madarak
Accipiter nisus	karvaly	BD_2	Védett	1554	45317	madarak
Acrocephalus agricola	rozsdás nádiposzáta	BD_2	Védett	1554	45318	madarak
Acrocephalus arundinaceus	nádirigó	BD_2	Védett	1554	45319	madarak
Acrocephalus melanopogon	fülemülesitke	BD	Védett	1554	45320	madarak
Acrocephalus paludicola	csíkosfejű nádiposzáta	BD	Fokozottan védett	1554	45321	madarak
Acrocephalus palustris	énekes nádiposzáta	BD_2	Védett	1554	45322	madarak
Acrocephalus schoenobaenus	foltos nádiposzáta	BD_2	Védett	1554	45323	madarak
Acrocephalus scirpaceus	cserregő nádiposzáta	BD_2	Védett	1554	45324	madarak
Actitis hypoleucos	billegetőcankó	BD_2	Védett	1554	45325	madarak
Aegithalos caudatus	őszapó	BD_2	Védett	1554	45326	madarak
Aegolius funereus	gatyáskuvik	BD	Védett	1554	45327	madarak
Aegypius monachus	barátkeselyű	BD	Fokozottan védett	1554	45328	madarak
Alauda arvensis	mezei pacsirta	BD_2	Védett	1554	45329	madarak
Alca torda	alka	BD_2	Védett	1554	45330	madarak
Alcedo atthis	jégmadár	BD	Védett	1554	45331	madarak
Alopochen aegyptiaca	nílusi lúd	IAS	Inváziós	1554	45332	madarak
Lyrurus tetrix	nyírfajd	\N	Védett	1554	45570	madarak
Alopochen aegyptiacus 	nílusi lúd	IAS	Inváziós	1554	45333	madarak
Anas acuta	nyílfarkú réce	BD_2	Védett	1554	45334	madarak
Anas clypeata	kanalas réce	BD_2	Védett	1554	45335	madarak
Anas crecca	csörgő réce	BD_2	Védett	1554	45336	madarak
Anas penelope	fütyülő réce	BD_2	Védett	1554	45337	madarak
Anas platyrhynchos	tőkés réce	BD_2	Nem védett	1554	45338	madarak
Anas querquedula	böjti réce	BD_2	Fokozottan védett	1554	45339	madarak
Anas strepera	kendermagos réce	BD_2	Védett	1554	45340	madarak
Anser albifrons	nagy lilik	BD_2	Nem védett	1554	45341	madarak
Anser anser	nyári lúd	BD_2	Nem védett	1554	45342	madarak
Anser brachyrhynchus	rövidcsőrű lúd	BD_2	Védett	1554	45343	madarak
Anser erythropus	kis lilik	BD	Fokozottan védett	1554	45344	madarak
Anser fabalis	vetési lúd	BD_2	Nem védett	1554	45345	madarak
Anthropoides virgo	pártás daru	BD_2	Védett	1554	45346	madarak
Anthus campestris	parlagi pityer	BD	Védett	1554	45347	madarak
Anthus cervinus	rozsdástorkú pityer	BD_2	Védett	1554	45348	madarak
Anthus pratensis	réti pityer	BD_2	Védett	1554	45349	madarak
Anthus spinoletta	havasi pityer	BD_2	Védett	1554	45350	madarak
Anthus trivialis	erdei pityer	BD_2	Védett	1554	45351	madarak
Apus apus	sarlósfecske	BD_2	Védett	1554	45352	madarak
Apus melba	havasi sarlósfecske	BD_2	Védett	1554	45353	madarak
Apus pallidus	halvány sarlósfecske	BD_2	Védett	1554	45354	madarak
Aquila chrysaetos	szirti sas	BD	Fokozottan védett	1554	45355	madarak
Aquila clanga	fekete sas	BD	Fokozottan védett	1554	45356	madarak
Aquila fasciata	héjasas	\N	Fokozottan védett	1554	45357	madarak
Aquila heliaca	parlagi sas	BD	Fokozottan védett	1554	45358	madarak
Aquila nipalensis	pusztai sas	\N	Fokozottan védett	1554	45359	madarak
Aquila pennata	törpesas	\N	Fokozottan védett	1554	45360	madarak
Aquila pomarina	békászó sas	BD	Fokozottan védett	1554	45361	madarak
Ardea alba	nagy kócsag	BD	Fokozottan védett	1554	45362	madarak
Ardea cinerea	szürke gém	BD_2	Védett	1554	45363	madarak
Ardea purpurea	vörös gém	BD	Fokozottan védett	1554	45364	madarak
Ardeola ralloides	üstökösgém	BD	Fokozottan védett	1554	45365	madarak
Arenaria interpres	kőforgató	BD_2	Védett	1554	45366	madarak
Asio flammeus	réti fülesbagoly	BD	Fokozottan védett	1554	45367	madarak
Asio otus	erdei fülesbagoly	BD_2	Védett	1554	45368	madarak
Athene noctua	kuvik	\N	Fokozottan védett	1554	45369	madarak
Aythya ferina	barátréce	BD_2	Védett	1554	45370	madarak
Aythya fuligula	kontyos réce	BD_2	Védett	1554	45371	madarak
Aythya marila	hegyi réce	BD_2	Védett	1554	45372	madarak
Aythya nyroca	cigányréce	BD	Fokozottan védett	1554	45373	madarak
Bombycilla garrulus	csonttollú	BD_2	Védett	1554	45374	madarak
Bonasa bonasia	császármadár	BD	Fokozottan védett	1554	45375	madarak
Botaurus stellaris	bölömbika	BD	Fokozottan védett	1554	45376	madarak
Branta bernicla	örvös lúd	BD_2	Védett	1554	45377	madarak
Branta canadensis	kanadai lúd	\N	Inváziós	1554	45378	madarak
Branta leucopsis	apácalúd	BD	Védett	1554	45379	madarak
Branta ruficollis	vörösnyakú lúd	BD	Fokozottan védett	1554	45380	madarak
Bubo bubo	uhu	BD	Fokozottan védett	1554	45381	madarak
Bubo scandiacus	hóbagoly	BD	Fokozottan védett	1554	45382	madarak
Bubulcus ibis	pásztorgém	BD_2	Védett	1554	45383	madarak
Bucephala clangula	kerceréce	BD_2	Védett	1554	45384	madarak
Burhinus oedicnemus	ugartyúk	BD	Fokozottan védett	1554	45385	madarak
Buteo buteo	egerészölyv	BD_2	Védett	1554	45386	madarak
Buteo lagopus	gatyás ölyv	BD_2	Védett	1554	45387	madarak
Buteo rufinus	pusztai ölyv	BD	Fokozottan védett	1554	45388	madarak
Calandrella brachydactyla	szikipacsirta	BD	Fokozottan védett	1554	45389	madarak
Calcarius lapponicus	sarkantyús sármány	BD_2	Védett	1554	45390	madarak
Calidris alba	fenyérfutó	BD_2	Védett	1554	45391	madarak
Calidris alpina	havasi partfutó	BD_2	Védett	1554	45392	madarak
Calidris canutus	sarki partfutó	BD_2	Védett	1554	45393	madarak
Calidris ferruginea	sarlós partfutó	BD_2	Védett	1554	45394	madarak
Calidris maritima	tengeri partfutó	BD_2	Védett	1554	45395	madarak
Calidris melanotos	vándorpartfutó	\N	Védett	1554	45396	madarak
Calidris minuta	apró partfutó	BD_2	Védett	1554	45397	madarak
Calidris temminckii	Temminck-partfutó	BD_2	Védett	1554	45398	madarak
Caprimulgus europaeus	lappantyú	BD	Védett	1554	45399	madarak
Carduelis cannabina	kenderike	BD_2	Védett	1554	45400	madarak
Carduelis carduelis	tengelic	BD_2	Védett	1554	45401	madarak
Carduelis chloris	zöldike	BD_2	Védett	1554	45402	madarak
Carduelis flammea	zsezse	BD_2	Védett	1554	45403	madarak
Carduelis flammea cabaret	barna zsezse	BD_2	Védett	1554	45404	madarak
Carduelis flavirostris	sárgacsőrű kenderike	BD_2	Védett	1554	45405	madarak
Carduelis hornemanni	szürke zsezse	BD_2	Védett	1554	45406	madarak
Carduelis spinus	csíz	BD_2	Védett	1554	45407	madarak
Carpodacus erythrinus	karmazsinpirók	BD_2	Védett	1554	45408	madarak
Casmerodius albus	nagy kócsag	BD	Fokozottan védett	1554	45409	madarak
Cecropis daurica	vörhenyes fecske	BD_2	Védett	1554	45410	madarak
Cercotrichas galactotes	tüskebujkáló	BD_2	8_melleklet	1554	45411	madarak
Podiceps cristatus	búbos vöcsök	BD_2	Védett	1554	45649	madarak
Certhia brachydactyla	rövidkarmú fakusz	BD_2	Védett	1554	45412	madarak
Certhia familiaris	hegyi fakusz	BD_2	Védett	1554	45413	madarak
Cettia cetti	berki poszáta	BD_2	Védett	1554	45414	madarak
Charadrius alexandrinus	széki lile	BD	Fokozottan védett	1554	45415	madarak
Charadrius dubius	kis lile	BD_2	Védett	1554	45416	madarak
Charadrius hiaticula	parti lile	BD_2	Védett	1554	45417	madarak
Charadrius leschenaultii	sivatagi lile	BD_2	Védett	1554	45418	madarak
Charadrius morinellus	havasi lile	BD	Védett	1554	45419	madarak
Chettusia gregaria	lilebíbic	\N	Fokozottan védett	1554	45420	madarak
Chettusia leucura	fehérfarkú lilebíbic	BD_2	Védett	1554	45421	madarak
Chlidonias hybrida	fattyúszerkő	BD	Fokozottan védett	1554	45422	madarak
Chlidonias hybridus	fattyúszerkő	BD	Fokozottan védett	1554	45423	madarak
Chlidonias leucopterus	fehérszárnyú szerkő	BD_2	Fokozottan védett	1554	45424	madarak
Chlidonias niger	kormos szerkő	BD	Fokozottan védett	1554	45425	madarak
Chroicocephalus genei	vékonycsőrű sirály	BD	Védett	1554	45426	madarak
Chroicocephalus ridibundus	dankasirály	BD_2	Védett	1554	45427	madarak
Ciconia ciconia	fehér gólya	BD	Fokozottan védett	1554	45428	madarak
Ciconia nigra	fekete gólya	BD	Fokozottan védett	1554	45429	madarak
Cinclus cinclus	vízirigó	BD_2	Fokozottan védett	1554	45430	madarak
Circaetus gallicus	kígyászölyv	BD	Fokozottan védett	1554	45431	madarak
Circus aeruginosus	barna rétihéja	BD	Védett	1554	45432	madarak
Circus cyaneus	kékes rétihéja	BD	Védett	1554	45433	madarak
Circus macrourus	fakó rétihéja	BD	Fokozottan védett	1554	45434	madarak
Circus pygargus	hamvas rétihéja	BD	Fokozottan védett	1554	45435	madarak
Cisticola juncidis	szuharbújó	\N	Védett	1554	45436	madarak
Clamator glandarius	pettyes kakukk	\N	Védett	1554	45437	madarak
Clangula hyemalis	jegesréce	\N	Fokozottan védett	1554	45438	madarak
Coccothraustes coccothraustes	meggyvágó	BD_2	Védett	1554	45439	madarak
Columba livia	szirti galamb	\N	8_melleklet	1554	45440	madarak
Columba oenas	kék galamb	BD_2	Védett	1554	45441	madarak
Coracias garrulus	szalakóta	BD	Fokozottan védett	1554	45442	madarak
Corvus corax	holló	\N	Védett	1554	45443	madarak
Corvus corone	kormos varjú	\N	Védett	1554	45444	madarak
Corvus corone corone	kormos varjú	\N	Védett	1554	45445	madarak
Corvus frugilegus	vetési varjú	BD_2	Védett	1554	45446	madarak
Corvus monedula	csóka	BD_2	Védett	1554	45447	madarak
Corvus splendens	házi varjú	IAS	Inváziós	1554	45448	madarak
Coturnix coturnix	fürj	BD_2	Védett	1554	45449	madarak
Crex crex	haris	BD	Fokozottan védett	1554	45450	madarak
Cuculus canorus	kakukk	BD_2	Védett	1554	45451	madarak
Curruca communis	mezei poszáta	BD_2	Védett	1554	45452	madarak
Curruca curruca	kis poszáta	BD_2	Védett	1554	45453	madarak
Curruca nisoria	karvalyposzáta	BD	Védett	1554	45454	madarak
Cyanistes caeruleus	kék cinege	BD_2	Védett	1554	45455	madarak
Cygnus columbianus	kis hattyú	\N	Védett	1554	45456	madarak
Cygnus cygnus	énekes hattyú	BD	Védett	1554	45457	madarak
Cygnus olor	bütykös hattyú	BD_2	8_melleklet	1554	45458	madarak
Delichon urbica	molnárfecske	BD_2	Védett	1554	45459	madarak
Delichon urbicum	molnárfecske	BD_2	Védett	1554	45460	madarak
Dendrocopos leucotos	fehérhátú fakopáncs	BD	Fokozottan védett	1554	45461	madarak
Dendrocopos major	nagy fakopáncs	\N	Védett	1554	45462	madarak
Dendrocopos medius	középfakopáncs	BD	Védett	1554	45463	madarak
Dendrocopos minor	kis fakopáncs	\N	Védett	1554	45464	madarak
Dendrocopos sp.	fakopáncs	\N	Védett	1554	45465	madarak
Dendrocopos syriacus	balkáni fakopáncs	BD	Védett	1554	45466	madarak
Dryobates minor	kis fakopáncs	\N	Védett	1554	45467	madarak
Dryocopus martius	fekete harkály	BD	Védett	1554	45468	madarak
Egretta alba	nagy kócsag	BD	Fokozottan védett	1554	45469	madarak
Egretta garzetta	kis kócsag	BD	Fokozottan védett	1554	45470	madarak
Elanus caeruleus	kuhi	BD	Védett	1554	45471	madarak
Emberiza calandra	sordély	BD_2	Védett	1554	45472	madarak
Emberiza cia	bajszos sármány	BD_2	Fokozottan védett	1554	45473	madarak
Emberiza cirlus	sövénysármány	BD_2	Védett	1554	45474	madarak
Emberiza citrinella	citromsármány	BD_2	Védett	1554	45475	madarak
Emberiza hortulana	kerti sármány	BD	Fokozottan védett	1554	45476	madarak
Emberiza leucocephalos	fenyősármány	BD_2	Védett	1554	45477	madarak
Emberiza melanocephala	kucsmás sármány	BD_2	Védett	1554	45478	madarak
Emberiza pusilla	törpesármány	BD_2	Védett	1554	45479	madarak
Emberiza rustica	erdei sármány	BD_2	Védett	1554	45480	madarak
Emberiza schoeniclus	nádi sármány	BD_2	Védett	1554	45481	madarak
Eremophila alpestris	havasi fülespacsirta	\N	Védett	1554	45482	madarak
Ereunetes alpina	havasi partfutó	BD_2	Védett	1554	45483	madarak
Ereunetes ferrugineus	sarlós partfutó	BD_2	Védett	1554	45484	madarak
Ereunetes temminckii	Temminck-partfutó	BD_2	Védett	1554	45485	madarak
Erithacus rubecula	vörösbegy	\N	Védett	1554	45486	madarak
Falco biarmicus	Feldegg-sólyom	BD	Fokozottan védett	1554	45487	madarak
Falco cherrug	kerecsensólyom	BD	Fokozottan védett	1554	45488	madarak
Falco columbarius	kis sólyom	BD	Védett	1554	45489	madarak
Falco eleonorae	Eleonóra-sólyom	BD	Fokozottan védett	1554	45490	madarak
Falco naumanni	fehérkarmú vércse	BD	Fokozottan védett	1554	45491	madarak
Falco peregrinus	vándorsólyom	BD	Fokozottan védett	1554	45492	madarak
Falco rusticolus	északi sólyom	BD	Fokozottan védett	1554	45493	madarak
Falco subbuteo	kabasólyom	BD_2	Védett	1554	45494	madarak
Falco tinnunculus	vörös vércse	BD_2	Védett	1554	45495	madarak
Falco vespertinus	kék vércse	BD	Fokozottan védett	1554	45496	madarak
Ficedula albicollis	örvös légykapó	BD	Védett	1554	45497	madarak
Ficedula hypoleuca	kormos légykapó	BD_2	Védett	1554	45498	madarak
Ficedula parva	kis légykapó	BD	Fokozottan védett	1554	45499	madarak
Ficedula sp.	légykapó	\N	Védett	1554	45500	madarak
Fratercula arctica	lunda	BD_2	Védett	1554	45501	madarak
Fringilla coelebs	erdei pinty	BD_2	Védett	1554	45502	madarak
Fringilla montifringilla	fenyőpinty	BD_2	Védett	1554	45503	madarak
Fulica atra	szárcsa	BD_2	Nem védett	1554	45504	madarak
Galerida cristata	búbos pacsirta	BD_2	Védett	1554	45505	madarak
Gallinago gallinago	sárszalonka	BD_2	Fokozottan védett	1554	45506	madarak
Gallinago media	nagy sárszalonka	\N	Fokozottan védett	1554	45507	madarak
Gallinula chloropus	vízityúk	BD_2	Védett	1554	45508	madarak
Gavia arctica	sarki búvár	BD	Védett	1554	45509	madarak
Gavia immer	jeges búvár	BD	Védett	1554	45510	madarak
Gavia stellata	északi búvár	BD	Védett	1554	45511	madarak
Gelochelidon nilotica	kacagócsér	BD	Védett	1554	45512	madarak
Glareola nordmanni	feketeszárnyú székicsér	BD_2	Fokozottan védett	1554	45513	madarak
Glareola pratincola	székicsér	BD	Fokozottan védett	1554	45514	madarak
Glaucidium passerinum	törpekuvik	BD	Védett	1554	45515	madarak
Grus grus	daru	BD	Védett	1554	45516	madarak
Grus virgo	pártás daru	BD_2	Védett	1554	45517	madarak
Gyps fulvus	fakó keselyű	\N	Fokozottan védett	1554	45518	madarak
Haematopus ostralegus	csigaforgató	BD_2	Védett	1554	45519	madarak
Haliaeetus albicilla	rétisas	BD	Fokozottan védett	1554	45520	madarak
Hieraaetus fasciatus	héjasas	\N	Fokozottan védett	1554	45521	madarak
Hieraaetus pennatus	törpesas	\N	Fokozottan védett	1554	45522	madarak
Himantopus himantopus	gólyatöcs	BD	Fokozottan védett	1554	45523	madarak
Hippolais icterina	kerti geze	BD_2	Védett	1554	45524	madarak
Hippolais pallida	halvány geze	BD_2	Védett	1554	45525	madarak
Hirundo daurica	vörhenyes fecske	BD_2	Védett	1554	45526	madarak
Hirundo rustica	füsti fecske	BD_2	Védett	1554	45527	madarak
Hoplopterus spinosus	tüskés bíbic	BD	Védett	1554	45528	madarak
Hydrocoloeus minutus	kis sirály	BD	Védett	1554	45529	madarak
Hydroprogne caspia	lócsér	BD	Védett	1554	45530	madarak
Ichthyaetus ichthyaetus	halászsirály	BD_2	Védett	1554	45531	madarak
Ichthyaetus melanocephalus	szerecsensirály	BD	Fokozottan védett	1554	45532	madarak
Iduna pallida	halvány geze	BD_2	Védett	1554	45533	madarak
Ixobrychus minutus	törpegém	BD	Fokozottan védett	1554	45534	madarak
Jynx torquilla	nyaktekercs	BD_2	Védett	1554	45535	madarak
Lanius collurio	tövisszúró gébics	BD	Védett	1554	45536	madarak
Lanius excubitor	nagy őrgébics	BD_2	Védett	1554	45537	madarak
Lanius minor	kis őrgébics	BD	Védett	1554	45538	madarak
Lanius senator	vörösfejű gébics	BD_2	Védett	1554	45539	madarak
Larus argentatus	ezüstsirály	BD_2	Védett	1554	45540	madarak
Larus audouinii	korallsirály	BD	Védett	1554	45541	madarak
Larus cachinnans	sztyeppi sirály	BD_2	8_melleklet	1554	45542	madarak
Larus canus	viharsirály	BD_2	Védett	1554	45543	madarak
Larus fuscus	heringsirály	BD_2	Védett	1554	45544	madarak
Larus fuscus graellsii	heringsirály	BD_2	Védett	1554	45545	madarak
Larus genei	vékonycsőrű sirály	BD	Védett	1554	45546	madarak
Larus glaucoides	sarki sirály	\N	Védett	1554	45547	madarak
Larus hyperboreus	jeges sirály	BD_2	Védett	1554	45548	madarak
Larus ichthyaetus	halászsirály	BD_2	Védett	1554	45549	madarak
Larus marinus	dolmányos sirály	BD_2	Védett	1554	45550	madarak
Larus melanocephalus	szerecsensirály	BD	Fokozottan védett	1554	45551	madarak
Larus michahellis	sárgalábú sirály	BD_2	8_melleklet	1554	45552	madarak
Larus minutus	kis sirály	BD	Védett	1554	45553	madarak
Larus ridibundus	dankasirály	BD_2	Védett	1554	45554	madarak
Leiopicus medius	középfakopáncs	BD	Védett	1554	45555	madarak
Limicola falcinellus	sárjáró	BD_2	Védett	1554	45556	madarak
Limosa lapponica	kis goda	BD	Védett	1554	45557	madarak
Limosa limosa	nagy goda	BD_2	Fokozottan védett	1554	45558	madarak
Locustella fluviatilis	berki tücsökmadár	BD_2	Védett	1554	45559	madarak
Locustella luscinioides	nádi tücsökmadár	BD_2	Védett	1554	45560	madarak
Locustella naevia	réti tücsökmadár	BD_2	Védett	1554	45561	madarak
Lophophanes cristatus	búbos cinege	\N	Védett	1554	45562	madarak
Loxia curvirostra	keresztcsőrű	BD_2	Védett	1554	45563	madarak
Loxia leucoptera	szalagos keresztcsőrű	\N	Védett	1554	45564	madarak
Lullula arborea	erdei pacsirta	BD	Védett	1554	45565	madarak
Luscinia luscinia	nagy fülemüle	BD_2	Fokozottan védett	1554	45566	madarak
Luscinia megarhynchos	fülemüle	BD_2	Védett	1554	45567	madarak
Luscinia svecica	kékbegy	BD	Védett	1554	45568	madarak
Lymnocryptes minimus	kis sárszalonka	BD_2	Védett	1554	45569	madarak
Marmaronetta angustirostris	márványos réce	\N	Fokozottan védett	1554	45571	madarak
Melanitta fusca	füstös réce	BD_2	Fokozottan védett	1554	45572	madarak
Melanitta nigra	fekete réce	BD_2	Védett	1554	45573	madarak
Melanocorypha calandra	kalandrapacsirta	BD	Védett	1554	45574	madarak
Mergellus albellus	kis bukó	BD	Védett	1554	45575	madarak
Mergus albellus	kis bukó	BD	Védett	1554	45576	madarak
Mergus merganser	nagy bukó	BD_2	Védett	1554	45577	madarak
Mergus serrator	örvös bukó	BD_2	Védett	1554	45578	madarak
Merops apiaster	gyurgyalag	BD_2	Fokozottan védett	1554	45579	madarak
Microcarbo pygmeus	kis kárókatona	BD	Fokozottan védett	1554	45580	madarak
Miliaria calandra	sordély	BD_2	Védett	1554	45581	madarak
Milvus migrans	barna kánya	BD	Fokozottan védett	1554	45582	madarak
Milvus milvus	vörös kánya	BD	Fokozottan védett	1554	45583	madarak
Monticola saxatilis	kövirigó	BD_2	Fokozottan védett	1554	45584	madarak
Monticola solitarius	kék kövirigó	BD_2	Védett	1554	45585	madarak
Montifringilla nivalis	havasipinty	\N	Védett	1554	45586	madarak
Motacilla alba	barázdabillegető	BD_2	Védett	1554	45587	madarak
Motacilla cinerea	hegyi billegető	BD_2	Védett	1554	45588	madarak
Motacilla citreola	citrombillegető	BD_2	Védett	1554	45589	madarak
Motacilla flava	sárga billegető	BD_2	Védett	1554	45590	madarak
Muscicapa striata	szürke légykapó	BD_2	Védett	1554	45591	madarak
Neophron percnopterus	dögkeselyű	BD	Fokozottan védett	1554	45592	madarak
Netta rufina	üstökös réce	BD_2	Védett	1554	45593	madarak
Nucifraga caryocatactes	fenyőszajkó	BD_2	Védett	1554	45594	madarak
Numenius arquata	nagy póling	BD_2	Fokozottan védett	1554	45595	madarak
Numenius phaeopus	kis póling	BD_2	Védett	1554	45596	madarak
Numenius tenuirostris	vékonycsőrű póling	BD	Fokozottan védett	1554	45597	madarak
Nyctea scandiaca	hóbagoly	BD	Fokozottan védett	1554	45598	madarak
Nycticorax nycticorax	bakcsó	BD	Fokozottan védett	1554	45599	madarak
Oenanthe hispanica	déli hantmadár	BD_2	Védett	1554	45600	madarak
Oenanthe isabellina	pusztai hantmadár	BD_2	Védett	1554	45601	madarak
Oenanthe oenanthe	hantmadár	BD_2	Védett	1554	45602	madarak
Oenanthe pleschanka	apácahantmadár	BD	Védett	1554	45603	madarak
Oriolus oriolus	sárgarigó	BD_2	Védett	1554	45604	madarak
Otis tarda	túzok	BD	Fokozottan védett	1554	45605	madarak
Otus scops	füleskuvik	BD_2	Fokozottan védett	1554	45606	madarak
Oxyura jamaicensis	halcsontfarkú réce	IAS	Inváziós	1554	45607	madarak
Oxyura leucocephala	kékcsőrű réce	BD	Fokozottan védett	1554	45608	madarak
Pandion haliaetus	halászsas	BD	Fokozottan védett	1554	45609	madarak
Panurus biarmicus	barkóscinege	\N	Védett	1554	45610	madarak
Parus ater	fenyvescinege	BD_2	Védett	1554	45611	madarak
Parus caeruleus	kék cinege	BD_2	Védett	1554	45612	madarak
Parus cristatus	búbos cinege	\N	Védett	1554	45613	madarak
Parus major	széncinege	\N	Védett	1554	45614	madarak
Parus montanus	kormosfejű cinege	\N	Védett	1554	45615	madarak
Parus palustris	barátcinege	\N	Védett	1554	45616	madarak
Passer domesticus	házi veréb	\N	8_melleklet	1554	45617	madarak
Passer montanus	mezei veréb	\N	Védett	1554	45618	madarak
Pastor roseus	pásztormadár	BD_2	Védett	1554	45619	madarak
Pelecanus crispus	borzas gödény	BD	Fokozottan védett	1554	45620	madarak
Pelecanus onocrotalus	rózsás gödény	BD	Fokozottan védett	1554	45621	madarak
Periparus ater	fenyvescinege	BD_2	Védett	1554	45622	madarak
Pernis apivorus	darázsölyv	BD	Fokozottan védett	1554	45623	madarak
Phalacrocorax carbo	kárókatona	BD_2	8_melleklet	1554	45624	madarak
Phalacrocorax pygmeus	kis kárókatona	BD	Fokozottan védett	1554	45625	madarak
Phalaropus fulicarius	laposcsőrű víztaposó	BD_2	Védett	1554	45626	madarak
Phalaropus lobatus	vékonycsőrű víztaposó	BD	Védett	1554	45627	madarak
Phalaropus sp.	víztaposó	\N	Védett	1554	45628	madarak
Philomachus pugnax	pajzsoscankó	BD	Védett	1554	45629	madarak
Phoenicopterus ruber	rózsás flamingó	BD	Védett	1554	45630	madarak
Phoenicurus ochruros	házi rozsdafarkú	BD_2	Védett	1554	45631	madarak
Phoenicurus phoenicurus	kerti rozsdafarkú	BD_2	Védett	1554	45632	madarak
Phylloscopus bonelli	Bonelli-füzike	\N	Védett	1554	45633	madarak
Phylloscopus collybita	csilpcsalpfüzike	BD_2	Védett	1554	45634	madarak
Phylloscopus inornatus	vándorfüzike	BD_2	Védett	1554	45635	madarak
Phylloscopus proregulus	királyfüzike	BD_2	Védett	1554	45636	madarak
Phylloscopus sibilatrix	sisegő füzike	BD_2	Védett	1554	45637	madarak
Phylloscopus trochilus	fitiszfüzike	BD_2	Védett	1554	45638	madarak
Picus canus	hamvas küllő	BD	Védett	1554	45639	madarak
Picus viridis	zöld küllő	\N	Védett	1554	45640	madarak
Pinicola enucleator	nagy pirók	BD_2	Védett	1554	45641	madarak
Platalea leucorodia	kanalasgém	BD	Fokozottan védett	1554	45642	madarak
Plectrophenax nivalis	hósármány	BD_2	Védett	1554	45643	madarak
Plegadis falcinellus	batla	BD	Fokozottan védett	1554	45644	madarak
Pluvialis apricaria	aranylile	BD	Védett	1554	45645	madarak
Pluvialis dominicus	amerikai pettyeslile	\N	8_melleklet	1554	45646	madarak
Pluvialis squatarola	ezüstlile	BD_2	Védett	1554	45647	madarak
Podiceps auritus	füles vöcsök	BD	Védett	1554	45648	madarak
Podiceps grisegena	vörösnyakú vöcsök	BD_2	Fokozottan védett	1554	45650	madarak
Podiceps nigricollis	feketenyakú vöcsök	BD_2	Fokozottan védett	1554	45651	madarak
Podiceps ruficollis	kis vöcsök	BD_2	Védett	1554	45652	madarak
Poecile montanus	kormosfejű cinege	\N	Védett	1554	45653	madarak
Poecile palustris	barátcinege	\N	Védett	1554	45654	madarak
Polysticta stelleri	Steller-pehelyréce	\N	Fokozottan védett	1554	45655	madarak
Porphyrio porphyrio	kék fú	\N	Védett	1554	45656	madarak
Porzana parva	kis vízicsibe	BD	Védett	1554	45657	madarak
Porzana porzana	pettyes vízicsibe	BD	Védett	1554	45658	madarak
Porzana pusilla	törpevízicsibe	BD	Fokozottan védett	1554	45659	madarak
Prunella collaris	havasi szürkebegy	BD_2	Védett	1554	45660	madarak
Prunella modularis	erdei szürkebegy	BD_2	Védett	1554	45661	madarak
Pyrrhocorax graculus	havasi csóka	\N	Védett	1554	45662	madarak
Pyrrhocorax pyrrhocorax	havasi varjú	BD	Védett	1554	45663	madarak
Pyrrhula pyrrhula	süvöltő	BD_2	Védett	1554	45664	madarak
Rallus aquaticus	guvat	BD_2	Védett	1554	45665	madarak
Recurvirostra avosetta	gulipán	BD	Fokozottan védett	1554	45666	madarak
Regulus ignicapilla	tüzesfejű királyka	BD_2	Védett	1554	45667	madarak
Regulus ignicapillus	tüzesfejű királyka	BD_2	Védett	1554	45668	madarak
Regulus regulus	sárgafejű királyka	BD_2	Védett	1554	45669	madarak
Remiz pendulinus	függőcinege	BD_2	Védett	1554	45670	madarak
Riparia riparia	partifecske	BD_2	Védett	1554	45671	madarak
Rissa tridactyla	csüllő	BD_2	Védett	1554	45672	madarak
Saxicola rubetra	rozsdás csuk	BD_2	Védett	1554	45673	madarak
Saxicola rubicola	cigánycsuk	BD_2	Védett	1554	45674	madarak
Saxicola torquata	cigánycsuk	BD_2	Védett	1554	45675	madarak
Saxicola torquatus	cigánycsuk	BD_2	Védett	1554	45676	madarak
Serinus serinus	csicsörke	BD_2	Védett	1554	45677	madarak
Sitta europaea	csuszka	\N	Védett	1554	45678	madarak
Somateria mollissima	pehelyréce	BD_2	Védett	1554	45679	madarak
Somateria spectabilis	cifra pehelyréce	BD_2	Védett	1554	45680	madarak
Stercorarius longicaudus	nyílfarkú halfarkas	BD_2	Védett	1554	45681	madarak
Stercorarius parasiticus	ékfarkú halfarkas	BD_2	Védett	1554	45682	madarak
Stercorarius pomarinus	szélesfarkú halfarkas	BD_2	Védett	1554	45683	madarak
Stercorarius skua	nagy halfarkas	BD_2	Védett	1554	45684	madarak
Sterna albifrons	kis csér	BD	Fokozottan védett	1554	45685	madarak
Sterna caspia	lócsér	BD	Védett	1554	45686	madarak
Sterna hirundo	küszvágó csér	BD	Fokozottan védett	1554	45687	madarak
Sterna paradisaea	sarki csér	BD	Védett	1554	45688	madarak
Sterna sandvicensis	kenti csér	BD	Védett	1554	45689	madarak
Sternula albifrons	kis csér	BD	Fokozottan védett	1554	45690	madarak
Streptopelia turtur	vadgerle	BD_2	Védett	1554	45691	madarak
Strix aluco	macskabagoly	\N	Védett	1554	45692	madarak
Strix uralensis	uráli bagoly	BD	Fokozottan védett	1554	45693	madarak
Sturnus roseus	pásztormadár	BD_2	Védett	1554	45694	madarak
Sturnus vulgaris	seregély	BD_2	8_melleklet	1554	45695	madarak
Surnia ulula	karvalybagoly	BD	Védett	1554	45696	madarak
Sylvia atricapilla	barátposzáta	BD_2	Védett	1554	45697	madarak
Sylvia borin	kerti poszáta	BD_2	Védett	1554	45698	madarak
Sylvia cantillans	bajszos poszáta	BD_2	Védett	1554	45699	madarak
Sylvia communis	mezei poszáta	BD_2	Védett	1554	45700	madarak
Sylvia curruca	kis poszáta	BD_2	Védett	1554	45701	madarak
Sylvia melanocephala	kucsmás poszáta	BD_2	Védett	1554	45702	madarak
Sylvia nisoria	karvalyposzáta	BD	Védett	1554	45703	madarak
Tachybaptus ruficollis	kis vöcsök	BD_2	Védett	1554	45704	madarak
Tadorna ferruginea	vörös ásólúd	BD	Védett	1554	45705	madarak
Tadorna tadorna	bütykös ásólúd	BD_2	Védett	1554	45706	madarak
Tarsiger cyanurus	kékfarkú	BD_2	Védett	1554	45707	madarak
Tetrao tetrix	nyírfajd	\N	Védett	1554	45708	madarak
Tetrao urogallus	siketfajd	BD	Védett	1554	45709	madarak
Tetrastes bonasia	császármadár	BD	Fokozottan védett	1554	45710	madarak
Tetrax tetrax	reznek	BD	Fokozottan védett	1554	45711	madarak
Threskiornis aethiopicus	szent íbisz	IAS	Inváziós	1554	45712	madarak
Tichodroma muraria	hajnalmadár	BD_2	Védett	1554	45713	madarak
Tringa erythropus	füstös cankó	BD_2	Védett	1554	45714	madarak
Tringa glareola	réti cankó	BD	Védett	1554	45715	madarak
Tringa hypoleucos	billegetőcankó	BD_2	Védett	1554	45716	madarak
Tringa nebularia	szürke cankó	BD_2	Védett	1554	45717	madarak
Tringa ochropus	erdei cankó	BD_2	Védett	1554	45718	madarak
Tringa stagnatilis	tavi cankó	BD_2	Fokozottan védett	1554	45719	madarak
Tringa totanus	piroslábú cankó	BD_2	Fokozottan védett	1554	45720	madarak
Troglodytes troglodytes	ökörszem	BD_2	Védett	1554	45721	madarak
Tryngites subruficollis	cankópartfutó	\N	Védett	1554	45722	madarak
Turdus iliacus	szőlőrigó	BD_2	Védett	1554	45723	madarak
Turdus merula	fekete rigó	BD_2	Védett	1554	45724	madarak
Turdus philomelos	énekes rigó	BD_2	Védett	1554	45725	madarak
Turdus pilaris	fenyőrigó	BD_2	Védett	1554	45726	madarak
Turdus torquatus	örvös rigó	BD_2	Védett	1554	45727	madarak
Turdus viscivorus	léprigó	BD_2	Védett	1554	45728	madarak
Tyto alba	gyöngybagoly	BD_2	Fokozottan védett	1554	45729	madarak
Upupa epops	búbosbanka	BD_2	Védett	1554	45730	madarak
Vanellus gregarius	lilebíbic	\N	Fokozottan védett	1554	45731	madarak
Vanellus leucurus	fehérfarkú lilebíbic	BD_2	Védett	1554	45732	madarak
Vanellus spinosus	tüskés bíbic	BD	Védett	1554	45733	madarak
Vanellus vanellus	bíbic	BD_2	Védett	1554	45734	madarak
Xema sabini	fecskesirály	\N	Védett	1554	45735	madarak
Xenus cinereus	terekcankó	BD	Védett	1554	45736	madarak
Abutilon theophrasti	sárga selyemmályva	\N	Inváziós	1554	45737	növények
Acer negundo	zöld juhar	\N	Inváziós	1554	45738	növények
Achillea crithmifolia	hegyközi cickafark	\N	Védett	1554	45739	növények
Achillea horanszkyi	Horánszky-cickafark	\N	Fokozottan védett	1554	45740	növények
Achillea ochroleuca	homoki cickafark	\N	Védett	1554	45741	növények
Achillea ptarmica	kenyérbélcickafark	\N	Védett	1554	45742	növények
Achillea tuzsonii	Tuzson-cickafark	\N	Fokozottan védett	1554	45743	növények
Aconitum anthora	méregölő sisakvirág	\N	Védett	1554	45744	növények
Aconitum moldavicum	kárpáti sisakvirág	\N	Védett	1554	45745	növények
Aconitum sp.	sisakvirág-faj	\N	Védett	1554	45746	növények
Aconitum variegatum	karcsú sisakvirág	\N	Védett	1554	45747	növények
Aconitum vulparia	farkasölő sisakvirág	\N	Védett	1554	45748	növények
Acorus calamus	kálmos (orvosi kálmos)	\N	Védett	1554	45749	növények
Adenophora liliifolia	csengettyűvirág	HD II, IV	Fokozottan védett	1554	45750	növények
Adonis vernalis	tavaszi hérics	\N	Védett	1554	45751	növények
Adonis volgensis	volgamenti hérics	\N	Fokozottan védett	1554	45752	növények
Adonis x hybrida	volgamenti hérics	\N	Fokozottan védett	1554	45753	növények
Aethionema saxatile	sulyoktáska (kövi sulyoktáska)	\N	Védett	1554	45754	növények
Agrostemma githago	konkoly (vetési konkoly)	\N	Védett	1554	45755	növények
Ajuga laxmannii	szennyes ínfű	\N	Védett	1554	45757	növények
Alchemilla acutiloba	hegyeskaréjú palástfű	\N	Védett	1554	45758	növények
Alchemilla crinita	csipkéslevelű palástfű	\N	Védett	1554	45759	növények
Alchemilla filicaulis	vékonyszárú palástfű	\N	Védett	1554	45760	növények
Alchemilla glabra	havasi palástfű	\N	Védett	1554	45761	növények
Alchemilla glaucescens	hegyi palástfű	\N	Védett	1554	45762	növények
Alchemilla hungarica	magyar palástfű	\N	Védett	1554	45763	növények
Alchemilla micans	kecses palástfű	\N	Védett	1554	45764	növények
Alchemilla monticola	közönséges palástfű	\N	Védett	1554	45765	növények
Alchemilla sp.	palástfű	\N	Védett	1554	45766	növények
Alchemilla subcrenata	hullámoslevelű palástfű	\N	Védett	1554	45767	növények
Alchemilla xanthochlora	réti palástfű	\N	Védett	1554	45768	növények
Aldrovanda vesiculosa	aldrovanda	HD II, IV	Védett	1554	45769	növények
Alkanna tinctoria	báránypirosító	\N	Védett	1554	45770	növények
Allium carinatum	szarvas hagyma	\N	Védett	1554	45771	növények
Allium marginatum	bugás hagyma (vöröses hagyma)	\N	Védett	1554	45772	növények
Allium moschatum	pézsmahagyma	\N	Védett	1554	45773	növények
Allium paniculatum	bugás hagyma (vöröses hagyma)	\N	Védett	1554	45774	növények
Allium paniculatum ssp. marginatum	bugás hagyma	\N	Védett	1554	45775	növények
Allium sphaerocephalon	bunkós hagyma	\N	Védett	1554	45776	növények
Allium suaveolens	illatos hagyma	\N	Védett	1554	45777	növények
Allium victorialis	győzedelmes hagyma (havasi hagyma)	\N	Védett	1554	45778	növények
Alnus viridis	havasi éger	\N	Védett	1554	45779	növények
Alternanthera philoxeroides	aligátorfű	IAS	Inváziós	1554	45780	növények
Amaranthus powellii	karcsú disznóparéj	\N	Inváziós	1554	45781	növények
Amaranthus retroflexus	szőrös disznóparéj	\N	Inváziós	1554	45782	növények
Amblystegium saxatile	sziklai karcsútokúmoha	\N	Védett	1554	45783	növények
Ambrosia artemisiifolia	ürömlevelű parlagfű	\N	Inváziós	1554	45784	növények
Amelanchier ovalis	közönséges fanyarka	\N	Védett	1554	45785	növények
Amorpha fruticosa	gyalogakác	\N	Inváziós	1554	45786	növények
Amygdalus nana	törpe mandula	\N	Védett	1554	45787	növények
Anacamptis coriophora	poloskaszagú kosbor (poloskaszagú sisakoskosbor)	\N	Védett	1554	45788	növények
Anacamptis palustris ssp. elegans	pompás kosbor (pompás sisakoskosbor)	\N	Védett	1554	45790	növények
Anacamptis palustris ssp. palustris	mocsári kosbor (mocsári sisakoskosbor)	\N	Védett	1554	45791	növények
Anacamptis pyramidalis	vitézvirág (tornyos sisakoskosbor, tornyos vitézvirág)	\N	Védett	1554	45792	növények
Anacamptodon splachnoides	ernyőmohaszerű görbefogúmoha	\N	Védett	1554	45793	növények
Anchusa barrelieri	kék atracél	\N	Védett	1554	45794	növények
Anchusa ochroleuca	vajszínű atracél	\N	Fokozottan védett	1554	45795	növények
Androsace maxima	nagy gombafű	\N	Védett	1554	45796	növények
Anemone sylvestris	erdei szellőrózsa	\N	Védett	1554	45797	növények
Anemone trifolia	hármaslevelű szellőrózsa	\N	Védett	1554	45798	növények
Angelica palustris	réti angyalgyökér	HD II, IV	Fokozottan védett	1554	45799	növények
Anogramma leptophylla	tavaszi kérészpáfrány (télipáfrány)	\N	Védett	1554	45800	növények
Anomodon rostratus	csőrös farkaslábmoha	\N	Védett	1554	45801	növények
Anthericum liliago	fürtös homokliliom	\N	Védett	1554	45802	növények
Anthriscus nitidus	havasi turbolya	\N	Védett	1554	45803	növények
Apamea syriaca	Tallós-dudvabagoly	\N	Védett	1554	45804	növények
Apium repens	kúszó zeller	HD II, IV	Védett	1554	45805	növények
Aquilegia nigricans	feketéllő harangláb	\N	Védett	1554	45806	növények
Aquilegia vulgaris	közönséges harangláb	\N	Védett	1554	45807	növények
Arabis alpina	havasi ikravirág	\N	Védett	1554	45808	növények
Aremonia agrimonioides	bükkös kispárlófű	\N	Védett	1554	45809	növények
Armeria elongata	magas istác	\N	Védett	1554	45810	növények
Armoracia macrocarpa	debreceni torma	\N	Fokozottan védett	1554	45811	növények
Arnica montana	árnika (hegyi árnika)	HD V	Védett	1554	45812	növények
Aruncus dioicus	erdei tündérfürt	\N	Védett	1554	45813	növények
Asclepias syriaca	selyemkóró	IAS	Inváziós	1554	45814	növények
Asperula taurina	olasz müge	\N	Védett	1554	45815	növények
Asphodelus albus	genyőte (fehér genyőte, királyné gyertyája)	\N	Védett	1554	45816	növények
Asplenium adiantum-nigrum	fekete fodorka	\N	Védett	1554	45817	növények
Asplenium ceterach	nyugati pikkelypáfrány	\N	Védett	1554	45818	növények
Asplenium fontanum	forrásfodorka	\N	Védett	1554	45819	növények
Asplenium javorkeanum	magyar pikkelypáfrány	\N	Védett	1554	45820	növények
Asplenium lepidum	mirigyes fodorka	\N	Védett	1554	45821	növények
Asplenium scolopendrium	gímpáfrány (gímnyelvű fodorka)	\N	Védett	1554	45822	növények
Asplenium viride	zöld fodorka	\N	Védett	1554	45823	növények
Aster ×salignus	fűzlevelű őszirózsa	\N	Inváziós	1554	45824	növények
Aster amellus	csillagőszirózsa	\N	Védett	1554	45825	növények
Aster laevis	simalevelű őszirózsa	\N	Inváziós	1554	45826	növények
Aster lanceolatus	lándzsás őszirózsa	\N	Inváziós	1554	45827	növények
Aster novae-angliae	mirigyes őszirózsa	\N	Inváziós	1554	45828	növények
Aster novi-belgii	sötétlila őszirózsa	\N	Inváziós	1554	45829	növények
Aster oleifolius	gyapjas őszirózsa	\N	Fokozottan védett	1554	45830	növények
Aster sedifolius	réti őszirózsa (pettyegetett őszirózsa)	\N	Védett	1554	45831	növények
Aster sedifolius ssp. canus	réti őszirózsa (pettyegetett őszirózsa)	\N	Védett	1554	45832	növények
Aster sedifolius ssp. sedifolius	réti őszirózsa (pettyegetett őszirózsa)	\N	Védett	1554	45833	növények
Aster tradescantii	kisvirágú őszirózsa	\N	Inváziós	1554	45834	növények
Aster x versicolor	tarka őszirózsa	\N	Inváziós	1554	45835	növények
Asterella saccata	zsákos zacskósmoha	\N	Védett	1554	45836	növények
Astragalus asper	érdes csüdfű	\N	Védett	1554	45837	növények
Astragalus contortuplicatus	tekert csüdfű	\N	Védett	1554	45838	növények
Astragalus dasyanthus	gyapjas csüdfű	\N	Fokozottan védett	1554	45839	növények
Astragalus exscapus	szártalan csüdfű	\N	Védett	1554	45840	növények
Astragalus sulcatus	barázdás csüdfű	\N	Védett	1554	45841	növények
Astragalus varius	homoki csüdfű	\N	Védett	1554	45842	növények
Astragalus vesicarius ssp. albidus	fehéres csüdfű	\N	Védett	1554	45843	növények
Astrantia major	nagy völgycsillag	\N	Védett	1554	45844	növények
Asyneuma canescens	harangcsillag	\N	Védett	1554	45845	növények
Aurinia saxatilis	sziklaiternye (szirti sziklaiternye)	\N	Védett	1554	45846	növények
Avena sterilis ssp. ludoviciana	magas zab	\N	Inváziós	1554	45847	növények
Avenula compressa	tömött zabfű	\N	Védett	1554	45848	növények
Azolla filiculoides	nagylevelű moszatpáfrány	\N	Inváziós	1554	45849	növények
Azolla mexicana	mexikói moszatpáfrány	\N	Inváziós	1554	45850	növények
Baccharis halimifolia	borfa,  tengerparti seprűcserje	IAS	Inváziós	1554	45851	növények
Bassia sedoides	hamvas seprőparéj	\N	Védett	1554	45852	növények
Betula pubescens	molyhos nyír (szőrös nyír)	\N	Védett	1554	45853	növények
Bidens frondosa	feketéllő farkasfog	\N	Inváziós	1554	45854	növények
Blackstonia acuminata	kései gyíkpohár	\N	Védett	1554	45855	növények
Blechnum spicant	erdei bordapáfrány	\N	Védett	1554	45856	növények
Blysmus compressus	lapos kétsoroskáka	\N	Védett	1554	45857	növények
Botrychium lunaria	kis holdruta	\N	Védett	1554	45858	növények
Botrychium matricariifolium	ágas holdruta	\N	Fokozottan védett	1554	45859	növények
Botrychium multifidum	sokcimpájú holdruta	\N	Fokozottan védett	1554	45860	növények
Botrychium virginianum	virginiai holdruta	\N	Fokozottan védett	1554	45861	növények
Brachydontium trichodes	szőrszerű pintycsőrfogúmoha	\N	Védett	1554	45862	növények
Brachytecium geheebii	Geheeb-pintycsőrűmoha	\N	Védett	1554	45863	növények
Brachytecium oxycladum	hegyeságú pintycsőrűmoha	\N	Védett	1554	45864	növények
Bryum neodamense	kanalaslevelű körtemoha	\N	Védett	1554	45865	növények
Bryum stirtonii	keskenyfogú körtemoha	\N	Védett	1554	45866	növények
Bryum versicolor	tarka körtemoha	\N	Védett	1554	45867	növények
Bryum warneum	ostoros körtemoha	\N	Védett	1554	45868	növények
Buddleja davidii	nyáriorgona	\N	Inváziós	1554	45869	növények
Bulbocodium vernum	egyhajúvirág	\N	Fokozottan védett	1554	45870	növények
Bulbocodium versicolor	egyhajúvirág	\N	Fokozottan védett	1554	45871	növények
Buphthalmum salicifolium	fűzlevelű ökörszem	\N	Védett	1554	45872	növények
Bupleurum longifolium	hosszúlevelű buvákfű	\N	Védett	1554	45873	növények
Bupleurum pachnospermum	deres buvákfű	\N	Védett	1554	45874	növények
Buxbaumia viridis	zöld koboldmoha	HD II	Védett	1554	45875	növények
Cabomba caroliniana	karolinai tündérhínár	IAS	Inváziós	1554	45876	növények
Calamagrostis phragmitoides	pirosló nádtippan	\N	Védett	1554	45877	növények
Calamagrostis pseudophragmites	parti nádtippan	\N	Védett	1554	45878	növények
Calamagrostis purpurea	pirosló nádtippan	\N	Védett	1554	45879	növények
Calamagrostis stricta	lápi nádtippan	\N	Védett	1554	45880	növények
Calamagrostis varia	tarka nádtippan	\N	Védett	1554	45881	növények
Caldesia parnassifolia	szíveslevelű-hídőr (lápi szíveslevelű-hídőr)	HD II, IV	Fokozottan védett	1554	45882	növények
Calliergon giganteum	óriásmoha	\N	Védett	1554	45883	növények
Calliergon stramineum	gyökerecskés-levélcsúcsú moha	\N	Védett	1554	45884	növények
Campanula latifolia	széleslevelű harangvirág	\N	Fokozottan védett	1554	45885	növények
Campanula macrostachya	hosszúfüzérű harangvirág	\N	Fokozottan védett	1554	45886	növények
Campylium elodes	mocsári aranymoha	\N	Védett	1554	45887	növények
Campylium vernicosus	karcsú pásztorbotmoha	\N	Védett	1554	45888	növények
Campylostelium saxicola	sziklai görbeszárúmoha	\N	Védett	1554	45889	növények
Cardamine amara	keserű kakukktorma	\N	Védett	1554	45890	növények
Cardamine glanduligera	ikrás fogasír	\N	Védett	1554	45891	növények
Cardamine trifolia	hármaslevelű kakukktorma	\N	Védett	1554	45892	növények
Cardamine waldsteinii	hármaslevelű fogasír	\N	Védett	1554	45893	növények
Cardaminopsis petraea	sziklai kövifoszlár	\N	Védett	1554	45894	növények
Carduus collinus	magyar bogáncs	\N	Védett	1554	45895	növények
Carduus crassifolius ssp. glaucus	szürke bogáncs	\N	Védett	1554	45896	növények
Carduus glaucus	szürke bogáncs	\N	Védett	1554	45897	növények
Carduus hamulosus	horgas bogáncs	\N	Védett	1554	45898	növények
Carex alba	fehér sás	\N	Védett	1554	45899	növények
Carex appropinquata	rostostövű sás	\N	Védett	1554	45900	növények
Carex bohemica	palkasás	\N	Védett	1554	45901	növények
Carex brevicollis	mérges sás	\N	Védett	1554	45902	növények
Carex buekii	bánsági sás	\N	Védett	1554	45903	növények
Carex buxbaumii	Buxbaum-sás	\N	Védett	1554	45904	növények
Carex canescens	szürkés sás	\N	Védett	1554	45905	növények
Carex cespitosa	gyepes sás	\N	Védett	1554	45906	növények
Carex davalliana	lápi sás	\N	Védett	1554	45907	növények
Carex depressa ssp. transsilvanica	erdélyi sás	\N	Védett	1554	45908	növények
Carex diandra	hengeres sás	\N	Védett	1554	45909	növények
Carex echinata	töviskés sás	\N	Védett	1554	45910	növények
Carex fritschii	dunántúli sás	\N	Védett	1554	45911	növények
Carex hartmanii	északi sás	\N	Védett	1554	45912	növények
Carex lasiocarpa	gyapjasmagvú sás	\N	Védett	1554	45913	növények
Carex paniculata	bugás sás	\N	Védett	1554	45914	növények
Carex repens	kúszó sás	\N	Védett	1554	45915	növények
Carex rostrata	csőrös sás	\N	Védett	1554	45916	növények
Carex strigosa	borostás sás	\N	Védett	1554	45917	növények
Carex umbrosa	árnyéki sás	\N	Védett	1554	45918	növények
Carlina acaulis	szártalan bábakalács	\N	Védett	1554	45919	növények
Carpesium abrotanoides	fürtös gyűrűvirág	\N	Védett	1554	45920	növények
Carpinus orientalis	keleti gyertyán	\N	Védett	1554	45921	növények
Celtis occidentalis	nyugati ostorfa	\N	Inváziós	1554	45922	növények
Cenchrus incertus	átoktüske	\N	Inváziós	1554	45923	növények
Centaurea arenaria	homoki imola	\N	Védett	1554	45924	növények
Centaurea arenaria ssp. tauscheri	homoki imola alfaj	\N	Védett	1554	45925	növények
Centaurea mollis	szirti imola	\N	Védett	1554	45926	növények
Centaurea montana ssp. mollis	szirti imola	\N	Védett	1554	45927	növények
Centaurea pseudophrygia	paróka imola	\N	Védett	1554	45928	növények
Centaurea sadleriana	budai imola	\N	Védett	1554	45929	növények
Centaurea scabiosa ssp. sadleriana	budai imola	\N	Védett	1554	45930	növények
Centaurea solstitialis	sáfrányos imola	\N	Védett	1554	45931	növények
Centaurea triumfettii	tarka imola	\N	Védett	1554	45932	növények
Centaurea triumfettii ssp. aligera	tarka imola	\N	Védett	1554	45933	növények
Cephalanthera alba	fehér madársisak	\N	Védett	1554	45934	növények
Cephalanthera damasonium	fehér madársisak	\N	Védett	1554	45935	növények
Cephalanthera longifolia	kardos madársisak	\N	Védett	1554	45936	növények
Cephalanthera rubra	piros madársisak	\N	Védett	1554	45937	növények
Cephalanthera sp.	madársisak-fajok	\N	Védett	1554	45938	növények
Cephalozia lacinulata	lapos májmohácska	\N	Védett	1554	45939	növények
Cerastium arvense ssp. matrense	mátrai madárhúr	\N	Védett	1554	45940	növények
Cerastium arvense ssp. molle	mátrai madárhúr	\N	Védett	1554	45941	növények
Ceratophyllum tanaiticum	donvidéki tócsagaz	\N	Védett	1554	45942	növények
Ceterach javorkeanum	magyar pikkelypáfrány	\N	Védett	1554	45943	növények
Cetraria aculeata  	tüskés vértecs	\N	Védett	1554	45944	növények
Chaerophyllum aureum	aranyos baraboly	\N	Védett	1554	45945	növények
Chaerophyllum hirsutum	szőrös baraboly	\N	Védett	1554	45946	növények
Chamaecytisus albus	fehér törpezanót	\N	Védett	1554	45947	növények
Chamaecytisus ciliatus	pillás törpezanót	\N	Védett	1554	45948	növények
Chamaecytisus heuffelii	Heuffel-törpezanót	\N	Védett	1554	45949	növények
Chamaecytisus hirsutus ssp. ciliatus	pillás törpezanót	\N	Védett	1554	45950	növények
Chamaecytisus rochelii	Rochel-törpezanót	\N	Védett	1554	45951	növények
Chamaenerion dodonaei	vízparti deréce	\N	Védett	1554	45952	növények
Chimaphila umbellata	csinos ernyőskörtike	\N	Védett	1554	45953	növények
Chlorocrepis staticifolia	keskenylevelű hölgymál	\N	Védett	1554	45954	növények
Cicuta virosa	gyilkos csomorika	\N	Védett	1554	45955	növények
Cimicifuga europaea	büdös poloskavész	\N	Fokozottan védett	1554	45956	növények
Circaea alpina	havasi varázslófű	\N	Védett	1554	45957	növények
Cirsium boujartii	pécsvidéki aszat	\N	Védett	1554	45958	növények
Cirsium brachycephalum	kisfészkű aszat	HD II, IV	Védett	1554	45959	növények
Cirsium erisithales	enyves aszat	\N	Védett	1554	45960	növények
Cirsium furiens	öldöklő aszat	\N	Védett	1554	45961	növények
Cirsium rivulare	csermelyaszat	\N	Védett	1554	45962	növények
Cladonia spp. (subgenus Cladinia)	tölcsérzuzmó fajok	HD V	Nem védett	1554	45963	növények
Clematis alpina	havasi iszalag	\N	Védett	1554	45964	növények
Clematis integrifolia	réti iszalag	\N	Védett	1554	45965	növények
Cnidium dubium	inas gyíkvirág	\N	Védett	1554	45966	növények
Coeloglossum viride	zöldike (zöldike ujjaskosbor)	\N	Védett	1554	45967	növények
Colchicum arenarium	homoki kikerics	HD II, IV	Fokozottan védett	1554	45968	növények
Colchicum hungaricum	magyar kikerics	\N	Fokozottan védett	1554	45969	növények
Comarum palustre	tőzegeper (tőzegpimpó)	\N	Fokozottan védett	1554	45970	növények
Conringia austriaca	keleti nyilasfű	\N	Védett	1554	45971	növények
Convolvulus cantabrica	borzas szulák	\N	Védett	1554	45972	növények
Conyza canadensis	betyárkóró	\N	Inváziós	1554	45973	növények
Corallorhiza trifida	erdei korallgyökér	\N	Védett	1554	45974	növények
Corispermum canescens	szürke poloskamag	\N	Védett	1554	45975	növények
Corispermum nitidum	fényes poloskamag	\N	Védett	1554	45976	növények
Coronilla coronata	sárga koronafürt	\N	Védett	1554	45977	növények
Coronilla vaginalis	terpedt koronafürt	\N	Védett	1554	45978	növények
Corydalis intermedia	bókoló keltike	\N	Védett	1554	45979	növények
Cotoneaster integerrimus	szirti madárbirs (piros madárbirs)	\N	Védett	1554	45980	növények
Cotoneaster matrensis	fekete madárbirs (incl. pannon madárbirs)	\N	Védett	1554	45981	növények
Cotoneaster niger	fekete madárbirs (incl. pannon madárbirs)	\N	Védett	1554	45982	növények
Cotoneaster sp.	madárbirs-fajok	\N	Védett	1554	45983	növények
Cotoneaster tomentosus	molyhos madárbirs (nagylevelű madárbirs)	\N	Védett	1554	45984	növények
Crambe tataria	tátorján (buglyos tátorján)	HD II, IV	Fokozottan védett	1554	45985	növények
Crataegus nigra	fekete galagonya	\N	Fokozottan védett	1554	45986	növények
Crataegus x degenii	fekete galagonya hibrid	\N	Fokozottan védett	1554	45987	növények
Crepis nicaeensis	nizzai zörgőfű	\N	Védett	1554	45988	növények
Crepis pannonica	magyar zörgőfű	\N	Fokozottan védett	1554	45989	növények
Crocus albiflorus	fehér sáfrány	\N	Védett	1554	45990	növények
Crocus heuffelianus	kárpáti sáfrány	\N	Védett	1554	45991	növények
Crocus reticulatus	tarka sáfrány	\N	Védett	1554	45992	növények
Crocus tommasinianus	illír sáfrány	\N	Védett	1554	45993	növények
Crocus vittatus	halvány sáfrány	\N	Védett	1554	45994	növények
Cuscuta campestris	nagy aranka	\N	Inváziós	1554	45995	növények
Cyclamen purpurascens	erdei ciklámen	\N	Védett	1554	45996	növények
Cyperus esculentus var. leptostachyus	mandulapalka	\N	Inváziós	1554	45997	növények
Cypripedium calceolus	rigópohár (erdei papucskosbor, Boldogasszony papucsa)	HD II, IV	Fokozottan védett	1554	45998	növények
Dactylorhiza fuchsii	erdei ujjaskosbor	\N	Védett	1554	45999	növények
Dactylorhiza incarnata	hússzínű ujjaskosbor	\N	Védett	1554	46000	növények
Dactylorhiza incarnata ssp. haematodes	hússzínű ujjaskosbor	\N	Védett	1554	46001	növények
Dactylorhiza incarnata ssp. incarnata	hússzínű ujjaskosbor	\N	Védett	1554	46002	növények
Dactylorhiza incarnata ssp. ochroleuca	halvány ujjaskosbor	\N	Fokozottan védett	1554	46003	növények
Dactylorhiza incarnata ssp. serotina	hússzínű ujjaskosbor	\N	Védett	1554	46004	növények
Dactylorhiza lapponica	lappföldi ujjaskosbor	\N	Védett	1554	46005	növények
Dactylorhiza maculata	foltos ujjaskosbor	\N	Védett	1554	46006	növények
Dactylorhiza maculata ssp. transylvanica	foltos ujjaskosbor	\N	Védett	1554	46007	növények
Dactylorhiza majalis	széleslevelű ujjaskosbor	\N	Védett	1554	46008	növények
Dactylorhiza ochroleuca	halvány ujjaskosbor	\N	Fokozottan védett	1554	46009	növények
Dactylorhiza sambucina	bodzaszagú ujjaskosbor	\N	Védett	1554	46010	növények
Dactylorhiza viridis	zöldike (zöldike ujjaskosbor)	\N	Védett	1554	46011	növények
Daphne cneorum	henye boroszlán	\N	Védett	1554	46012	növények
Daphne laureola	babérboroszlán	\N	Védett	1554	46013	növények
Daphne mezereum	farkasboroszlán	\N	Védett	1554	46014	növények
Daphne sp.	boroszlán	\N	Védett	1554	46015	növények
Desmatodon cernuus	bókoló trágyamoha	\N	Védett	1554	46016	növények
Dianthus arenarius	balti szegfű	\N	Védett	1554	46017	növények
Dianthus arenarius ssp. borussicus	balti szegfű	\N	Védett	1554	46018	növények
Dianthus collinus	dunai szegfű	\N	Védett	1554	46019	növények
Dianthus deltoides	réti szegfű	\N	Védett	1554	46020	növények
Dianthus diutinus	tartós szegfű	HD II kiemelt, IV	Fokozottan védett	1554	46021	növények
Dianthus giganteiformis	nagy szegfű	\N	Védett	1554	46022	növények
Dianthus giganteiformis ssp. pontederae	nagy szegfű	\N	Védett	1554	46023	növények
Dianthus plumarius ssp. lumnitzeri	Lumnitzer-szegfű (incl. István király-szegfű)	HD II kiemelt, IV	Fokozottan védett	1554	46024	növények
Dianthus plumarius ssp. praecox	korai szegfű	\N	Fokozottan védett	1554	46025	növények
Dianthus serotinus	kései szegfű	\N	Védett	1554	46027	növények
Dianthus superbus	buglyos szegfű	\N	Védett	1554	46028	növények
Dicranella humilis	kis seprőcskemoha	\N	Védett	1554	46029	növények
Dicranum viride	zöld seprőmoha	HD II	Védett	1554	46030	növények
Dictamnus albus	kőrislevelű nagyezerjófű	\N	Védett	1554	46031	növények
Didymodon glaucus	szürke ikresmoha	\N	Védett	1554	46032	növények
Digitalis ferruginea	rozsdás gyűszűvirág	\N	Fokozottan védett	1554	46033	növények
Digitalis lanata	gyapjas gyűszűvirág	\N	Fokozottan védett	1554	46034	növények
Diphasiastrum complanatum	közönséges laposkorpafű	HD V	Védett	1554	46035	növények
Diphasiastrum issleri	Issler-laposkorpafű	HD V	Védett	1554	46036	növények
Diphasium complanatum	közönséges laposkorpafű	HD V	Védett	1554	46037	növények
Diphasium issleri	Issler-laposkorpafű	HD V	Védett	1554	46038	növények
Doronicum austriacum	osztrák zergevirág	\N	Védett	1554	46039	növények
Doronicum hungaricum	magyar zergevirág	\N	Védett	1554	46040	növények
Doronicum orientale	keleti zergevirág	\N	Védett	1554	46041	növények
Draba lasiocarpa	kövér daravirág	\N	Védett	1554	46042	növények
Dracocephalum austriacum	osztrák sárkányfű	HD II, IV	Fokozottan védett	1554	46043	növények
Dracocephalum ruyschiana	északi sárkányfű	\N	Fokozottan védett	1554	46044	növények
Drepanocladus cossonii	körkörös sarlósmoha	\N	Védett	1554	46045	növények
Drepanocladus exannulatus	gyűrűtlen sarlósmoha	\N	Védett	1554	46046	növények
Drepanocladus lycopodioides	korpafűszerű sarlósmoha	\N	Védett	1554	46047	növények
Drepanocladus revolvens	körkörös sarlósmoha	\N	Védett	1554	46048	növények
Drepanocladus sendtneri	Sendtner-sarlósmoha	\N	Védett	1554	46049	növények
Drosera rotundifolia	kereklevelű harmatfű	\N	Védett	1554	46050	növények
Dryopteris affinis	pelyvás pajzsika	\N	Védett	1554	46051	növények
Dryopteris carthusiana	szálkás pajzsika	\N	Védett	1554	46052	növények
Dryopteris cristata	tarajos pajzsika	\N	Fokozottan védett	1554	46053	növények
Dryopteris dilatata	széles pajzsika	\N	Védett	1554	46054	növények
Dryopteris expansa	hegyi pajzsika	\N	Védett	1554	46055	növények
Echinocystis lobata	süntök	\N	Inváziós	1554	46056	növények
Echinops ruthenicus	kék szamárkenyér	\N	Védett	1554	46057	növények
Echium maculatum	piros kígyószisz	HD II, IV	Védett	1554	46058	növények
Echium russicum	piros kígyószisz	HD II, IV	Védett	1554	46059	növények
Eichhornia crassipes	vízijácint	IAS	Inváziós	1554	46060	növények
Elaeagnus angustifolia	keskenylevelű ezüstfa	\N	Inváziós	1554	46061	növények
Elatine alsinastrum	pocsolyalátonya	\N	Védett	1554	46062	növények
Elatine hungarica	magyar látonya	\N	Védett	1554	46063	növények
Elatine hydropiper	csigásmagvú látonya	\N	Védett	1554	46064	növények
Elatine triandra	háromporzós látonya	\N	Védett	1554	46065	növények
Eleocharis carniolica	sűrű csetkáka	HD II, IV	Védett	1554	46066	növények
Eleocharis quinqueflora	gyérvirágú csetkáka	\N	Védett	1554	46067	növények
Eleocharis uniglumis	egypelyvás csetkáka	\N	Védett	1554	46068	növények
Eleusine indica	aszályfű	\N	Inváziós	1554	46069	növények
Elodea canadensis	kanadai átokhínár	\N	Inváziós	1554	46070	növények
Elodea nuttallii	aprólevelű átokhínár	IAS	Inváziós	1554	46071	növények
Elymus elongatus	magas tarackbúza	\N	Védett	1554	46072	növények
Enthostodon hungaricus	sóspusztai magyarmoha	\N	Védett	1554	46073	növények
Ephedra distachya	csikófark	\N	Fokozottan védett	1554	46074	növények
Ephemerum cohaerens	csoportos paránymoha	\N	Védett	1554	46075	növények
Ephemerum recurvifolium	sarlóslevelű paránymoha	\N	Védett	1554	46076	növények
Epilobium dodonaei	vízparti deréce	\N	Védett	1554	46077	növények
Epilobium palustre	mocsári füzike	\N	Védett	1554	46078	növények
Epipactis albensis	elbai nőszőfű	\N	Védett	1554	46079	növények
Epipactis atrorubens	vörösbarna nőszőfű (incl. Borbás nőszőfű)	\N	Védett	1554	46080	növények
Epipactis atrorubens ssp. borbasii	vörösbarna nőszőfű (incl. Borbás nőszőfű)	\N	Védett	1554	46081	növények
Epipactis bugacensis	bugaci nőszőfű	\N	Fokozottan védett	1554	46082	növények
Epipactis exilis	karcsú nőszőfű	\N	Fokozottan védett	1554	46083	növények
Epipactis futakii	Futák-nőszőfű	\N	Védett	1554	46084	növények
Epipactis gracilis	karcsú nőszőfű	\N	Fokozottan védett	1554	46085	növények
Epipactis helleborine	széleslevelű nőszőfű	\N	Védett	1554	46086	növények
Epipactis latina	lazioi nőszőfű	\N	Védett	1554	46087	növények
Epipactis leptochila	csőrös nőszőfű	\N	Védett	1554	46088	növények
Epipactis mecsekensis	mecseki nőszőfű	\N	Védett	1554	46089	növények
Epipactis microphylla	kislevelű nőszőfű	\N	Védett	1554	46090	növények
Epipactis moravica	morva nőszőfű	\N	Védett	1554	46091	növények
Epipactis muelleri	Müller-nőszőfű	\N	Védett	1554	46092	növények
Epipactis neglecta	keskenyajkú nőszőfű	\N	Védett	1554	46093	növények
Epipactis nordeniorum	Norden-nőszőfű	\N	Védett	1554	46094	növények
Epipactis palustris	mocsári nőszőfű	\N	Védett	1554	46095	növények
Epipactis peitzii	Peitz-nőszőfű	\N	Védett	1554	46096	növények
Epipactis placentina	ciklámenlila nőszőfű	\N	Fokozottan védett	1554	46097	növények
Epipactis pontica	pontuszi nőszőfű	\N	Védett	1554	46098	növények
Epipactis pseudopurpurata	nőszőfű-faj	\N	Védett	1554	46099	növények
Epipactis purpurata	ibolyás nőszőfű	\N	Védett	1554	46100	növények
Epipactis sp.	nőszőfű-fajok	\N	Védett	1554	46101	növények
Epipactis tallosii	Tallós-nőszőfű	\N	Védett	1554	46102	növények
Epipactis voethii	Vöth-nőszőfű	\N	Védett	1554	46103	növények
Epipogium aphyllum	levéltelen bajuszvirág	\N	Fokozottan védett	1554	46104	növények
Equisetum hyemale	téli zsurló	\N	Védett	1554	46105	növények
Equisetum sylvaticum	erdei zsurló	\N	Védett	1554	46106	növények
Equisetum variegatum	tarka zsurló	\N	Védett	1554	46107	növények
Eranthis hyemalis	téltemető (kikeletnyitó téltemető)	\N	Védett	1554	46108	növények
Erechtites hieraciifolia	amerikai keresztlapu	\N	Inváziós	1554	46109	növények
Erigeron annuus	egynyári seprence	\N	Inváziós	1554	46110	növények
Eriophorum angustifolium	keskenylevelű gyapjúsás	\N	Védett	1554	46111	növények
Eriophorum gracile	vékony gyapjúsás	\N	Védett	1554	46112	növények
Eriophorum latifolium	széleslevelű gyapjúsás	\N	Védett	1554	46113	növények
Eriophorum vaginatum	hüvelyes gyapjúsás	\N	Védett	1554	46114	növények
Erysimum crepidifolium	sziklai repcsény	\N	Védett	1554	46115	növények
Erysimum odoratum	magyar repcsény	\N	Védett	1554	46116	növények
Erysimum pallidiflorum	halványsárga repcsény	\N	Fokozottan védett	1554	46117	növények
Erysimum witmannii ssp. pallidiflorum	halványsárga repcsény	\N	Fokozottan védett	1554	46118	növények
Erysimum wittmannii	halványsárga repcsény	\N	Fokozottan védett	1554	46119	növények
Erythronium dens-canis	kakasmandikó (európai kakasmandikó)	\N	Védett	1554	46120	növények
Fallopia japonica	ártéri japánkeserűfű	\N	Inváziós	1554	46122	növények
Fallopia sachalinensis	óriás japánkeserűfű	\N	Inváziós	1554	46123	növények
Fallopia sp.	japánkeserűfű	\N	Inváziós	1554	46124	növények
Ferula sadleriana	magyarföldi husáng	HD II kiemelt, IV	Fokozottan védett	1554	46125	növények
Festuca amethystina	lila csenkesz	\N	Védett	1554	46126	növények
Festuca dalmatica	dalmát csenkesz	\N	Védett	1554	46127	növények
Festuca pallens	deres csenkesz	\N	Védett	1554	46128	növények
Festuca pallens ssp. pannonica	deres csenkesz	\N	Védett	1554	46129	növények
Festuca wagneri	rákosi csenkesz	\N	Védett	1554	46130	növények
Fissidens algarvicus	Algarvei hasadtfogúmoha	\N	Védett	1554	46131	növények
Fissidens arnoldii	Arnoldi-hasadtfogúmoha	\N	Védett	1554	46132	növények
Fissidens exiguus	kis hasadtfogúmoha	\N	Védett	1554	46133	növények
Fraxinus pennsylvanica	amerikai kőris	\N	Inváziós	1554	46134	növények
Fritillaria meleagris	mocsári kockásliliom (kotuliliom)	\N	Védett	1554	46135	növények
Frullania inflata	hólyagos kerekesféreglakta-májmoha	\N	Védett	1554	46136	növények
Gagea bohemica	cseh tyúktaréj	\N	Védett	1554	46137	növények
Gagea spathacea	fiókás tyúktaréj	\N	Védett	1554	46138	növények
Gagea szovitsii	pusztai tyúktaréj	\N	Védett	1554	46139	növények
Galanthus nivalis	hóvirág	HD V	7_melleklet	1554	46140	növények
Galinsoga parviflora	kicsiny gombvirág	\N	Inváziós	1554	46141	növények
Galium austriacum	osztrák galaj	\N	Védett	1554	46142	növények
Galium tenuissimum	vékony galaj	\N	Védett	1554	46143	növények
Gentiana asclepiadea	fecsketárnics	\N	Védett	1554	46144	növények
Gentiana cruciata	Szent László-tárnics	\N	Védett	1554	46145	növények
Gentiana pneumonanthe	kornistárnics	\N	Védett	1554	46146	növények
Gentianella amarella	csinos tárnicska	\N	Védett	1554	46147	növények
Gentianella austriaca	osztrák tárnicska (hegyi tárnicska)	\N	Védett	1554	46148	növények
Gentianopsis ciliata	kései prémestárnics	\N	Védett	1554	46149	növények
Geranium sylvaticum	erdei gólyaorr	\N	Védett	1554	46150	növények
Geum aleppicum	hegyi gyömbérgyökér	\N	Védett	1554	46151	növények
Geum rivale	patakparti gyömbérgyökér (bókoló gyömbérgyökér)	\N	Védett	1554	46152	növények
Gladiolus imbricatus	réti kardvirág	\N	Védett	1554	46153	növények
Gladiolus palustris	mocsári kardvirág	HD II, IV	Fokozottan védett	1554	46154	növények
Glaux maritima	tengerparti bagolyfű	\N	Védett	1554	46156	növények
Globularia cordifolia	szívlevelű gubóvirág	\N	Védett	1554	46157	növények
Goodyera repens	avarvirág (kúszó avarvirág)	\N	Védett	1554	46158	növények
Grimmia plagiopodia	hasastokú őszmoha	\N	Védett	1554	46159	növények
Grimmia teretinervis	vastagerű őszmoha	\N	Védett	1554	46160	növények
Groenlandia densa	sűrűlevelű-békaszőlő	\N	Védett	1554	46161	növények
Gunnera manicata	brazil óriáslapu	\N	Inváziós	1554	46162	növények
Gunnera tinctoria	óriás rebarbara	IAS	Inváziós	1554	46163	növények
Gymnadenia conopsea	szúnyoglábú bibircsvirág	\N	Védett	1554	46164	növények
Gymnadenia densiflora	sűrűvirágú bibircsvirág	\N	Védett	1554	46165	növények
Gymnadenia odoratissima	illatos bibircsvirág	\N	Védett	1554	46166	növények
Gymnocarpium dryopteris	közönséges tölgyespáfrány (közönséges hármasleveű-páfrány)	\N	Védett	1554	46167	növények
Gymnocarpium robertianum	mirigyes tölgyespáfrány (mirigyes hármaslevelűpáfrány)	\N	Védett	1554	46168	növények
Gymnocarpium sp.	tölgyespáfrány-fajok	\N	Védett	1554	46169	növények
Gypsophila arenaria	homoki fátyolvirág	\N	Védett	1554	46170	növények
Gypsophila fastigiata	homoki fátyolvirág	\N	Védett	1554	46171	növények
Gypsophila fastigiata ssp. arenaria	homoki fátyolvirág alfaj	\N	Védett	1554	46172	növények
Hamatocaulis vernicosus	karcsú pásztorbotmoha	\N	Védett	1554	46173	növények
Hammarbya paludosa	tőzegorchidea (fiókás tőzegorchidea)	\N	Fokozottan védett	1554	46174	növények
Helianthus ×laetiflorus	kései napraforgó	\N	Inváziós	1554	46175	növények
Helianthus decapetalus	sokvirágú napraforgó	\N	Inváziós	1554	46176	növények
Helianthus pauciflorus	merevlevelű napraforgó	\N	Inváziós	1554	46177	növények
Helianthus sp.	napraforgó	\N	Inváziós	1554	46178	növények
Helianthus tuberosus	csicsóka	\N	Inváziós	1554	46179	növények
Helichrysum arenarium	homoki szalmagyopár	\N	Védett	1554	46180	növények
Helictotrichon compressum	tömött zabfű	\N	Védett	1554	46181	növények
Heliotropium supinum	henye kunkor	\N	Védett	1554	46182	növények
Helleborus dumetorum	kisvirágú hunyor	\N	Védett	1554	46183	növények
Helleborus odorus	illatos hunyor	\N	Védett	1554	46184	növények
Helleborus purpurascens	pirosló hunyor	\N	Védett	1554	46185	növények
Hemerocallis lilio-asphodelus	sárga sásliliom	\N	Fokozottan védett	1554	46186	növények
Hepatica nobilis	nemes májvirág	\N	Védett	1554	46187	növények
Heracleum mantegazzianum	kaukázusi medvetalp	IAS	Inváziós	1554	46188	növények
Heracleum persicum	perzsa medvetalp	IAS	Inváziós	1554	46189	növények
Heracleum sosnowskyi	sosnowsky-medvetalp	IAS	Inváziós	1554	46190	növények
Herniaria incana	szürke porcika	\N	Védett	1554	46191	növények
Hesperis matronalis	hölgyestike (kiv. Vrabély-estike)	\N	Védett	1554	46192	növények
Hesperis matronalis ssp. vrabelyiana	Vrabély-estike	\N	Fokozottan védett	1554	46193	növények
Hesperis sylvestris	erdei estike	\N	Védett	1554	46194	növények
Hesperis vrabelyiana	Vrabély-estike	\N	Fokozottan védett	1554	46195	növények
Hieracium aurantiacum	rezes hölgymál	\N	Védett	1554	46196	növények
Hieracium bupleuroides	tátrai hölgymál	\N	Védett	1554	46197	növények
Hieracium kossuthianum	Kossuth-hölgymál	\N	Védett	1554	46198	növények
Hieracium staticifolium	keskenylevelű hölgymál	\N	Védett	1554	46199	növények
Hilpertia velenovskyi	Velenovsky-löszmoha	\N	Védett	1554	46200	növények
Himantoglossum adriaticum	adriai sallangvirág	HD II, IV	Fokozottan védett	1554	46201	növények
Himantoglossum caprinum	bíboros/Janka sallangvirág	HD II, IV	Fokozottan védett	1554	46202	növények
Himantoglossum jankae	bíboros/Janka sallangvirág	HD II, IV	Fokozottan védett	1554	46203	növények
Hippocrepis emerus	bokros koronafürt	\N	Védett	1554	46204	növények
Hippophae rhamnoides	homoktövis (európai homoktövis)	\N	Védett	1554	46205	növények
Hippophaë rhamnoides	homoktövis (európai homoktövis)	\N	Védett	1554	46206	növények
Hippuris vulgaris	közönséges vízilófark	\N	Védett	1554	46207	növények
Hottonia palustris	mocsári békaliliom	\N	Védett	1554	46208	növények
Humulus japonicus	japán komló	\N	Inváziós	1554	46209	növények
Huperzia selago	györgyfű (részegkorpafű)	\N	Védett	1554	46211	növények
Hydrocotyle ranunculoides	hévízi gázló	IAS	Inváziós	1554	46212	növények
Hydrocotyle vulgaris	gázló (lápi gázló)	\N	Védett	1554	46213	növények
Hypericum barbatum	szakállas orbáncfű	\N	Fokozottan védett	1554	46214	növények
Hypericum elegans	karcsú orbáncfű	\N	Védett	1554	46215	növények
Hypericum maculatum	pettyes orbáncfű	\N	Védett	1554	46216	növények
Impatiens glandulifera	bíbor nebáncsvirág	IAS	Inváziós	1554	46217	növények
Impatiens parviflora	kisvirágú nebáncsvirág	\N	Inváziós	1554	46218	növények
Inula germanica	hengeresfészkű peremizs	\N	Védett	1554	46219	növények
Inula helenium	örménygyökér	\N	Védett	1554	46220	növények
Inula oculus-christi	selymes peremizs	\N	Védett	1554	46221	növények
Inula spiraeifolia	baranyai peremizs	\N	Védett	1554	46222	növények
Iris aphylla ssp. hungarica	magyar nőszirom	HD II, IV	Fokozottan védett	1554	46223	növények
Iris arenaria	homoki nőszirom	HD II, IV	Védett	1554	46224	növények
Iris graminea	pázsitos nőszirom	\N	Védett	1554	46225	növények
Iris humilis ssp. arenaria	homoki nőszirom	HD II, IV	Védett	1554	46226	növények
Iris pumila	apró nőszirom	\N	Védett	1554	46227	növények
Iris sibirica	szibériai nőszirom	\N	Védett	1554	46228	növények
Iris spuria	fátyolos nőszirom (korcs nőszirom)	\N	Védett	1554	46229	növények
Iris variegata	tarka nőszirom	\N	Védett	1554	46230	növények
Isatis tinctoria	festő csülleng	\N	Védett	1554	46231	növények
Iva xanthiifolia	parlagi rézgyom	\N	Inváziós	1554	46232	növények
Jovibarba globifera	sárga-kövirózsa	\N	Védett	1554	46233	növények
Jovibarba globifera ssp. globifera	sárga-kövirózsa	\N	Védett	1554	46234	növények
Jovibarba globifera ssp. hirta	sárga-kövirózsa	\N	Védett	1554	46235	növények
Jovibarba hirta	sárga-kövirózsa	\N	Védett	1554	46236	növények
Jovibarba sp.	kövirózsa-fajok	\N	Védett	1554	46237	növények
Juncus alpinoarticulatus	havasi szittyó	\N	Védett	1554	46238	növények
Juncus maritimus	tengeri szittyó	\N	Védett	1554	46239	növények
Juncus tenuis	vékony szittyó	\N	Inváziós	1554	46240	növények
Jungermannia subulata	Jungermann-moha	\N	Védett	1554	46241	növények
Jurinea glycacantha	nagyfészkű hangyabogáncs	\N	Védett	1554	46242	növények
Jurinea mollis	kisfészkű hangyabogáncs	\N	Védett	1554	46243	növények
Jurinea sp.	hangyabogáncs-fajok	\N	Védett	1554	46244	növények
Knautia arvensis ssp. kitaibelii	Kitaibel-varfű	\N	Fokozottan védett	1554	46245	növények
Knautia dipsacifolia	erdei varfű	\N	Védett	1554	46246	növények
Knautia kitaibelii ssp. tomentella	Kitaibel-varfű	\N	Fokozottan védett	1554	46247	növények
Knautia maxima	erdei varfű	\N	Védett	1554	46248	növények
Koeleria javorkae	Jávorka-fényperje	\N	Védett	1554	46249	növények
Koeleria majoriflora	nagyvirágú fényperje	\N	Védett	1554	46250	növények
Koeleria pyramidata	nyugati fényperje (magas fényperje)	\N	Védett	1554	46251	növények
Krascheninnikovia ceratoides	pamacslaboda	\N	Védett	1554	46252	növények
Lagarosiphon major	fodros átokhínár	IAS	Inváziós	1554	46253	növények
Lamium orvala	pofók árvacsalán	\N	Védett	1554	46254	növények
Lathyrus lacteus	koloncos lednek	\N	Védett	1554	46255	növények
Lathyrus linifolius	hegyi lednek	\N	Védett	1554	46256	növények
Lathyrus nissolia	kacstalan lednek	\N	Védett	1554	46257	növények
Lathyrus pallescens	sápadt lednek	\N	Fokozottan védett	1554	46258	növények
Lathyrus palustris	mocsári lednek	\N	Védett	1554	46259	növények
Lathyrus pannonicus	magyar lednek	\N	Védett	1554	46260	növények
Lathyrus pannonicus ssp. collinus	koloncos lednek	\N	Védett	1554	46261	növények
Lathyrus pisiformis	borsóképű lednek	\N	Fokozottan védett	1554	46262	növények
Lathyrus transsylvanicus	erdélyi lednek	\N	Védett	1554	46263	növények
Lathyrus venetus	tarka lednek	\N	Védett	1554	46264	növények
Lemna minuta	szemcsés békalencse	\N	Inváziós	1554	46265	növények
Leontodon incanus	szőke oroszlánfog	\N	Védett	1554	46266	növények
Leucanthemella serotina	tiszaparti késeimargitvirág (tisza-parti margitvirág)	\N	Védett	1554	46267	növények
Leucobryum glaucum	fehérlő vánkosmoha	HD V	Védett	1554	46268	növények
Leucojum aestivum	nyári tőzike	\N	Védett	1554	46269	növények
Leucojum vernum	tavaszi tőzike	\N	Védett	1554	46270	növények
Ligularia sibirica	szibériai hamuvirág	\N	Védett	1554	46271	növények
Lilium bulbiferum	tüzes liliom	\N	Fokozottan védett	1554	46272	növények
Lilium bulbiferum ssp. bulbiferum	tüzes liliom	\N	Fokozottan védett	1554	46273	növények
Lilium martagon	turbánliliom	\N	Védett	1554	46274	növények
Limodorum abortivum	gérbics (ibolyás gérbics)	\N	Védett	1554	46275	növények
Linaria biebersteinii	Bieberstein-gyújtoványfű	\N	Védett	1554	46276	növények
Lindernia procumbens	heverő iszapfű	HD IV	Védett	1554	46277	növények
Linum dolomiticum	pilisi len (dolomitlen)	HD II kiemelt, IV	Fokozottan védett	1554	46278	növények
Linum flavum	sárga len	\N	Védett	1554	46279	növények
Linum hirsutum	borzas len	\N	Védett	1554	46280	növények
Linum hirsutum ssp. glabrescens	borzas len	\N	Védett	1554	46281	növények
Linum hirsutum ssp. hirsutum	borzas len	\N	Védett	1554	46282	növények
Linum tenuifolium	árlevelű len	\N	Védett	1554	46283	növények
Linum trigynum	francia len	\N	Védett	1554	46284	növények
Liparis loeselii	hagymaburok (lápi hagymaburok)	HD II, IV	Fokozottan védett	1554	46285	növények
Listera ovata	békakonty	\N	Védett	1554	46286	növények
Lonicera caprifolium	jerikói lonc	\N	Védett	1554	46287	növények
Lonicera nigra	fekete lonc	\N	Védett	1554	46288	növények
Lophozia ascendens	felálló hegyesmájmoha	\N	Védett	1554	46289	növények
Lotus borbasii	Borbás-kerep	\N	Védett	1554	46290	növények
Ludwigia grandiflora	nagyvirágú tóalma	IAS	Inváziós	1554	46291	növények
Ludwigia palustris	tóalma (közönséges tóalma)	\N	Védett	1554	46292	növények
Ludwigia peploides	sárgavirágú tóalma	IAS	Inváziós	1554	46293	növények
Lunaria annua	kerti holdviola	\N	Védett	1554	46294	növények
Lunaria rediviva	erdei holdviola	\N	Védett	1554	46295	növények
Lupinus polyphyllus	erdei csillagfürt	\N	Inváziós	1554	46296	növények
Lychnis coronaria	bársonyos kakukkszegfű	\N	Védett	1554	46297	növények
Lycopodium annotinum	kígyózó korpafű	HD V	Védett	1554	46298	növények
Lycopodium clavatum	kapcsos korpafű	HD V	Védett	1554	46299	növények
Lycopodium complanatum	közönséges laposkorpafű	HD V	Védett	1554	46300	növények
Lycopodium issleri	Issler-laposkorpafű	HD V	Védett	1554	46301	növények
Lycopodium spp.	korpafű	HD V	Védett	1554	46302	növények
Lysichiton americanus	sárga lápbuzogány	IAS	Inváziós	1554	46303	növények
Lysimachia nemorum	berki lizinka	\N	Védett	1554	46304	növények
Lythrum linifolium	lenlevelű füzény	\N	Védett	1554	46305	növények
Lythrum tribracteatum	apró füzény	\N	Védett	1554	46306	növények
Marsilea quadrifolia	mételyfű (négylevelű mételyfű)	HD II, IV	Védett	1554	46308	növények
Matteuccia struthiopteris	struccpáfrárny (európai struccpáfrárny)	\N	Védett	1554	46309	növények
Medicago orbicularis	korongos lucerna	\N	Védett	1554	46310	növények
Medicago rigidula	keménytövisű lucerna	\N	Védett	1554	46311	növények
Meesia triquetra	háromélű moha	\N	Védett	1554	46312	növények
Melampyrum bihariense	erdélyi csormolya	\N	Védett	1554	46313	növények
Menyanthes trifoliata	hármaslevelű vidrafű	\N	Védett	1554	46314	növények
Micromeria thymifolia	illír szirtiperesztény	\N	Fokozottan védett	1554	46315	növények
Microstegium vimineum	japán gázlófű	IAS	Inváziós	1554	46316	növények
Minuartia frutescens	magyar kőhúr	\N	Védett	1554	46317	növények
Minuartia hirsuta	magyar kőhúr	\N	Védett	1554	46318	növények
Moehringia muscosa	mohos csitri	\N	Védett	1554	46319	növények
Moneses uniflora	egyvirágú kiskörtike	\N	Védett	1554	46320	növények
Monochoria korsakowii	kék rizsjácint	\N	Inváziós	1554	46321	növények
Montia fontana ssp. chondrosperma	kis forrásfű	\N	Védett	1554	46322	növények
Muscari botryoides	epergyöngyike	\N	Védett	1554	46323	növények
Myosotis caespitosa	gyepes nefelejcs	\N	Védett	1554	46324	növények
Myosotis laxa ssp. caespitosa	gyepes nefelejcs	\N	Védett	1554	46325	növények
Myosotis stenophylla	sziklai nefelejcs	\N	Védett	1554	46326	növények
Myricaria germanica	német csermelyciprus	\N	Védett	1554	46327	növények
Myriophyllum aquaticum	közönséges süllőhínár	IAS	Inváziós	1554	46328	növények
Myriophyllum heterophyllum	felemáslevelű süllőhínár	IAS	Inváziós	1554	46329	növények
Narcissus poeticus ssp. radiiflorus	csillagos nárcisz	\N	Védett	1554	46330	növények
Narcissus radiiflorus	csillagos nárcisz	\N	Védett	1554	46331	növények
Nasturtium officinale	vízitorma	\N	Védett	1554	46332	növények
Neckera pennata	tollas függönymoha	\N	Védett	1554	46333	növények
Neotinea tridentata	tarka kosbor (tarka pettyeskosbor)	\N	Védett	1554	46334	növények
Neotinea ustulata	sömörös kosbor (sömörös pettyeskosbor)	\N	Védett	1554	46335	növények
Neotinea ustulata ssp. aestivalis	sömörös kosbor (sömörös pettyeskosbor)	\N	Védett	1554	46336	növények
Neotinea ustulata ssp. ustulata	sömörös kosbor (sömörös pettyeskosbor)	\N	Védett	1554	46337	növények
Neotinea x dietrichiana	hibrid pettyeskosbor (tarka és sömörös kosbor hibrid)	\N	Védett	1554	46338	növények
Neottia nidus-avis	madárfészek (madárfészek-békakonty)	\N	Védett	1554	46339	növények
Neottia ovata	békakonty	\N	Védett	1554	46340	növények
Nepeta parviflora	borzas macskamenta	\N	Fokozottan védett	1554	46341	növények
Notholaena marantae	cselling (déli cselling)	\N	Fokozottan védett	1554	46342	növények
Nymphaea alba	fehér tündérrózsa	\N	Védett	1554	46343	növények
Nymphoides peltata	tündérfátyol (vízi tündérfátyol)	\N	Védett	1554	46344	növények
Oenothera biennis	parlagi ligetszépe	\N	Inváziós	1554	46345	növények
Omphalodes scorpioides	erdei békaszem	\N	Védett	1554	46346	növények
Onosma arenaria	homoki vértő	\N	Védett	1554	46347	növények
Onosma arenaria ssp. tuberculata	homoki vértő	\N	Védett	1554	46348	növények
Onosma arenarium	homoki vértő	\N	Védett	1554	46349	növények
Onosma sp.	vértő-fajok	\N	Védett	1554	46350	növények
Onosma tornense	tornai vértő	HD II kiemelt, IV	Fokozottan védett	1554	46351	növények
Onosma visianii	borzas vértő	\N	Védett	1554	46352	növények
Ophioglossum vulgatum	kígyónyelv (közönséges kígyónyelv)	\N	Védett	1554	46353	növények
Ophrys apifera	méhbangó	\N	Fokozottan védett	1554	46354	növények
Ophrys bertolonii	Bertoloni-bangó	\N	Fokozottan védett	1554	46355	növények
Ophrys fuciflora ssp. fuciflora	poszméhbangó	\N	Fokozottan védett	1554	46356	növények
Ophrys fuciflora ssp. holubyana	Holuby-bangó	\N	Fokozottan védett	1554	46357	növények
Ophrys holoserica	poszméhbangó	\N	Fokozottan védett	1554	46358	növények
Ophrys insectifera	légybangó	\N	Fokozottan védett	1554	46359	növények
Ophrys oestrifera	szarvas bangó	\N	Fokozottan védett	1554	46360	növények
Ophrys scolopax	szarvas bangó	\N	Fokozottan védett	1554	46361	növények
Ophrys scolopax ssp. cornuta	szarvas bangó	\N	Fokozottan védett	1554	46362	növények
Ophrys sp.	bangó-fajok	\N	Fokozottan védett	1554	46363	növények
Ophrys sphegodes	pókbangó	\N	Fokozottan védett	1554	46364	növények
Opuntia fragilis	törékeny fügekaktusz	\N	Inváziós	1554	46365	növények
Orchidaceae	orchidea-faj	\N	Védett	1554	46366	növények
Orchis coriophora	poloskaszagú kosbor (poloskaszagú sisakoskosbor)	\N	Védett	1554	46367	növények
Orchis laxiflora	mocsári kosbor (mocsári sisakoskosbor)	\N	Védett	1554	46368	növények
Orchis laxiflora ssp. elegans	pompás kosbor (pompás sisakoskosbor)	\N	Védett	1554	46369	növények
Orchis laxiflora ssp. palustris	mocsári kosbor (mocsári sisakoskosbor)	\N	Védett	1554	46370	növények
Orchis mascula	füles kosbor	\N	Védett	1554	46371	növények
Orchis mascula ssp. signifera	füles kosbor	\N	Védett	1554	46372	növények
Orchis militaris	vitézkosbor	\N	Védett	1554	46373	növények
Orchis pallens	sápadt kosbor	\N	Védett	1554	46375	növények
Orchis palustris	mocsári kosbor (mocsári sisakoskosbor)	\N	Védett	1554	46376	növények
Orchis purpurea	bíboros kosbor	\N	Védett	1554	46377	növények
Orchis simia	majomkosbor	\N	Védett	1554	46378	növények
Orchis sp.	kosbor-fajok	\N	Védett	1554	46379	növények
Orchis tridentata	tarka kosbor (tarka pettyeskosbor)	\N	Védett	1554	46380	növények
Orchis ustulata	sömörös kosbor (sömörös pettyeskosbor)	\N	Védett	1554	46381	növények
Oreopteris limbosperma	tölcséres hegyipáfrány	\N	Védett	1554	46382	növények
Ornithogalum brevistylum	nyúlánk sárma	\N	Védett	1554	46383	növények
Ornithogalum pannonicum	üstökös sárma	\N	Védett	1554	46384	növények
Ornithogalum pyramidale	nyúlánk sárma	\N	Védett	1554	46385	növények
Ornithogalum refractum	csilláros sárma	\N	Védett	1554	46386	növények
Ornithogalum sphaerocarpum	gömböstermésű sárma	\N	Védett	1554	46387	növények
Orobanche artemisiae-campestris	üröm-vajvirág (ürömszádor)	\N	Védett	1554	46388	növények
Orobanche bartlingii	Bartling-vajvirág (Bartling-szádor)	\N	Védett	1554	46389	növények
Orobanche caesia	deres vajvirág (deres szádor)	\N	Védett	1554	46390	növények
Orobanche coerulescens	kékes vajvirág (kékes szádor)	\N	Védett	1554	46391	növények
Orobanche flava	martilapu-vajvirág (martilapu-szádor)	\N	Védett	1554	46392	növények
Orobanche hederae	borostyán-vajvirág (borostyán-szádor)	\N	Védett	1554	46393	növények
Orobanche nana	apró vajvirág (apró szádor)	\N	Védett	1554	46394	növények
Orobanche pancicii	varfű-vajvirág (vajfűszádor)	\N	Védett	1554	46395	növények
Orthilia secunda	bókoló gyöngyvirágoskörtike	\N	Védett	1554	46396	növények
Orthotrichum rogeri	alhavasi szőrössüvegűmoha	\N	Védett	1554	46397	növények
Orthotrichum scanicum	halványzöld szőrössüvegűmoha	\N	Védett	1554	46398	növények
Orthotrichum stellatum	csillagos szőrössüvegűmoha	\N	Védett	1554	46399	növények
Osmunda regalis	királyharaszt (óriás királyharaszt)	\N	Fokozottan védett	1554	46400	növények
Oxalis corniculata	szürke madársóska	\N	Inváziós	1554	46401	növények
Oxalis dillenii	dillenius-madársóska	\N	Inváziós	1554	46402	növények
Oxalis stricta	felálló madársóska	\N	Inváziós	1554	46403	növények
Oxytropis pilosa	borzas csajkavirág	\N	Védett	1554	46404	növények
Oxytropis pilosa ssp. hungarica	borzas csajkavirág	\N	Védett	1554	46405	növények
Paeonia officinalis ssp. banatica	bánáti bazsarózsa	HD II, IV	Fokozottan védett	1554	46406	növények
Paeonia tenuifolia	keleti bazsarózsa	\N	Fokozottan védett	1554	46407	növények
Panicum capillare	hajszálágú köles	\N	Inváziós	1554	46408	növények
Panicum miliaceum ssp. ruderale	törékeny köles	\N	Inváziós	1554	46409	növények
Panicum sp.	köles	\N	Inváziós	1554	46410	növények
Parnassia palustris	fehérmájvirág (mocsári tőzegboglár)	\N	Védett	1554	46411	növények
Paronychia cephalotes	ezüstaszott (keskenylevelű ezüstvirág)	\N	Védett	1554	46412	növények
Parthenium hysterophorus	keserű hamisüröm	IAS	Inváziós	1554	46413	növények
Parthenocissus inserta	közönséges vadszőlő	\N	Inváziós	1554	46414	növények
Paulownia sp.	császárfa	\N	Inváziós	1554	46415	növények
Paulownia tomentosa	császárfa	\N	Inváziós	1554	46416	növények
Pedicularis palustris	posvány kakastaréj	\N	Védett	1554	46417	növények
Pennisetum setaceum	tollborzfű	IAS	Inváziós	1554	46418	növények
Persicaria bistorta	kígyógyökerű keserűfű	\N	Védett	1554	46419	növények
Persicaria perfoliata	ördögfarok keserűfű	IAS	Inváziós	1554	46420	növények
Petasites albus	fehér acsalapu	\N	Védett	1554	46421	növények
Petrosimonia triandra	szikárszik	\N	Védett	1554	46422	növények
Peucedanum arenarium	homoki kocsord	\N	Védett	1554	46423	növények
Peucedanum officinale	sziki kocsord	\N	Védett	1554	46424	növények
Peucedanum palustre	mocsári kocsord	\N	Védett	1554	46425	növények
Peucedanum verticillare	magasszárú kocsord	\N	Védett	1554	46426	növények
Phascum floekeanum	vörösbarna rügymoha	\N	Védett	1554	46427	növények
Phegopteris connectilis	bükkös buglyospáfrány	\N	Védett	1554	46428	növények
Phlomis tuberosa	macskahere (gumós macskahere)	\N	Védett	1554	46429	növények
Phyllitis scolopendrium	gímpáfrány (gímnyelvű fodorka)	\N	Védett	1554	46430	növények
Physcomitrium sphaericum	gömbös körtikemoha	\N	Védett	1554	46431	növények
Physospermum cornubiense	harangláblevelű dudamag	\N	Védett	1554	46432	növények
Phyteuma orbiculare	gombos varjúköröm	\N	Védett	1554	46433	növények
Phyteuma spicatum	erdei varjúköröm	\N	Védett	1554	46434	növények
Phytolacca americana	amerikai alkörmös	\N	Inváziós	1554	46435	növények
Phytolacca esculenta	kínai alkörmös	\N	Inváziós	1554	46436	növények
Pinguicula vulgaris	lápi hízóka	\N	Fokozottan védett	1554	46437	növények
Pistia stratiotes	úszó kagylótutaj	\N	Inváziós	1554	46438	növények
Pisum elatius	magas borsó	\N	Védett	1554	46439	növények
Plantago argentea	ezüstös útifű	\N	Védett	1554	46440	növények
Plantago maxima	óriás útifű	\N	Fokozottan védett	1554	46441	növények
Plantago schwarzenbergiana	erdélyi útifű	\N	Védett	1554	46442	növények
Platanthera bifolia	kétlevelű sarkvirág	\N	Védett	1554	46443	növények
Platanthera chlorantha	zöldes sarkvirág	\N	Védett	1554	46444	növények
Platanthera sp.	sarkvirág-fajok	\N	Védett	1554	46445	növények
Pleurospermum austriacum	osztrák borzamag	\N	Védett	1554	46446	növények
Poa pannonica ssp. glabra	hegyi perje	\N	Védett	1554	46447	növények
Poa remota	magyar perje	\N	Védett	1554	46448	növények
Poa scabra	hegyi perje	\N	Védett	1554	46449	növények
Polygala amarella	kisvirágú pacsirtafű	\N	Védett	1554	46450	növények
Polygala major	nagy pacsirtafű	\N	Védett	1554	46451	növények
Polygonatum verticillatum	pávafarkú salamonpecsét	\N	Védett	1554	46452	növények
Polystichum aculeatum	karéjos vesepáfrány	\N	Védett	1554	46453	növények
Polystichum braunii	szőrös vesepáfrány	\N	Védett	1554	46454	növények
Polystichum lonchitis	dárdás vesepáfrány	\N	Védett	1554	46455	növények
Polystichum setiferum	díszes vesepáfrány	\N	Védett	1554	46456	növények
Polystichum sp.	vesepáfrány-fajok	\N	Védett	1554	46457	növények
Potamogeton coloratus	színes békaszőlő	\N	Védett	1554	46458	növények
Potamogeton obtusifolius	tompalevelű békaszőlő	\N	Védett	1554	46459	növények
Potentilla indica	indiaiszamóca	\N	Inváziós	1554	46460	növények
Potentilla palustris	tőzegeper (tőzegpimpó)	\N	Fokozottan védett	1554	46461	növények
Potentilla patula	kiterült pimpó	\N	Védett	1554	46462	növények
Potentilla rupestris	kövi pimpó	\N	Védett	1554	46463	növények
Primula auricula	cifra kankalin (medvefül kankalin)	\N	Fokozottan védett	1554	46464	növények
Primula elatior	sudár kankalin (sugárkankalin)	\N	Védett	1554	46465	növények
Primula farinosa	lisztes kankalin	\N	Fokozottan védett	1554	46466	növények
Primula nutans	bókoló kankalin	\N	7_melleklet	1554	46467	növények
Primula vulgaris	szártalan kankalin	\N	Védett	1554	46468	növények
Primula x brevistyla	szártalan/tavaszi kankalin hibrid	\N	Védett	1554	46469	növények
Prospero elisae	őszi csillagvirágok	\N	Védett	1554	46470	növények
Prospero paratheticum	őszi csillagvirágok	\N	Védett	1554	46471	növények
Prunella grandiflora	nagyvirágú gyíkfű	\N	Védett	1554	46472	növények
Prunella x dissecta	nagyvirágú/fehér gyíkfű hibrid	\N	Védett	1554	46473	növények
Prunus serotina	kései meggy	\N	Inváziós	1554	46474	növények
Prunus tenella	törpe mandula	\N	Védett	1554	46475	növények
Pseudolysimachion incanum	szürke fürtösveronika	\N	Védett	1554	46476	növények
Pseudolysimachion longifolium	hosszúlevelű fürtösveronika	\N	Védett	1554	46477	növények
Pseudolysimachion spurium	bugás fürtösveronika	\N	Fokozottan védett	1554	46478	növények
Pseudolysimachion spurium ssp. foliosum	bugás fürtösveronika alfaj	\N	Fokozottan védett	1554	46479	növények
Pterygoneurum lamellatum	lemezes szárnyaserűmoha	\N	Védett	1554	46480	növények
Puccinellia festuciformis ssp. intermedia	fertőtavi mézpázsit	\N	Védett	1554	46481	növények
Puccinellia peisonis	fertőtavi mézpázsit	\N	Védett	1554	46482	növények
Pueraria lobata	kudzu nyílgyökér	IAS	Inváziós	1554	46483	növények
Pueraria montana	kudzu nyílgyökér	IAS	Inváziós	1554	46484	növények
Pueraria montana var. lobata	kudzu nyílgyökér	IAS	Inváziós	1554	46485	növények
Pulmonaria angustifolia	keskenylevelű tüdőfű	\N	Védett	1554	46486	növények
Pulsatilla flavescens	magyar kökörcsin	HD II kiemelt, IV	Fokozottan védett	1554	46487	növények
Pulsatilla grandis	leánykökörcsin	HD II, IV	Védett	1554	46488	növények
Pulsatilla montana	hegyi kökörcsin	\N	Védett	1554	46489	növények
Pulsatilla nigricans	fekete kökörcsin	\N	Védett	1554	46490	növények
Pulsatilla patens	tátogó kökörcsin	HD II, IV	Fokozottan védett	1554	46491	növények
Pulsatilla pratensis ssp. hungarica	magyar kökörcsin	HD II kiemelt, IV	Fokozottan védett	1554	46492	növények
Pulsatilla pratensis ssp. nigricans	fekete kökörcsin	\N	Védett	1554	46493	növények
Pulsatilla sp.	kökörcsin-fajok	\N	Védett	1554	46494	növények
Pulsatilla zimmermannii	hegyi kökörcsin	\N	Védett	1554	46495	növények
Pyramidula tetragona	négysarkú piramismoha	\N	Védett	1554	46496	növények
Pyrola chlorantha	zöldes körtike	\N	Védett	1554	46497	növények
Pyrola media	közepes körtike	\N	Védett	1554	46498	növények
Pyrola minor	kis körtike	\N	Védett	1554	46499	növények
Pyrola rotundifolia	kereklevelű körtike	\N	Védett	1554	46500	növények
Pyrola sp.	körtike-fajok	\N	Védett	1554	46501	növények
Pyrus magyarica	magyar vadkörte	HD II kiemelt, IV	Fokozottan védett	1554	46502	növények
Pyrus nivalis	vastaggallyú körte	\N	Védett	1554	46503	növények
Radiola linoides	apró csepplen	\N	Védett	1554	46504	növények
Ranunculus fluitans	úszó víziboglárka	\N	Védett	1554	46505	növények
Ranunculus illyricus	selymes boglárka	\N	Védett	1554	46506	növények
Ranunculus lateriflorus	sziki boglárka	\N	Védett	1554	46507	növények
Ranunculus lingua	nádi boglárka	\N	Védett	1554	46508	növények
Ranunculus nemorosus	berki boglárka	\N	Védett	1554	46509	növények
Ranunculus polyphyllus	buglyos boglárka	\N	Védett	1554	46510	növények
Ranunculus psilostachys	csőrös boglárka	\N	Védett	1554	46511	növények
Ranunculus strigulosus	merevszőrű boglárka (Steven-boglárka)	\N	Védett	1554	46512	növények
Reseda inodora	szagtalan rezeda	\N	Védett	1554	46513	növények
Reynoutria japonica	japánkeserűfű	\N	Inváziós	1554	46514	növények
Reynoutria sp.	japánkeserűfű	\N	Inváziós	1554	46515	növények
Reynoutria x bohemica	cseh óriáskeserűfű	\N	Inváziós	1554	46516	növények
Rhamnus saxatilis	sziklai benge	\N	Védett	1554	46517	növények
Rhus hirta	ecetfa	\N	Inváziós	1554	46518	növények
Rhus typhina	ecetfa	\N	Inváziós	1554	46519	növények
Rhynchospora alba	fehér tőzegkáka	\N	Védett	1554	46520	növények
Rhynchostegiella jacquinii	mattzöld kiscsőrűmoha	\N	Védett	1554	46521	növények
Rhynchostegium rotundifolium	kereklevelű hosszúcsőrűmoha	\N	Védett	1554	46522	növények
Ribes alpinum	havasi ribiszke	\N	Védett	1554	46523	növények
Ribes aureum	arany ribiszke	\N	Inváziós	1554	46524	növények
Ribes nigrum	fekete ribiszke	\N	Védett	1554	46525	növények
Ribes petraeum	bérci ribiszke	\N	Védett	1554	46526	növények
Riccia frostii	Frost-májmoha	\N	Védett	1554	46527	növények
Riccia huebeneriana	Hübener-májmoha	\N	Védett	1554	46528	növények
Robinia pseudoacacia	fehér akác	\N	Inváziós	1554	46529	növények
Rosa pendulina	havasalji rózsa	\N	Védett	1554	46530	növények
Rosa sancti-andreae	gyapjas rózsa (incl. szentendrei rózsa)	\N	Védett	1554	46531	növények
Rosa villosa	gyapjas rózsa (incl. szentendrei rózsa)	\N	Védett	1554	46532	növények
Rubus saxatilis	kövi szeder	\N	Védett	1554	46533	növények
Rudbeckia hirta	borzas kúpvirág	\N	Inváziós	1554	46534	növények
Rudbeckia laciniata	magas kúpvirág	\N	Inváziós	1554	46535	növények
Rumex pseudonatronatus	sziki lórom	\N	Védett	1554	46536	növények
Ruscus aculeatus	szúrós csodabogyó	HD V	Védett	1554	46537	növények
Ruscus hypoglossum	lónyelvű csodabogyó	\N	Védett	1554	46538	növények
Salix aurita	füles fűz	\N	Védett	1554	46539	növények
Salix elaeagnos	parti fűz	\N	Védett	1554	46540	növények
Salix myrsinifolia	feketéllő fűz	\N	Védett	1554	46541	növények
Salix pentandra	babérfűz	\N	Védett	1554	46542	növények
Salsola soda	sziki ballagófű	\N	Védett	1554	46543	növények
Salvia nutans	kónya zsálya	\N	Fokozottan védett	1554	46544	növények
Salvinia natans	rucaöröm (vízi rucaöröm)	\N	Védett	1554	46545	növények
Samolus valerandi	sziki árokvirág	\N	Védett	1554	46546	növények
Saxifraga adscendens	hegyi kőtörőfű	\N	Védett	1554	46547	növények
Saxifraga paniculata	fürtös kőtörőfű	\N	Védett	1554	46548	növények
Scabiosa canescens	szürkés ördögszem	\N	Védett	1554	46549	növények
Schoenus nigricans	kormos csáté	\N	Védett	1554	46550	növények
Scilla autumnalis	őszi csillagvirágok	\N	Védett	1554	46551	növények
Scilla autumnalis agg.	őszi csillagvirágok	\N	Védett	1554	46552	növények
Scilla bifolia agg.	tavaszi csillagvirág-fajok	\N	Védett	1554	46553	növények
Scilla drunensis	nyugati csillagvirág	\N	Védett	1554	46554	növények
Scilla kladnii	erdélyi csillagvirág	\N	Védett	1554	46555	növények
Scilla spetana	Speta-csillagvirág	\N	Védett	1554	46556	növények
Scilla vindobonensis	ligeti csillagvirág	\N	Védett	1554	46557	növények
Scleranthus perennis	évelő szikárka	\N	Védett	1554	46558	növények
Scolochloa festucacea	északi mocsáricsenkesz	\N	Védett	1554	46559	növények
Scopolia carniolica	krajnai farkasbogyó	\N	Védett	1554	46560	növények
Scorpidium scorpioides	skorpiómoha	\N	Védett	1554	46561	növények
Scorzonera humilis	alacsony pozdor	\N	Védett	1554	46562	növények
Scorzonera purpurea	piros pozdor	\N	Védett	1554	46563	növények
Scrophularia scopolii	bársonyos görvélyfű	\N	Védett	1554	46564	növények
Scrophularia vernalis	tavaszi görvélyfű	\N	Védett	1554	46565	növények
Scutellaria columnae	bozontos csukóka	\N	Védett	1554	46566	növények
Securigera elegans	nagylevelű tarkakoronafürt	\N	Védett	1554	46567	növények
Sedum acre ssp. neglectum	adriai varjúháj	\N	Védett	1554	46568	növények
Sedum caespitosum	sziki varjúháj	\N	Védett	1554	46569	növények
Sedum hillebrandtii	homoki varjúháj	\N	Védett	1554	46570	növények
Sedum hispanicum	deres varjúháj	\N	Védett	1554	46571	növények
Sedum urvillei ssp. hillebrandtii	homoki varjúháj	\N	Védett	1554	46572	növények
Selaginella helvetica	hegyi csipkeharaszt	\N	Védett	1554	46573	növények
Sempervivum marmoreum	mátrai kövirózsa (rózsás kövirózsa)	\N	Védett	1554	46574	növények
Sempervivum marmoreum auct. hung.	mátrai kövirózsa (rózsás kövirózsa)	\N	Védett	1554	46575	növények
Sempervivum matricum	mátrai kövirózsa (rózsás kövirózsa)	\N	Védett	1554	46576	növények
Sempervivum sp.	kövirózsa-fajok	\N	Védett	1554	46577	növények
Sempervivum tectorum	házi kövirózsa (fali kövirózsa)	\N	Védett	1554	46578	növények
Senecio aquaticus	vízi aggófű	\N	Védett	1554	46579	növények
Senecio inaequidens	vesszős aggófű	\N	Inváziós	1554	46580	növények
Senecio ovirensis	hosszúlevelű aggóvirág	\N	Védett	1554	46581	növények
Senecio paludosus	mocsári aggófű	\N	Védett	1554	46582	növények
Senecio umbrosus	nagy aggófű	\N	Fokozottan védett	1554	46583	növények
Serratula radiata	sugaras zsoltina	\N	Védett	1554	46585	növények
Seseli leucospermum	magyar gurgolya	HD II, IV	Fokozottan védett	1554	46586	növények
Seseli peucedanoides	zöldes gurgolya (zöldes kígyókapor)	\N	Védett	1554	46587	növények
Sesleria albicans	tarka nyúlfarkfű	\N	Védett	1554	46588	növények
Sesleria caerulea	lápi nyúlfarkfű	\N	Védett	1554	46589	növények
Sesleria heufleriana	erdélyi nyúlfarkfű	\N	Védett	1554	46590	növények
Sesleria heufleriana ssp. hungarica	magyar nyúlfarkfű	\N	Védett	1554	46591	növények
Sesleria hungarica	magyar nyúlfarkfű	\N	Védett	1554	46592	növények
Sesleria sadleriana	budai nyúlfarkfű	\N	Védett	1554	46593	növények
Sesleria sp.	nyúlfarkfű-fajok	\N	Védett	1554	46594	növények
Sesleria uliginosa	lápi nyúlfarkfű	\N	Védett	1554	46595	növények
Silaum peucedanoides	zöldes gurgolya (zöldes kígyókapor)	\N	Védett	1554	46596	növények
Silene borysthenica	kisvirágú habszegfű	\N	Védett	1554	46597	növények
Silene bupleuroides	termetes habszegfű (gór habszegfű)	\N	Védett	1554	46598	növények
Silene dioica	piros mécsvirág	\N	Védett	1554	46599	növények
Silene flavescens	sárgás habszegfű	\N	Fokozottan védett	1554	46600	növények
Silene multiflora	sokvirágú habszegfű	\N	Védett	1554	46601	növények
Silene nemoralis	berki habszegfű	\N	Védett	1554	46602	növények
Sisymbrium polymorphum	karcsú zsombor	\N	Védett	1554	46603	növények
Sium sisarum	keleti békakorsó	\N	Védett	1554	46604	növények
Solidago canadensis	kanadai aranyvessző	\N	Inváziós	1554	46605	növények
Solidago gigantea	magas aranyvessző	\N	Inváziós	1554	46606	növények
Solidago sp.	aranyvessző	\N	Inváziós	1554	46607	növények
Sonchus palustris	mocsári csorbóka	\N	Védett	1554	46608	növények
Sorbus × kitaibeliana	dunai/barkócaberkenye hibrid	\N	Védett	1554	46609	növények
Sorbus acutiserratus	kőhányási berkenye	\N	Védett	1554	46610	növények
Sorbus adami	Ádám-berkenye	\N	Védett	1554	46611	növények
Sorbus andreanszkyana	Andreánszky-berkenye	\N	Védett	1554	46612	növények
Sorbus aria	lisztes berkenye	\N	Védett	1554	46613	növények
Sorbus austriaca	Hazslinszky-berkenye (osztrák berkenye alakköre)	\N	Védett	1554	46614	növények
Sorbus bakonyensis	bakonyi berkenye	\N	Védett	1554	46615	növények
Sorbus balatonica	balatoni berkenye	\N	Védett	1554	46616	növények
Sorbus barabitsii	Barabits-berkenye	\N	Védett	1554	46617	növények
Sorbus barthae	Bartha-berkenye	\N	Védett	1554	46618	növények
Sorbus bodajkensis	bodajki berkenye	\N	Védett	1554	46619	növények
Sorbus borosiana	Boros-berkenye	\N	Védett	1554	46620	növények
Sorbus budaiana	Budai-berkenye	\N	Védett	1554	46621	növények
Sorbus buekkensis	bükki berkenye	\N	Védett	1554	46622	növények
Sorbus cretica	déli berkenye	\N	Védett	1554	46623	növények
Sorbus danubialis	dunai berkenye	\N	Védett	1554	46624	növények
Sorbus decipientiformis	keszthelyi berkenye	\N	Védett	1554	46625	növények
Sorbus degenii	Degen-berkenye	\N	Védett	1554	46626	növények
Sorbus domestica	házi berkenye (kerti berkenye)	\N	Védett	1554	46627	növények
Sorbus dracofolius	gánti berkenye	\N	Védett	1554	46628	növények
Sorbus eugenii-kelleri	Keller-berkenye	\N	Védett	1554	46629	növények
Sorbus gayerana	Gáyer-berkenye	\N	Védett	1554	46630	növények
Sorbus gerecseensis	gerecsei berkenye	\N	Védett	1554	46631	növények
Sorbus graeca	déli berkenye	\N	Védett	1554	46632	növények
Sorbus hazslinszkyana	Hazslinszky-berkenye (osztrák berkenye alakköre)	\N	Védett	1554	46633	növények
Sorbus huljakii	Hulják-berkenye	\N	Védett	1554	46634	növények
Sorbus javorkae	Jávorka-berkenye	\N	Védett	1554	46635	növények
Sorbus karpatii	kárpáti-berkenye	\N	Védett	1554	46636	növények
Sorbus latissima	nagylevelű berkenye	\N	Védett	1554	46637	növények
Sorbus majeri	Májer-berkenye	\N	Védett	1554	46638	növények
Sorbus pannonica	dunántúli berkenye	\N	Védett	1554	46639	növények
Sorbus pseudobakonyensis	rövidkaréjú berkenye	\N	Védett	1554	46640	növények
Sorbus pseudodanubialis	dunamenti berkenye	\N	Védett	1554	46641	növények
Sorbus pseudolatifolia	széleslevelű berkenye	\N	Védett	1554	46642	növények
Sorbus pseudosemiincisa	kevéserű berkenye	\N	Védett	1554	46643	növények
Sorbus pseudovertesensis	csákberényi berkenye	\N	Védett	1554	46644	növények
Sorbus redliana	Rédl-berkenye	\N	Védett	1554	46645	növények
Sorbus semiincisa	budai berkenye	\N	Védett	1554	46646	növények
Sorbus simonkaiana	Simonkai-berkenye	\N	Védett	1554	46647	növények
Sorbus sooi	Soó-berkenye	\N	Védett	1554	46648	növények
Sorbus subdanubialis	közép-dunai berkenye (Duna-vidéki berkenye)	\N	Védett	1554	46649	növények
Sorbus thaiszii	Thaisz-berkenye	\N	Védett	1554	46650	növények
Sorbus tobani	Tobán-berkenye	\N	Védett	1554	46651	növények
Sorbus ulmifolia	szillevelű berkenye	\N	Védett	1554	46652	növények
Sorbus vajdae	Vajda-berkenye	\N	Védett	1554	46653	növények
Sorbus vallerubusensis	szedresvölgyi berkenye	\N	Védett	1554	46654	növények
Sorbus vertesensis	vértesi berkenye	\N	Védett	1554	46655	növények
Sorbus veszpremensis	veszprémi berkenye	\N	Védett	1554	46656	növények
Sorbus x buekkensis	bükki berkenye	\N	Védett	1554	46657	növények
Sorbus zolyomii	Zólyomi-berkenye	\N	Védett	1554	46658	növények
Sorghum halepense	fenyércirok	\N	Inváziós	1554	46659	növények
Sparganium natans	lápi békabuzogány	\N	Fokozottan védett	1554	46660	növények
Sphagnum auriculatum	fülecskés tőzegmoha	HD V	Védett	1554	46661	növények
Sphagnum capillifolium	félgömbfejű tőzegmoha	HD V	Védett	1554	46662	növények
Sphagnum centrale	sárgásbarna tőzegmoha	HD V	Védett	1554	46663	növények
Sphagnum compactum	tömöttágú tőzegmoha	HD V	Védett	1554	46664	növények
Sphagnum contortum	csavart tőzegmoha	HD V	Védett	1554	46665	növények
Sphagnum cuspidatum	keskenylevelű tőzegmoha	HD V	Védett	1554	46666	növények
Sphagnum fimbriatum	rojtos tőzegmoha	HD V	Védett	1554	46667	növények
Sphagnum girgensohnii	Girgensohn-tőzegmoha	HD V	Védett	1554	46668	növények
Sphagnum magellanicum	magellán-tőzegmoha	HD V	Védett	1554	46669	növények
Sphagnum obtusum	tompalevelű tőzegmoha	HD V	Védett	1554	46670	növények
Sphagnum palustre	csónakos tőzegmoha	HD V	Védett	1554	46671	növények
Sphagnum platyphyllum	lágy tőzegmoha	HD V	Védett	1554	46672	növények
Sphagnum quinquefarium	ötlevélsoros tőzegmoha	HD V	Védett	1554	46673	növények
Sphagnum recurvum	karcsú tőzegmoha	HD V	Védett	1554	46674	növények
Sphagnum russowi	óriás-tőzegmoha	HD V	Védett	1554	46675	növények
Sphagnum sp.	tőzegmoha fajok	HD V	Védett	1554	46676	növények
Sphagnum spp.	tőzegmoha fajok	HD V	Védett	1554	46677	növények
Sphagnum squarrosum	berzedt tőzegmoha	HD V	Védett	1554	46678	növények
Sphagnum subnitens	halványbarna tőzegmoha	HD V	Védett	1554	46679	növények
Sphagnum subsecundum	zászlós tőzegmoha	HD V	Védett	1554	46680	növények
Sphagnum teres	láperdei tőzegmoha	HD V	Védett	1554	46681	növények
Sphagnum warnstorfi	Warnstorf-tőzegmoha	HD V	Védett	1554	46682	növények
Spiraea crenata	csipkés gyöngyvessző	\N	Védett	1554	46683	növények
Spiraea media	szirti gyöngyvessző	\N	Védett	1554	46684	növények
Spiraea salicifolia	fűzlevelű gyöngyvessző	\N	Védett	1554	46685	növények
Spiranthes aestivalis	nyári füzértekercs	HD IV	Védett	1554	46686	növények
Spiranthes spiralis	őszi füzértekercs	\N	Védett	1554	46687	növények
Stachys alpina	havasi tisztesfű	\N	Védett	1554	46688	növények
Stellaria palustris	mocsári csillaghúr	\N	Védett	1554	46689	növények
Sternbergia colchiciflora	vetővirág (apró vetővirág)	\N	Védett	1554	46690	növények
Stipa borysthenica	homoki árvalányhaj	\N	Védett	1554	46691	növények
Stipa bromoides	szálkás árvalányhaj	\N	Védett	1554	46692	növények
Stipa dasyphylla	bozontos árvalányhaj	\N	Védett	1554	46693	növények
Stipa eriocaulis	délvidéki árvalányhaj	\N	Védett	1554	46694	növények
Stipa joannis	pusztai árvalányhaj	\N	Védett	1554	46695	növények
Stipa pennata	pusztai árvalányhaj	\N	Védett	1554	46696	növények
Stipa pulcherrima	csinos árvalányhaj	\N	Védett	1554	46697	növények
Stipa stenophylla	hosszúlevelű árvalányhaj	\N	Védett	1554	46698	növények
Stipa tirsa	hosszúlevelű árvalányhaj	\N	Védett	1554	46699	növények
Syringa vulgaris	közönséges orgona	\N	Inváziós	1554	46700	növények
Tamus communis	pirítógyökér (felfutó pirítógyökér)	\N	Védett	1554	46701	növények
Taraxacum serotinum	kései pitypang	\N	Védett	1554	46702	növények
Taxiphyllum densifolium	sűrűleveles kaukázusimoha	\N	Védett	1554	46703	növények
Teesdalia nudicaulis	nyugati rejtőke	\N	Védett	1554	46704	növények
Telekia speciosa	Teleki-virág (pompás Teleki-virág)	\N	Védett	1554	46705	növények
Tephroseris aurantiaca	narancsszínű aggóvirág	\N	Védett	1554	46706	növények
Tephroseris crispa	csermelyaggóvirág	\N	Védett	1554	46707	növények
Tephroseris longifolia	hosszúlevelű aggóvirág	\N	Védett	1554	46708	növények
Teucrium scorodonia	fenyérgamandor	\N	Védett	1554	46709	növények
Thalictrum aquilegiifolium	erdei borkóró	\N	Védett	1554	46710	növények
Thalictrum foetidum	sziklai borkóró	\N	Védett	1554	46711	növények
Thalictrum minus ssp. pseudominus	kékes borkóró	\N	Védett	1554	46712	növények
Thalictrum pseudominus	kékes borkóró	\N	Védett	1554	46713	növények
Thelypteris palustris	tőzegpáfrány (mocsári tőzegpáfrány)	\N	Védett	1554	46714	növények
Thlaspi alliaceum	hagymaszagú tarsóka	\N	Védett	1554	46715	növények
Thlaspi caerulescens	havasalji tarsóka	\N	Védett	1554	46716	növények
Thlaspi goesingense	osztrák tarsóka	\N	Védett	1554	46717	növények
Thlaspi hungaricum	Janka-tarsóka (incl. magyar tarsóka)	HD II, IV	Védett	1554	46718	növények
Thlaspi jankae	Janka-tarsóka (incl. magyar tarsóka)	HD II, IV	Védett	1554	46719	növények
Thlaspi jankae agg.	Janka-tarsóka (incl. magyar tarsóka)	HD II, IV	Védett	1554	46720	növények
Thlaspi kovatsii	Schudich-tarsóka	\N	Védett	1554	46721	növények
Thlaspi kovatsii ssp. schudichii	Schudich-tarsóka	\N	Védett	1554	46722	növények
Thlaspi montanum	hegyi tarsóka	\N	Védett	1554	46723	növények
Torilis ucrainica	keleti tüskemag	\N	Védett	1554	46724	növények
Tortula brevissima	törpelöszmoha	\N	Védett	1554	46725	növények
Tragopogon floccosus	homoki bakszakáll	\N	Védett	1554	46726	növények
Tragus racemosus	bugás tövisperje	\N	Inváziós	1554	46727	növények
Trapa natans	sulyom (csemegesulyom)	\N	Védett	1554	46728	növények
Traunsteinera globosa	gömböskosbor (karcsú gömböskosbor)	\N	Fokozottan védett	1554	46729	növények
Trifolium ornithopodioides	egyvirágú here	\N	Védett	1554	46730	növények
Trifolium subterraneum	földbentermő here	\N	Védett	1554	46731	növények
Trifolium vesiculosum	hólyagos here	\N	Védett	1554	46732	növények
Triglochin palustre	mocsári kígyófű	\N	Védett	1554	46733	növények
Trigonella gladiata	bakszarvú lepkeszeg	\N	Védett	1554	46734	növények
Trinia ramosissima	magyar nyúlkapor	\N	Védett	1554	46735	növények
Trollius europaeus	zergeboglár (közönséges zergeboglár)	\N	Védett	1554	46736	növények
Ulmus procera	érdeslevelű szil	\N	Inváziós	1554	46737	növények
Ulmus pumila	szibériai (turkesztáni) szil	\N	Inváziós	1554	46738	növények
Urtica kioviensis	kúszó csalán (lápi csalán)	\N	Védett	1554	46739	növények
Utricularia bremii	lápi rence	\N	Fokozottan védett	1554	46740	növények
Utricularia minor	kis rence	\N	Védett	1554	46741	növények
Vaccinium microcarpum	tőzegáfonya	\N	Védett	1554	46742	növények
Vaccinium oxycoccos	tőzegáfonya	\N	Védett	1554	46743	növények
Vaccinium vitis-idaea	vörös áfonya	\N	Védett	1554	46744	növények
Valeriana officinalis ssp. sambucifolia	bodzalevelű macskagyökér	\N	Védett	1554	46745	növények
Valeriana simplicifolia	éplevelű macskagyökér	\N	Védett	1554	46746	növények
Valeriana tripteris	hármaslevelű macskagyökér	\N	Védett	1554	46747	növények
Vallisneria spiralis	közönséges csavarhínár	\N	Inváziós	1554	46748	növények
Veratrum album	fehér zászpa	\N	Védett	1554	46749	növények
Verbena supina	henye vasfű	\N	Védett	1554	46750	növények
Vicia biennis	kunsági bükköny	\N	Fokozottan védett	1554	46751	növények
Vicia narbonensis	fogaslevelű bükköny	\N	Védett	1554	46752	növények
Vicia oroboides	zalai bükköny	\N	Védett	1554	46753	növények
Vicia sparsiflora	pilisi bükköny	\N	Védett	1554	46754	növények
Vicia sylvatica	ligeti bükköny	\N	Védett	1554	46755	növények
Vinca herbacea	pusztai meténg	\N	Védett	1554	46756	növények
Vincetoxicum pannonicum	magyar méreggyilok	HD II, IV	Fokozottan védett	1554	46757	növények
Viola biflora	sárga ibolya	\N	Védett	1554	46758	növények
Viola collina	dombi ibolya	\N	Védett	1554	46759	növények
Viola stagnina	lápi ibolya	\N	Védett	1554	46760	növények
Vitis riparia	parti szőlő	\N	Inváziós	1554	46761	növények
Vitis rupestris	sziklai szőlő	\N	Inváziós	1554	46762	növények
Vitis sylvestris	ligeti szőlő	\N	Védett	1554	46763	növények
Vitis vulpina	parti szőlő	\N	Inváziós	1554	46764	növények
Weisia rostellata	kiscsőrű gyöngymoha	\N	Védett	1554	46765	növények
Woodsia alpina	havasi szirtipáfrány	\N	Védett	1554	46766	növények
Woodsia ilvensis	hegyi szirtipáfrány (északi szirtipárfány)	\N	Fokozottan védett	1554	46767	növények
Xanthium albinum ssp. riparium	elbai szerbtövis	\N	Inváziós	1554	46768	növények
Xanthium italicum	olasz szerbtövis	\N	Inváziós	1554	46769	növények
Xanthium saccharatum	nagytermésű szerbtövis	\N	Inváziós	1554	46770	növények
Xanthium sp.	szerbtövis	\N	Inváziós	1554	46771	növények
Sialis nigripes	fekete vízifátyolka	\N	Védett	1554	46772	recésszárnyúak
Cucujus cinnaberinus	skarlátbogár	HD II, IV	Védett	1554	44545	bogarak
Polymitarcis virgo	dunavirág	\N	Védett	1554	44963	kérészek
Ephoron virgo	dunavirág	\N	Védett	1684	46775	kérészek
Fallopia x hybrida	japánkeserűfű hibrid	\N	Inváziós	1685	46776	növények
Phytolacca acinosa	kínai alkörmös	\N	Inváziós	1686	46777	növények
Hymenalia morio	magyar alkonybogár	\N	Védett	1554	44702	bogarak
Anacamptis morio	agár kosbor (agár sisakoskosbor)	\N	Védett	1554	45789	növények
Orchis morio	agár kosbor (agár sisakoskosbor)	\N	Védett	1554	46374	növények
Carabus ulrichii	rezes futrinka	\N	Védett	1728	46778	bogarak
Cicada orni	mannakabóca	\N	Védett	1554	44513	kabócák, növénytetvek
Metcalfa pruinosa	amerikai lepkekabóca	\N	Inváziós	1554	44818	kabócák, növénytetvek
Stictocephala bisonia	amerikai bivalykabóca	\N	Inváziós	1554	45068	kabócák, növénytetvek
Tibicina haematodes	óriás-énekeskabóca	\N	Védett	1554	45087	kabócák, növénytetvek
Eresus sandaliatus	bikapók	\N	Védett	1731	46779	pókok
Eresus moravicus	bikapók	\N	Védett	1731	46780	pókok
Eresus kollari	bikapók	\N	Védett	1731	46781	pókok
Eresus hermani	bikapók	\N	Védett	1731	46782	pókok
Eresus sp.	bikapók faj	\N	Védett	1732	46783	pókok
Ludwigia repens	pirosfonákú tóalma	\N	Inváziós	1766	46784	növények
Phyllostachys bambusoides	bambusz	\N	Inváziós	1766	46785	növények
Hygrophila polysperma	indiai vízicsillag	\N	Inváziós	1767	46786	növények
Geolycosa vultuosa	pokoli cselőpók	\N	Védett	1787	46787	pókok
Gaillardia sp.	kokárdavirág	\N	Inváziós	1920	46788	növények
Egeria densa	argentin átokhínár	\N	Inváziós	2210	46789	növények
Shinnersia rivularis	mexikói tölgy	\N	Inváziós	2210	46791	növények
Cherax quadricarinatus	ausztrál vörösollós rák	\N	Inváziós	2210	46792	rákok
Xiphophorus helleri	mexikói kardfarkú hal (szifó)	\N	Inváziós	2210	46793	halak
Xiphophorus sp.	platti	\N	Inváziós	2210	46794	halak
Xiphophorus maculatus	széleshátú fogasponty	\N	Inváziós	2210	46795	halak
Gambusia affinis	szúnyogirtó fogasponty	\N	Inváziós	2210	46796	halak
Proterorhinus semilunaris	tarka géb 	\N	Inváziós	2210	46797	halak
Gasterosteus aculeatus	tüskés pikó	\N	Inváziós	2210	46798	halak
Opuntia phaeacantha	coloradói fügekaktusz	\N	Inváziós	2211	46799	növények
Opuntia sp.	fügekaktusz	\N	Inváziós	2212	46800	növények
Larus heuglini	\N	\N	Védett	2279	46802	madarak
Ablepharus kitaibelii fitzingeri	pannon gyík (magyar gyík)	HD IV	Fokozottan védett	3205	46803	hüllők
Ablepharus kitaibelii	pannon gyík (magyar gyík)	HD IV	Fokozottan védett	1554	45251	hüllők
Parthenocissus quinquefolia	tapadó vadszőlő	\N	Inváziós	3210	46804	növények
Phytolacca sp.	alkörmös faj	\N	Inváziós	3271	46805	növények
Linaria cannabina	kenderike	BD_2	Védett	3566	46806	madarak
Opuntia humifusa	ördögnyelv-fügekaktusz	\N	Inváziós	3658	46807	növények
Opuntia chlorotica	\N	\N	Inváziós	3661	46808	növények
Opuntia compressa	\N	\N	Inváziós	3661	46809	növények
Opuntia macrorhiza	\N	\N	Inváziós	3661	46810	növények
Opuntia polyacantha	\N	\N	Inváziós	3661	46811	növények
Opuntia tortispina	\N	\N	Inváziós	3661	46812	növények
Azolla sp.	\N	\N	Inváziós	3790	46813	növények
Cyperus esculentus	\N	\N	Inváziós	3791	46814	növények
Graptemys kohni	mississippi tarajosteknős	\N	Inváziós	3793	46815	hüllők
Limnobium laevigatum	amasonasi békatutaj	\N	Inváziós	3794	46816	növények
Micropterus salmoides	pisztrángsügér	\N	Inváziós	3795	46817	halak
Myriophyllum sp.	\N	\N	Inváziós	3796	46818	növények
Gymnocoronis spilanthoides	mexikói vízibojt	IAS	Inváziós	2210	46790	növények
Acacia saligna	\N	IAS	Inváziós	3899	46831	növények
Acridotheres tristis	pásztormejnó	IAS	Inváziós	3901	46834	madarak
Lespedeza cuneata	\N	IAS	Inváziós	3911	46859	növények
Lygodium japonicum	\N	IAS	Inváziós	3911	46860	növények
Plotosus lineatus	\N	IAS	Inváziós	3911	46861	halak
Polygonum perfoliatum	ördögfarok-keserûfû	IAS	Inváziós	3911	46862	növények
Prosopis juliflora	\N	IAS	Inváziós	3911	46863	növények
Salvinia molesta	átellenes rucaöröm	IAS	Inváziós	3911	46864	növények
Triadica sebifera	\N	IAS	Inváziós	3911	46865	növények
Andropogon virginicus	\N	IAS	Inváziós	3924	46893	növények
Arthurdendyus triangulatus	\N	IAS	Inváziós	3927	46904	laposférgek
Cardiospermum grandiflorum	\N	IAS	Inváziós	3927	46905	növények
Cortaderia jubata	\N	IAS	Inváziós	3927	46906	növények
Ehrharta calycina	 	IAS	Inváziós	3927	46907	növények
Ailanthus altissima	bálványfa	IAS	Inváziós	1554	45756	növények
Humulus scandens	japán komló	IAS	Inváziós	1554	46210	növények
Yucca filamentosa	\N	\N	Inváziós	3931	46911	növények
Yucca sp.	\N	\N	Inváziós	3931	46912	növények
Carabus scheidleri jucundus	\N	\N	Védett	4101	46913	bogarak
Pelophylax esculentus	kecskebéka	HD V	Védett	5396	46914	kétéltűek
Dendrocygna bicolor	sujtásos fütyülőlúd	\N	8_melleklet	5688	46915	madarak
Phylloscopus schwarzi	vastagcsőrű füzike	\N	8_melleklet	5688	46916	madarak
Anas discors	kékszárnyú réce	\N	8_melleklet	5688	46917	madarak
Testudo hermanni	görög teknős	HD II	8_melleklet	5688	46918	hüllők
Carabus clathratus auraniensis	\N	\N	Védett	8761	46919	bogarak
Carabus coriaceus pseudorugifer	\N	\N	Védett	8761	46920	bogarak
Carabus montivagus blandus	\N	\N	Védett	8761	46921	bogarak
Carabus scheidleri pseudopreyssleri	\N	\N	Védett	8761	46922	bogarak
Carabus ulrichii fastuosus	\N	\N	Védett	8761	46923	bogarak
Glaresis rufa	vörhenyes csorvány	\N	Védett	1554	46155	bogarak
Maculinea arion ligurica	nagyfoltú hangyaboglárka alfaj	HD IV	Védett	9741	46924	lepkék
Mannia triandra	sziklai illatosmoha	HD II	Védett	1554	46307	növények
Leuciscus aspius	balin	HD II, V	Nem védett	11945	46926	halak
Dianthus plumarius ssp. regis-stephani	Lumnitzer-szegfű (incl. István király-szegfű)	HD II, IV	Fokozottan védett	1554	46026	növények
Canis lupus	farkas	HD II kiemelt, IV	Fokozottan védett	1554	44214	emlősök
Serratula lycopifolia	fénylő zsoltina	HD II kiemelt, IV	Fokozottan védett	1554	46584	növények
Triturus dobrogicus	dunai tarajosgőte	HD II	Védett	1554	45311	kétéltűek
Barbus carpathicus	kárpáti márna	HD II, V	Fokozottan védett	11217	46925	halak
Carabus nodulosus	dunántúli vízifutrinka	HD II, IV	Fokozottan védett	1554	44461	bogarak
Carassius auratus auratus	ezüstkárász	\N	Inváziós	12999	46928	halak
Maculinea alcon alcon	szürkés hangyaboglárka	\N	Védett	13000	46929	lepkék
\.


--
-- Name: vedett_fajok_obm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.vedett_fajok_obm_id_seq', 46929, true);


--
-- Name: vedett_fajok vedett_dinpi_pkey; Type: CONSTRAINT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY public.vedett_fajok
    ADD CONSTRAINT vedett_dinpi_pkey PRIMARY KEY (faj);


--
-- Name: faj_idx; Type: INDEX; Schema: public; Owner: gisadmin
--

CREATE INDEX faj_idx ON public.vedett_fajok USING btree (faj);


--
-- Name: vedett_fajok_vedettseg_idx; Type: INDEX; Schema: public; Owner: gisadmin
--

CREATE INDEX vedett_fajok_vedettseg_idx ON public.vedett_fajok USING btree (vedettseg);


--
-- Name: TABLE vedett_fajok; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON TABLE public.vedett_fajok FROM gisadmin;
GRANT SELECT ON TABLE public.vedett_fajok TO gisadmin;
GRANT ALL ON TABLE public.vedett_fajok TO gisadmin;
GRANT SELECT ON TABLE public.vedett_fajok TO gisadmin;


--
-- Name: SEQUENCE vedett_fajok_obm_id_seq; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE public.vedett_fajok_obm_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE public.vedett_fajok_obm_id_seq TO gisadmin;


--
-- PostgreSQL database dump complete
--

